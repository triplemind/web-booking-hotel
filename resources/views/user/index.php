<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="user alternate icon"></i>
                <div class="content">
                    จัดการผู้ใช้งานระบบ
                </div>
            </h3>
        </div>

        <div class="six wide right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big basic olive button btn-adduser" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลผู้ใช้งาน</button>
                </div>
            </div>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <br>
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui blue table" id="TBL_user">
                    <thead>
                        <tr>
                            <th>ชื่อ - นามสกุล</th>
                            <th>ชื่อผู้ใช้งาน</th>
                            <th>อีเมล</th>
                            <th>ประเภทผู้ใช้งาน</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($users)): ?>
                            <?php foreach ($users as $key => $user):?>
                                <tr>
                                    <td class="wide two"><?php echo $user->firstname." ".$user->lastname ?></td>
                                    <td class="wide two"><?php echo $user->username ?></td>
                                    <td class="wide two"><?php echo $user->email ?></td>
                                    <td class="wide two"><?php echo $user->user_type ?></td>
                                    <td class="wide two">
                                        <i class="large edit outline icon edituser" data-id="<?php echo $user->user_id; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deleteuser" data-id="<?php echo $user->user_id; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('user.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('user.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('user.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('user.delete.post');?>"></div>


<div class="ui medium modal" id="addUserModal">
    <div class="header">เพิ่มข้อมูลผู้ใช้งาน</div>
    <div class="content">
        <div class="ui medium form">
            <div class="field">
                <label>ชื่อผู้ใช้งาน</label>
                <input type="text" placeholder="" name="username" id="username">
            </div>
            <div class="field">
                <label>รหัสผ่าน</label>
                <input type="password" placeholder="" name="password" id="password">
            </div>
            <div class="two fields">
                <div class="field">
                    <label>ชื่อ</label>
                    <input type="text" placeholder="" name="firstname" id="firstname">
                </div>
                <div class="field">
                    <label>นามสกุล</label>
                    <input type="text" placeholder="" name="lastname" id="lastname">
                </div>
            </div>
            <div class="field">
                <label>อีเมล</label>
                <input type="text" placeholder="" name="email" id="email">
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Line ID</label>
                    <input type="text" placeholder="" name="line_id" id="line_id">
                </div>
                <div class="field">
                   <label>เบอร์โทรศัพท์</label>
                    <input type="text" placeholder="" name="tel" id="tel" maxlength="10">
                </div>
            </div>
            <div class="field">
                <label>ประเภทผู้ใช้งาน</label>
                <select class="ui fluid dropdown" name="user_type" id="user_type" >
                    <option value="">เลือกผู้ใช้งาน</option>
                    <option value="Root">Root</option>
                    <option value="ผู้ดูแลระบบ">ผู้ดูแลระบบ</option>
                    <option value="ผู้ใช้ทั่วไป">ผู้ใช้ทั่วไป</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="EditUserModal">
    <div class="header">แก้ไขข้อมูลผู้ใช้งาน</div>
    <div class="content">
        <div class="ui medium form">
            <input type="hidden" placeholder="" name="user_id" id="user_id">
            <div class="field">
                <label>ชื่อผู้ใช้งาน</label>
                <input type="text" placeholder="" name="ed_username" id="ed_username">
            </div>
            <div class="field">
                <label>รหัสผ่าน</label>
                <input type="password" placeholder="" name="ed_password" id="ed_password">
            </div>
            <div class="two fields">
                <div class="field">
                    <label>ชื่อ</label>
                    <input type="text" placeholder="" name="ed_firstname" id="ed_firstname">
                </div>
                <div class="field">
                    <label>นามสกุล</label>
                    <input type="text" placeholder="" name="ed_lastname" id="ed_lastname">
                </div>
            </div>
            <div class="field">
                <label>อีเมล</label>
                <input type="text" placeholder="" name="ed_email" id="ed_email">
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Line ID</label>
                    <div class="ui fluid left icon input">
                        <input type="text" placeholder="" id="ed_line_id" name="ed_line_id">
                        <i class="at icon"></i>
                    </div>
                </div>
                <div class="field">
                   <label>เบอร์โทรศัพท์</label>
                    <input type="text" placeholder="" name="ed_tel" id="ed_tel" maxlength="10">
                </div>
            </div>
            <div class="field">
                <label>ประเภทผู้ใช้งาน</label>
                <select class="ui fluid dropdown" name="ed_user_type" id="ed_user_type" >
                    <option value="">เลือกผู้ใช้งาน</option>
                    <option value="Root">Root</option>
                    <option value="ผู้ดูแลระบบ">ผู้ดูแลระบบ</option>
                    <option value="ผู้ใช้ทั่วไป">ผู้ใช้ทั่วไป</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>