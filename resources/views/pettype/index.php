<div class="ui basic segment">
    <br>
    <div class="ui stackable column grid segment">
        <div class="eight wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="paw icon"></i>
                <div class="content">
                    จัดการประเภทสัตว์เลี้ยง
                </div>
            </h3>
        </div>

        <div class="four wide column">
            <!-- <button class="ui fluid big blue button btn-addpet" type="submit" style="border-radius: 30px;">จัดการสายพันธุ์สัตว์เลี้ยง</button> -->
        </div>

        <div class="four wide column">
            <button class="ui fluid big button" type="submit" onclick="window.location.href = '/admin/petsShop';" style="border-radius: 30px;">ย้อนกลับ</button>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <div class="two fields">
            <div class="field">
                <h4>ประเภทสัตว์เลี้ยง</h4>
            </div>
            <div class="field" style="text-align: end;">
                <button class="ui green button btn-addpettype" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลประเภทสัตว์เลี้ยง</button>
            </div>
        </div>
        <br>
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_managepettype">
                    <thead>
                        <tr>
                            <th>ชื่อประเภทสัตว์เลี้ยง</th>
                            <th>สายพันธุ์สัตว์เลี้ยง</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($getpettypes)): ?>
                            <?php foreach ($getpettypes as $key => $pettype):?>
                                <tr>
                                    <td><?php echo empty($pettype->type_pet) ? '-' : $pettype->type_pet ?></td>
                                    <td><?php echo empty($pettype->species_pet) ? '-' : $pettype->species_pet ?></td>
                                    <td>
                                        <?php 
                                            $lb_color = "";
                                            if($pettype->status == 'Active'){
                                                $lb_color = 'green';
                                            }else{
                                                $lb_color = 'red';
                                            }
                                        ?>
                                        
                                        <a class="ui <?php echo $lb_color; ?> label"><?php echo $pettype->status ?></a>
                                    </td>
                                    <td>
                                        <i class="large edit outline icon editpettype" data-id="<?php echo $pettype->type_id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deletepettype" data-id="<?php echo $pettype->type_id ; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>

    <br>

    <div class="ui form segment">
        <div class="two fields">
            <div class="field">
                <h4>สายพันธุ์สัตว์เลี้ยง</h4>
            </div>
            <div class="field" style="text-align: end;">
                <button class="ui green button btn-addspecies" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลสายพันธุ์สัตว์เลี้ยง</button>
            </div>
        </div>
        <br>
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui teal table" id="TBL_getspecies">
                    <thead>
                        <tr>
                            <th>ประเภทสัตว์เลี้ยง</th>
                            <th>ชื่อภาษาไทย</th>
                            <th>ชื่อภาษาอังกฤษ</th>
                            <th>สถานะ</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($getspecies)): ?>
                            <?php foreach ($getspecies as $key => $specy):?>
                                <tr>
                                    <td>
                                        <?php 
                                            $lb_color = "";
                                            if($specy->pet_type == 'แมว'){
                                                $lb_color = 'yellow';
                                            }else{
                                                $lb_color = 'blue';
                                            }
                                        ?>
                                        
                                        <a class="ui <?php echo $lb_color; ?> label"><?php echo empty($specy->pet_type) ? '-' : $specy->pet_type ?></a>
                                    </td>
                                    <td><?php echo empty($specy->name) ? '-' : $specy->name ?></td>
                                    <td><?php echo empty($specy->shortname) ? '-' : $specy->shortname ?></td>
                                    <td>
                                        <?php 
                                            $lb_color = "";
                                            if($specy->status == 'Active'){
                                                $lb_color = 'green';
                                            }else{
                                                $lb_color = 'red';
                                            }
                                        ?>
                                        
                                        <a class="ui <?php echo $lb_color; ?> label"><?php echo $specy->status ?></a>
                                    </td>
                                    <td>
                                        <i class="large edit outline icon editspecies" data-id="<?php echo $specy->id ; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deletespecies" data-id="<?php echo $specy->id ; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('pettype.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('pettype.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('pettype.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('pettype.delete.post');?>"></div>

    <div id='addspecies-url' data-url="<?php echo \URL::route('pettype.addspecies.post');?>"></div>
    <div id='editspecies-url' data-url="<?php echo \URL::route('pettype.editspecies.post');?>"></div>
    <div id='deletespecies-url' data-url="<?php echo \URL::route('pettype.deletespecies.post');?>"></div>


<div class="ui medium modal" id="addPetTypeModal">
    <div class="header">เพิ่มข้อมูลประเภทสัตว์เลี้ยง</div>
    <div class="content">
        <div class="ui medium form">
            <div class="field">
                <label>ชื่อประเภทสัตว์เลี้ยง</label>
                <input type="text" placeholder="" name="type_pet" id="type_pet">
            </div>
            <div class="field">
                <label>ขนาดของสัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="type_petsize" id="type_petsize">
                    <option value="">เลือกขนาดของสัตว์เลี้ยง</option>
                    <option value="22">ขนาดใหญ่</option>
                    <option value="11">ขนาดเล็ก</option>
                </select>
            </div>
            <div class="field">
                <label>สายพันธุ์สัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="species" id="species">
                    <option value="">เลือกสายพันธุ์สัตว์เลี้ยง</option>
                    <?php if(!empty($getspecies)): ?>
                        <?php foreach ($getspecies as $key => $species): ?>
                            <?php if($species->status != 'Inactive'): ?>
                                <option value="<?php echo $species->id ?>"><?php echo $species->name." (".$species->pet_type.")" ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="status" id="status">
                    <option value="">เลือกสถานะ</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="editPetTypeModal">
    <div class="header">เพิ่มข้อมูลประเภทสัตว์เลี้ยง</div>
    <div class="content">
        <div class="ui medium form">
            <input type="hidden" placeholder="" name="edtype_id" id="edtype_id">
            <div class="field">
                <label>ชื่อประเภทสัตว์เลี้ยง</label>
                <input type="text" placeholder="" name="ed_type_pet" id="ed_type_pet">
            </div>
            <div class="field">
                <label>ขนาดของสัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="ed_type_petsize" id="ed_type_petsize">
                    <option value="">เลือกขนาดของสัตว์เลี้ยง</option>
                    <option value="22">ขนาดใหญ่</option>
                    <option value="11">ขนาดเล็ก</option>
                </select>
            </div>
            <div class="field">
                <label>สายพันธุ์สัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="ed_species" id="ed_species">
                    <option value="">เลือกสายพันธุ์สัตว์เลี้ยง</option>
                    <?php if(!empty($getspecies)): ?>
                        <?php foreach ($getspecies as $key => $species): ?>
                            <?php if($species->status != 'Inactive'): ?>
                                <option value="<?php echo $species->id ?>"><?php echo $species->name." (".$species->pet_type.")" ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="ed_status" id="ed_status">
                    <option value="">เลือกสถานะ</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="addSpeciesModal">
    <div class="header">เพิ่มข้อมูลสายพันธุ์สัตว์เลี้ยง</div>
    <div class="content">
        <div class="ui medium form">
            <div class="field">
                <label>ประเภทสัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="pet_type" id="pet_type" >
                    <option value="">เลือกประเภทสัตว์เลี้ยง</option>
                    <?php if(!empty($getpettypes)): ?>
                        <?php foreach ($getpettypes as $key => $pettype): ?>
                            <?php if($pettype->status != 'Inactive'): ?>
                                <option value="<?php echo $pettype->type_pet; ?>"><?php echo $pettype->type_pet.' ('.($pettype->type_petsize == 11 ? "ขนาดเล็ก" : "ขนาดใหญ่").')'; ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>ชื่อภาษาไทย</label>
                <input type="text" placeholder="" name="name" id="name">
            </div>
            <div class="field">
                <label>ชื่อภาษาอังกฤษ</label>
                <input type="text" placeholder="" name="shortname" id="shortname">
            </div>
            <div class="field">
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="SPstatus" id="SPstatus">
                    <option value="">เลือกสถานะ</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="editSpeciesModal">
    <div class="header">เพิ่มข้อมูลสายพันธุ์สัตว์เลี้ยง</div>
    <div class="content">
        <div class="ui medium form">
            <input type="hidden" placeholder="" name="ed_id" id="ed_id">
            <div class="field">
                <label>ประเภทสัตว์เลี้ยง</label>
                <select class="ui fluid dropdown" name="ed_pet_type" id="ed_pet_type" >
                    <option value="">เลือกประเภทสัตว์เลี้ยง</option>
                    <?php if(!empty($getpettypes)): ?>
                        <?php foreach ($getpettypes as $key => $pettype): ?>
                            <?php if($pettype->status != 'Inactive'): ?>
                                <option value="<?php echo $pettype->type_pet; ?>"><?php echo $pettype->type_pet.' ('.($pettype->type_petsize == 11 ? "ขนาดเล็ก" : "ขนาดใหญ่").')'; ?></option>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </select>
            </div>
            <div class="field">
                <label>ชื่อภาษาไทย</label>
                <input type="text" placeholder="" name="ed_name" id="ed_name">
            </div>
            <div class="field">
                <label>ชื่อภาษาอังกฤษ</label>
                <input type="text" placeholder="" name="ed_shortname" id="ed_shortname">
            </div>
            <div class="field">
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="ed_SPstatus" id="ed_SPstatus">
                    <option value="">เลือกสถานะ</option>
                    <option value="Active">Active</option>
                    <option value="Inactive">Inactive</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>