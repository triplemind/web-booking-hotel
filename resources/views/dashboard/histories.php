<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="clipboard outline icon"></i>
                <div class="content">
                    รายการจองห้องพักทั้งหมด
                </div>
            </h3>
        </div>

        <!-- <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big green button btn-addproduct" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลสินค้า</button>
                </div>
            </div>
        </div> -->

    </div>

    <br>

    <div class="ui form segment">
        <h4 class="ui header">ค้นหารายการจอง</h4>
        <div class="fields">
            <div class="five wide field">
                <label>วันที่เริ่มต้น</label>
                <div class="ui calendar" id="rangestart">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" placeholder="วันที่เริ่มต้น" value="<?php echo isset($filters['rangestart'])? $filters['rangestart'] : '' ?>">
                    </div>
                </div>
            </div>
            <div class="five wide field">
                <label>วันที่สิ้นสุด</label>
                <div class="ui calendar" id="rangeend">
                    <div class="ui input left icon">
                        <i class="calendar icon"></i>
                        <input type="text" placeholder="วันที่สิ้นสุด" value="<?php echo isset($filters['rangeend'])? $filters['rangeend'] : '' ?>">
                    </div>
                </div>
            </div>
            <div class="six wide field">
                <?php $filters['order_status'] = isset($filters['order_status']) ? $filters['order_status'] : '' ?>
                <label>สถานะ</label>
                <select class="ui fluid dropdown" name="order_status" id="order_status" >
                    <option value="all" <?php echo $filters['order_status'] == 'all' ? 'selected' : '' ?>>ทั้งหมด</option>
                    <option value="รอชำระเงิน" <?php echo $filters['order_status'] == 'รอชำระเงิน' ? 'selected' : '' ?>>รอชำระเงิน</option>
                    <option value="รอตรวจสอบการชำระเงิน" <?php echo $filters['order_status'] == 'รอตรวจสอบการชำระเงิน' ? 'selected' : '' ?>>รอตรวจสอบการชำระเงิน</option>
                    <option value="การจองสำเร็จ" <?php echo $filters['order_status'] == 'การจองสำเร็จ' ? 'selected' : '' ?>>การจองสำเร็จ</option>
                    <option value="อยู่ระหว่างการใช้งาน" <?php echo $filters['order_status'] == 'อยู่ระหว่างการใช้งาน' ? 'selected' : '' ?>>อยู่ระหว่างการใช้งาน</option>
                    <option value="เสร็จสิ้น" <?php echo $filters['order_status'] == 'เสร็จสิ้น' ? 'selected' : '' ?>>เสร็จสิ้น</option>
                </select>
            </div>
        </div>
        <div class="ui unstackable four column grid ">
            <div class="column">
            </div>
            <div class="right floated column">
                <div class="fields">
                    <div class="eight wide field">
                        <button class="ui fluid small blue button btn-search" type="submit" style="border-radius: 30px;">ค้นหา</button>
                    </div>
                    <div class="eight wide field">
                        <button class="ui fluid small red button btn-clear" type="submit" style="border-radius: 30px;">ล้าง</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui blue table" id="TBL_onlinelist">
                    <thead>
                        <tr>
                            <th>หมายเลขการจอง</th>
                            <th>จำนวนห้องพัก</th>
                            <th>วัน-เวลา ที่จอง</th>
                            <th>การชำระเงิน</th>
                            <th>สถานะการจอง</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody> 
                        <?php if(!empty($reserves)): ?>
                            <?php foreach ($reserves as $key => $reserve):?>
                                <tr>
                                    <td>
                                        <?php echo $reserve->reserve_number ?>
                                    </td>
                                    <td>
                                        <?php echo $reserve->typeroom_id ?>
                                    </td>
                                    <td>
                                        <?php echo DateThai($reserve->reserve_data_time, true, true) ?>
                                    </td>
                                    <td>
                                        <?php echo empty($reserve->pay_id) ? 'รอชำระเงิน' : 'ชำระเงินแล้ว'; ?>
                                    </td>
                                    <td>
                                        <?php 
                                            $lb_color = "";
                                            if($reserve->reserve_status == "รอชำระเงิน"){
                                                $lb_color = "red";
                                            }else if($reserve->reserve_status == "รอตรวจสอบการชำระเงิน"){
                                                $lb_color = "orange";
                                            }else if($reserve->reserve_status == "การจองสำเร็จ"){
                                                $lb_color = "green";
                                            }else if($reserve->reserve_status == "อยู่ระหว่างการใช้งาน"){ 
                                                $lb_color = "blue";
                                            }else if($reserve->reserve_status == "เสร็จสิ้น"){ 
                                                $lb_color = "teal";
                                            }
                                        ?>
                                        
                                        <a class="ui <?php echo $lb_color; ?> label"><?php echo $reserve->reserve_status ?></a>
                                    </td>
                                    <td>
                                        <button class="ui small teal button btn-online-detail" type="submit" data-id="<?php echo $reserve->reserve_id ; ?>">รายละเอียด</button>
                                    </td>
                                </tr> 
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>

                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('dashboard.ajax_center.post');?>"></div>

