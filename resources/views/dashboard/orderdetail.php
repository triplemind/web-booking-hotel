<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="receipt icon"></i>
                <div class="content">
                    รายละเอียดการจอง
                </div>
            </h3>
        </div>
        <div class="right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid big grey button btn-back" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
                </div>
            </div>
        </div>
    </div>
    <?php 
        $lb_color = "";
        if($reserves->reserve_status == "รอชำระเงิน"){
            $lb_color = "red";
        }else if($reserves->reserve_status == "รอตรวจสอบการชำระเงิน"){
            $lb_color = "orange";
        }else if($reserves->reserve_status == "การจองสำเร็จ"){
            $lb_color = "green";
        }else if($reserves->reserve_status == "อยู่ระหว่างการใช้งาน"){ 
            $lb_color = "blue";
        }else if($reserves->reserve_status == "เสร็จสิ้น"){ 
            $lb_color = "teal";
        }else if($reserves->reserve_status == "ยกเลิก"){
            $lb_color = "gray";
        }
    ?>
    <div class="ui form segment">
        <div class="top right attached large ui label">
            <div class="ui <?php echo $lb_color; ?> empty circular label"></div> <?php echo $reserves->reserve_status ?>
        </div>

        <div class="fields" style="margin-top: 1em;">
            <div class="sixteen wide field">
                <table class="ui single line table">
                    <tbody>
                        <tr>
                            <td class="" colspan="4">
                                <img class="ui centered medium image" src="<?php echo empty($reserves->roomtype) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $reserves->roomtype->room_img); ?>">
                            </td>
                        </tr>
                        <tr>
                            <td class="two wide active">หมายเลขการจอง</td>
                            <td class="six wide"><?php echo $reserves->reserve_number ?></td>
                            <td class="two wide active">วัน-เวลา ที่ทำรายการ</td>
                            <td class="six wide"><?php echo DateThai($reserves->created_at, true, true) ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ประเภทห้องพัก</td>
                            <td class="six wide"><?php echo empty($reserves->roomtype) ? '-' : $reserves->roomtype->type_nameroom; ?></td>
                            <td class="two wide active">Check in - Check out</td>
                            <td class="six wide"><?php echo empty($reserves->roomtype) ? '-' : $reserves->roomtype->checkindate.' - '.$reserves->roomtype->checkoutdate; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">วันเข้าพัก - วันสิ้นสุด</td>
                            <td class="six wide"><?php echo DateThai($reserves->reserve_data_chkin, true, false).' - '.DateThai($reserves->reserve_data_chkout, true, false); ?></td>
                            <td class="two wide active">จำนวนวันเข้าพัก</td>
                            <td class="six wide"><?php echo $reserves->numofnights.' วัน'; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">การชำระเงิน</td>
                            <td class="six wide"><?php echo empty($reserves->payment_method) ? 'รอชำระเงิน' : $reserves->payment_method; ?></td>
                            <td class="two wide active">จำนวนเงินทั้งหมด</td>
                            <td class="six wide"><?php echo $reserves->reserve_price ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">รายละเอียดการจอง</td>
                            <td class="six wide" colspan="3">
                                <p>จำนวนห้องพัก : <?php echo $reserves->reserve_nmroom ?> ห้อง</p>
                                <p>จำนวนคนเข้าพัก : <?php echo $reserves->reserve_nmpeople ?> คน</p>
                                <p>จำนวนสัตว์เลี้ยงเข้าพัก :<?php echo $reserves->reserve_nmpet ?> ตัว</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="two wide active">บริการเสริม</td>
                            <td class="six wide" colspan="3">
                                <?php if($reserves->req_swim_type == 1): ?><p>ว่ายน้ำ : <?php echo $reserves->num_swim ?></p><?php endif ?>
                                <?php if($reserves->req_bed_type == 1): ?><p>เตียงเสริม : <?php echo $reserves->num_bed ?></p><?php endif ?>
                                <?php if($reserves->req_breakfast_type == 1): ?><p>อาหารเช้าสำหรับผู้เข้าพัก : <?php echo $reserves->num_food ?></p><?php endif ?>
                                <?php if($reserves->req_bath_type == 1): ?><p>อาบน้ำเป่าขน : <?php echo $reserves->num_bath ?></p><?php endif ?>
                                <?php if($reserves->req_pet_breakfast_type == 1): ?><p>อาหารสำหรับสุนัข : <?php echo $reserves->num_petfood ?></p><?php endif ?>
                            </td>
                        </tr>
                        <?php if(!empty($reserves->pay_id)): ?>
                            <tr>
                                <tr>
                                    <td class="two wide active">วัน - เวลา ที่ชำระเงิน</td>
                                    <td class="six wide">
                                        <?php echo empty($reserves->payment) ? '-' : DateThai($reserves->payment->pay_date_time, true, true) ?>
                                    </td>
                                    <td class="two wide active">ธนาคารที่ชำระเงิน</td>
                                    <td class="six wide">
                                        <?php echo empty($reserves->payment) ? '-' : $reserves->payment->pay_bank ?>
                                    </td>
                                </tr>
                                <td class="two wide active">หลักฐานการชำระเงิน</td>
                                <td class="six wide"  colspan="3">
                                    <?php if(empty($reserves->payment)): ?>
                                        รอชำระเงิน
                                    <?php else: ?>
                                        <img class="ui large image" src="<?php echo empty($reserves->payment->pay_img) ? url('').'/themes/image/logo.png' : url("").str_replace("/public","", $reserves->payment->pay_img); ?>">
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>

        <input type="hidden" name="reserve_id" id="reserve_id" value="<?php echo $reserves->reserve_id ?>" />
        <div class="ui section divider"></div>

        <h3>ข้อมูลการเช็คอิน - เช็คเอ้าท์</h3>
        <?php if($reserves->reserve_status == 'การจองสำเร็จ'): ?>
            <!-- กำลังจัดเตรียมสินค้า -->
            <div class="two fields">
                <div class="field">
                    <label style="color: #000;">เลขบัตรประจำตัวประชาชนผู้เช็คอิน</label>
                    <input type="text" maxlength="13" name="idCard" id="idCard" value="<?php echo empty($reserves->idCard) ? '' : $reserves->idCard; ?>" />
                </div>
                <div class="field">
                    <label style="color: #000;">ชื่อ-นามสกุลผู้เช็คอิน</label>
                    <input type="text" name="realname" id="realname" value="<?php echo empty($reserves->realname) ? '' : $reserves->realname; ?>" />
                </div>
            </div>
            <div class="three fields" style="margin-top: 1em;">
                <div class="field">
                    <label style="color: #000;">วันที่เช็คอิน</label>
                    <div class="ui calendar" id="reserve_data_chkin">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="วันที่เช็คอิน" style="border-radius: 20px;" value="<?php echo $reserves->reserve_data_chkin ?>">
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label style="color: #000;">เวลาเช็คอิน</label>
                    <div class="ui calendar" id="reserve_time_chkin">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="เวลาเช็คอิน" style="border-radius: 20px;" value="">
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label style="color: #000;">ห้องพัก</label>
                    <select class="ui fluid dropdown" name="room_id" id="room_id" >
                        <option value="">เลือกห้องพัก</option>
                        <?php if(!empty($getrooms)): ?>
                        <?php foreach ($getrooms as $key => $getroom): ?>
                            <option value="<?php echo $getroom->rm_id; ?>"><?php echo $getroom->rm_name." (".$getroom->room_number.")"; ?></option>
                        <?php endforeach ?>
                        <?php endif ?>
                    </select>
                </div>
            </div>
            <div class="field" style="margin-top: 1em;text-align: end;">
                <button class="ui blue button btn-checkin " data-status="อยู่ระหว่างการใช้งาน" type="submit" >บันทึก</button>
            </div>
        <?php endif ?>
        <?php if($reserves->reserve_status == 'อยู่ระหว่างการใช้งาน'): ?>
            <!-- อยู่ระหว่างการใช้งาน -->
            <div class="two fields">
                <div class="field">
                    <label style="color: #000;">เลขบัตรประจำตัวประชาชนผู้เช็คอิน</label>
                    <input type="text" name="idCard" value="<?php echo empty($reserves->idCard) ? '' : $reserves->idCard; ?>" readonly />
                </div>
                <div class="field">
                    <label style="color: #000;">ชื่อ-นามสกุลผู้เช็คอิน</label>
                    <input type="text" name="realname" value="<?php echo empty($reserves->realname) ? '' : $reserves->realname; ?>" readonly />
                </div>
            </div>
            <div class="three fields" style="margin-top: 1em;">
                <div class="field">
                    <label style="color: #000;">วันที่เช็คเอ้าท์</label>
                    <div class="ui calendar" id="reserve_data_chkout">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="วันที่เช็คเอ้าท์" style="border-radius: 20px;" value="<?php echo $reserves->reserve_data_chkout ?>">
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label style="color: #000;">เวลาเช็คเอ้าท์</label>
                    <div class="ui calendar" id="reserve_data_time">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="เวลาเช็คเอ้าท์" style="border-radius: 20px;" value="">
                        </div>
                    </div>
                </div>
                <div class="field">
                    <label style="color: #000;">ห้องงพัก</label>
                    <input type="text" name="room_ids" value="<?php echo empty($reserves->room) ? '-' : $reserves->room->rm_name." (ห้องพักหมายเลข ".$reserves->room->room_number.")" ?>" readonly />
                </div>
            </div>
            <div class="field" style="margin-top: 1em;text-align: end;">
                <button class="ui blue button btn-checkout " data-status="เสร็จสิ้น" type="submit" >บันทึก</button>
            </div>
        <?php endif ?>



        <div class="ui section divider"></div>

        <?php //if($reserves->reserve_status == 'อยู่ระหว่างการใช้งาน' || $reserves->reserve_status == 'เสร็จสิ้น' || $reserves->reserve_status == 'ยกเลิก'): ?>
            <div class="sixteen wide field">
                <table class="ui single line table">
                    <tbody>
                        <tr>
                            <td class="two wide active">วัน - เวลาเช็คอิน</td>
                            <td class="six wide"><?php echo empty($reserves->reserve_data_chkin) ? "-" : DateThai($reserves->reserve_data_chkin, true, false)."  ".$reserves->reserve_time_chkin ?></td>
                            <td class="two wide active">วัน - เวลาเช็คเอ้าท์</td>
                            <td class="six wide"><?php echo empty($reserves->reserve_data_chkout) ? "-" : DateThai($reserves->reserve_data_chkout, true, false)."  ".$reserves->reserve_data_time ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ห้องพัก</td>
                            <td class="six wide" colspan="3"><?php echo empty($reserves->room) ? '-' : $reserves->room->rm_name." (ห้องพักหมายเลข ".$reserves->room->room_number.")" ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="ui section divider"></div>
        <?php //endif ?>

        <?php if(!empty($user_type)): ?>
            <?php if($user_type == 'ผู้ดูแลระบบ'): ?>
                <div class="sixteen wide field">
                    <div class="ui unstackable column grid ">
                        <div class="right floated column">
                            <div class="sixteen wide fields" style="float: right;">
                                <?php if($reserves->reserve_status == 'รอตรวจสอบการชำระเงิน'): ?>
                                    <!-- การจองสำเร็จ -->
                                    <div class="field">
                                        <button class="ui fluid green button btn-confirm" data-status="การจองสำเร็จ" type="submit" >ยืนยันการชำระเงิน</button>
                                    </div>
                                <?php endif ?>
                                <?php if($reserves->reserve_status !== 'อยู่ระหว่างการใช้งาน' || $reserves->reserve_status !== 'เสร็จสิ้น' || $reserves->reserve_status !== 'ยกเลิก'): ?>
                                    <div class="field">
                                        <button class="ui fluid red button btn-cancel" type="submit" >ยกเลิก</button>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        <?php endif ?>
        
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('dashboard.ajax_center.post');?>"></div>

