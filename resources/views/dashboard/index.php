<div class="ui basic segment">
    <br>
    <br>
    <div class="ui form segment">
        <h4 class="ui header">รายการจองห้องพักประจำวันที่ <?php echo $datetoday; ?></h4>
        <div class="fields">
            <!-- <div class="field">
                <div class="ui orange massive message">
                    <i class="shopping cart icon"></i>
                    ยอดขายออนไลน์   <?php //echo $online_pos; ?>
                </div>
            </div>
            <div class="field">
                <div class="ui green massive message">
                    <i class="shopping basket icon"></i>
                    ยอดขายหน้าร้าน   <?php //echo $mormal_pos; ?>
                </div>
            </div> -->
            <!-- <div class="sixteen wide field">
                <div class="ui pink massive message" style="text-align: center;">
                    <i class="calculator icon"></i>
                    ยอดขายทั้งหมด   <?php //echo $total_all; ?>
                </div>
            </div> -->
        </div>
    </div>
    <br>
    <div class="ui form segment">
        <div class="fields">
            <div class="twelve wide field">
                 <h4 class="ui header">รายการจองห้องพัก</h4>
            </div>
            <div class="four wide field">
                <!--   -->
                <button class="ui blue right floated button" onclick="window.location = '/admin/dashboard/histories';">ประวัติการจองห้องพักทั้งหมด</button>
            </div>
        </div>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui blue table" id="TBL_onlinelist">
                    <thead>
                        <tr>
                            <th>หมายเลขการจอง</th>
                            <th>จำนวนห้องพัก</th>
                            <th>วัน-เวลา ที่จอง</th>
                            <th>การชำระเงิน</th>
                            <th>สถานะการจอง</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody> 
                        <?php if(!empty($reserves)): ?>
                            <?php foreach ($reserves as $key => $reserve):?>
                                <tr>
                                    <td>
                                        <?php echo $reserve->reserve_number ?>
                                    </td>
                                    <td>
                                        <?php echo $reserve->typeroom_id ?>
                                    </td>
                                    <td>
                                        <?php echo DateThai($reserve->reserve_data_chkin, true, false) ?>
                                    </td>
                                    <td>
                                        <?php echo empty($reserve->pay_id) ? 'รอชำระเงิน' : 'ชำระเงินแล้ว'; ?>
                                    </td>
                                    <td>
                                        <?php 
                                            $lb_color = "";
                                            if($reserve->reserve_status == "รอชำระเงิน"){
                                                $lb_color = "red";
                                            }else if($reserve->reserve_status == "รอตรวจสอบการชำระเงิน"){
                                                $lb_color = "orange";
                                            }else if($reserve->reserve_status == "การจองสำเร็จ"){
                                                $lb_color = "green";
                                            }else if($reserve->reserve_status == "อยู่ระหว่างการใช้งาน"){ 
                                                $lb_color = "blue";
                                            }else if($reserve->reserve_status == "เสร็จสิ้น"){ 
                                                $lb_color = "teal";
                                            }
                                        ?>
                                        
                                        <a class="ui <?php echo $lb_color; ?> label"><?php echo $reserve->reserve_status ?></a>
                                    </td>
                                    <td>
                                        <button class="ui small teal button btn-online-detail" type="submit" data-id="<?php echo $reserve->reserve_id ; ?>">รายละเอียด</button>
                                    </td>
                                </tr> 
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>

                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

