<br>
<br>
<h1 class="ui header" style="text-align: center;">
    ติดต่อเรา
</h1>
<br>

<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">	

	<div class="ui stackable two column grid" style="padding: 15px;background-color: #886d4d;">
		<div class="ten wide column" style="text-align: center;color: #fff;">
			<img class="ui centered image" src="<?php echo url('').'/themes/image/mapdd.png'; ?>">
		</div>
		<div class="six wide column" style="text-align: center;color: #fff;">

			<div class="ui teal icon message">
				<i class="phone icon"></i>
				<div class="content">
					<div class="header">
				      ติดต่อสอบถาม หรือ จองห้องพัก
				    </div>
				</div>
			</div>
			<p>โทรศัพท์  : 02 917 3349</p>
			<p>มือถือ   : 092 2688272</p>
			<p>แฟกซ์   : 02 917 3159</p>
			<p>ID.Line  : gingerbread1</p>

			<br>

			<div class="ui teal icon message">
				<i class="map marked alternate icon"></i>
				<div class="content">
					<div class="header">
				      สถานที่ตั้งโครงการจังหวดนครนายก
				    </div>
				</div>
			</div>
			<p>76/16 หมู่ 12 หนองแสนตอ ต.พรหมณี</p>
			<p>อ.เมือง นครนายก 26000</p>
			<p>GPS  14.224313,101.184084</p>
			<br>
			<p><a href="https://www.facebook.com/GingerBreadHouse.Nakonnayok" target="_blank" style="color: #fff">www.facebook.com/GingerBreadHouse.Nakonnayok</a></p>


		</div>
	</div>
</div>

<br>
<br>
<br>