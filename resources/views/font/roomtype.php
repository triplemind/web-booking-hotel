<br>
<br>
<!-- <h1 class="ui header" style="text-align: center;">
    ROOM TYPE
</h1>
<br> -->
<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">
	<?php if(!empty($getRoomTypes)): ?>
		<?php foreach ($getRoomTypes as $key => $getRoomType): ?>
			<?php if(($key+1)%2 != 0): ?>
			<div class="ui stackable two column grid" style="padding: 15px;">
				<div class="ten wide column">
					<!-- class="ui centered large image" -->
		            <img  src="<?php echo empty($getRoomType->room_img) ? url('').'/themes/image/The HOTEL.png' : url("").str_replace("/public","", $getRoomType->room_img); ?>" height="400px" width="450px">
		        </div>
		        <div class="six wide column" >
		        	<br>
					<h3 style="text-align: center;"><?php echo $getRoomType->type_nameroom; ?></h3>
					<br>
					<br>
		            <p style="word-break: break-all;"><?php echo $getRoomType->info; ?></p>
		            <br>
					<br>
					<div style="text-align: center;">
						<button class="ui button" style="border-radius: 30px;background-color: #886D4D;color: #fff;" type="submit" onclick="window.location.href = '/roomdetail/<?php echo $getRoomType->typeroom_id; ?>';">เพิ่มเติม</button>
					</div>
		        </div>
		    </div>
		    <?php elseif(($key+1)%2 == 0): ?>
		    	<div class="ui stackable two column grid" style="padding: 15px;">
		    		<div class="six wide column" >
			        	<br>
						<h3 style="text-align: center;"><?php echo $getRoomType->type_nameroom; ?></h3>
						<br>
						<br>
			            <p style="word-break: break-all;"><?php echo $getRoomType->info; ?></p>
			            <br>
						<br>
						<div style="text-align: center;">
							<button class="ui button" style="border-radius: 30px;background-color: #886D4D;color: #fff;" type="submit" onclick="window.location.href = '/roomdetail/<?php echo $getRoomType->typeroom_id; ?>';">เพิ่มเติม</button>
						</div>
			        </div>
					<div class="ten wide column" style="text-align: end;">
						<!-- class="ui centered large image" -->
			            <img  src="<?php echo empty($getRoomType->room_img) ? url('').'/themes/image/The HOTEL.png' : url("").str_replace("/public","", $getRoomType->room_img); ?>" height="400px" width="450px">
			        </div>
			    </div>
		    <?php endif ?>
	    <?php endforeach ?>
    <?php endif ?>
</div>
    
