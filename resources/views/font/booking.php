<br>
<br>
<h1 class="ui header" style="text-align: center;">
    BOOKING
</h1>
<br>

<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">
    <?php if(!empty($getRoomType)): ?>
        <div class="ui stackable two column grid" style="padding: 15px;">
            <input type="hidden" name="typeroom_id" id="typeroom_id" value="<?php echo $getRoomType->typeroom_id ?>">
            <input type="hidden" name="reserve_price" id="reserve_price" value="<?php echo $getRoomType->type_rate ?>">
            <div class="ten wide column">
                <img class="ui centered large image" src="<?php echo empty($getRoomType->room_img) ? url('').'/themes/image/The HOTEL.png' : url("").str_replace("/public","", $getRoomType->room_img); ?>">
            </div>
            <div class="six wide column" style="background-color: #886d4d;color: #fff;">
                <br>
                <h3 style="text-align: center;"><?php echo $getRoomType->type_nameroom; ?></h3>
                <br>
                <br>
               
                <div class="ui form " style="margin-bottom: 1rem;">
                    <div class="required field">
                        <label style="color: #fff;">เลือกวันเข้าพัก</label>
                        <div class="ui calendar" id="reserve_data_chkin">
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input type="text" placeholder="กรุณาเลือกวันเข้าพัก" style="border-radius: 20px;">
                            </div>
                        </div>
                    </div>
                    <div class="required field">
                        <label style="color: #fff;">เลือกวันสิ้นสุด</label>
                        <div class="ui calendar" id="reserve_data_chkout">
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input type="text" placeholder="กรุณาเลือกวันสิ้นสุด" style="border-radius: 20px;">
                            </div>
                        </div>
                    </div>
                    <div class="four wide fields">
                        <div class="field">
                            <label style="color: #fff;">จำนวนผู้เข้าพัก</label>
                            <div class="ui fluid left icon input">
                                <input type="number" placeholder="1" id="reserve_nmpeople" name="reserve_nmpeople" min="1" style="border-radius: 20px;">
                                <i class="user icon"></i>
                            </div>
                        </div>
                        <div class="field">
                            <label style="color: #fff;">สัตว์เลี้ยงพันธุ์เล็ก</label>
                            <div class="ui fluid left icon input">
                                <input type="number" placeholder="1" id="reserve_nmpet" name="reserve_nmpet" min="1" style="border-radius: 20px;">
                                <i class="paw icon"></i>
                            </div>
                        </div>
                        <div class="field">
                            <label style="color: #fff;">สัตว์เลี้ยงพันธุ์ใหญ่</label>
                            <div class="ui fluid left icon input">
                                <input type="number" placeholder="1" id="reserve_nmpetbig" name="reserve_nmpetbig" min="1" style="border-radius: 20px;">
                                <i class="paw icon"></i>
                            </div>
                        </div>
                        <div class="required field">
                            <label style="color: #fff;">จำนวนห้องพัก</label>
                            <div class="ui fluid left icon input">
                                <input type="number" placeholder="1" id="reserve_nmroom" name="reserve_nmroom" min="1" style="border-radius: 20px;">
                                <i class="paw icon"></i>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <button class="ui fluid teal button" style="border-radius: 30px;background-color: #fff;color: #886D4D;" type="submit" id="btn_booking">Confirm booking</button>
                    </div>
                </div>
            </div>

        </div>
    <?php endif ?>
</div>

<div class="ui stackable" style="padding-left: 2rem;padding-right: 2rem;margin-top: 4rem;">
   <!--  <div class="ui very padded raised segment" style="background-color: #80674b;border-radius: 10px;">
        <div class="ui form " style="margin-bottom: 1rem;">
            <div class="field">
                <h3 style="color: #fff;">Personal information</h3>
            </div>
            <div class="two fields required">
                <div class="field">
                    <label style="color: #fff;">Firstname</label>
                    <input type="text" placeholder="Please Enter First name" id="firstname" name="firstname" style="border-radius: 20px;" value="<?php echo empty($user->firstname) ? '' : $user->firstname; ?>">
                </div>
                <div class="field">
                    <label style="color: #fff;">Lastname</label>
                    <input type="text" placeholder="Please Enter Last name" id="lastname" name="lastname" style="border-radius: 20px;" value="<?php echo empty($user->lastname) ? '' : $user->lastname; ?>">
                </div>
            </div>
            <div class="two fields">
                <div class="required field">
                    <label style="color: #fff;">Phone number</label>
                    <input type="text" placeholder="Please Enter Phone number" id="tel" name="tel" maxlength="10" style="border-radius: 20px;" value="<?php echo empty($user->tel) ? '' : $user->tel; ?>">
                </div>

                <div class="field">
                    <label style="color: #fff;">Email</label>
                    <input type="text" placeholder="Please Enter Email" id="email" name="email" style="border-radius: 20px;" value="<?php echo empty($user->email) ? '' : $user->email; ?>">
                </div>

                <div class="field">
                    <label style="color: #fff;">Line ID</label>
                    <input type="text" placeholder="Please Enter Line ID" id="line_id" name="line_id" style="border-radius: 20px;" value="<?php echo empty($user->line_id) ? '' : $user->line_id; ?>">
                </div>
            </div>


        </div>
    </div> -->
</div>




