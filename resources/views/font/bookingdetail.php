<br>
<br>
<h1 class="ui header" style="text-align: center;">
    รายละเอียดการจอง
</h1>
<br>

<input type="hidden" name="id" id="id" value="<?php echo $reserves->reserve_id; ?>">

<div class="ui stackable" style="padding-left: 2rem;padding-right: 2rem;margin-top: 4rem;">
	<div class="ui form">
		<div class="fields" style="margin-top: 1em;">
            <div class="sixteen wide field">
                <table class="ui table">
                    <tbody>
                        <tr>
                            <td class="" colspan="4" style="text-align: center;">
                                <!-- class="ui centered medium image" -->
                                <img  src="<?php echo empty($reserves->roomtype) ? url('').(env('APP_ENV') == 'production' ? '/public' : '').'/themes/image/logo.png' : url("").str_replace("/public",(env('APP_ENV') == 'production' ? '/public' : ''), $reserves->roomtype->room_img); ?>" height="400px" width="450px">
                            </td>
                        </tr>
                        <tr>
                            <td class="two wide active">หมายเลขการจอง</td>
                            <td class="six wide"><?php echo $reserves->reserve_number ?></td>
                            <td class="two wide active">วัน-เวลา ที่ทำรายการ</td>
                            <td class="six wide"><?php echo DateThai($reserves->created_at, true, true) ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">ประเภทห้องพัก</td>
                            <td class="six wide"><?php echo empty($reserves->roomtype) ? '-' : $reserves->roomtype->type_nameroom; ?></td>
                            <td class="two wide active">Check in - Check out</td>
                            <td class="six wide"><?php echo empty($reserves->roomtype) ? '-' : $reserves->roomtype->checkindate.' - '.$reserves->roomtype->checkoutdate; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">วันเข้าพัก - วันสิ้นสุด</td>
                            <td class="six wide"><?php echo DateThai($reserves->reserve_data_chkin, true, false).' - '.DateThai($reserves->reserve_data_chkout, true, false); ?></td>
                            <td class="two wide active">จำนวนวันเข้าพัก</td>
                            <td class="six wide"><?php echo $reserves->numofnights.' วัน'; ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">การชำระเงิน</td>
                            <td class="six wide"><?php echo empty($reserves->pay_id) ? 'รอชำระเงิน' : $reserves->payment->pay_status; ?></td>
                            <td class="two wide active">จำนวนเงินทั้งหมด</td>
                            <td class="six wide"><?php echo $reserves->reserve_price ?></td>
                        </tr>
                        <tr>
                            <td class="two wide active">รายละเอียดการจอง</td>
                            <td class="six wide" colspan="3">
                                <p>จำนวนห้องพัก : <?php echo $reserves->reserve_nmroom ?> ห้อง</p>
                                <p>จำนวนคนเข้าพัก : <?php echo $reserves->reserve_nmpeople ?> คน</p>
                                <p>จำนวนสัตว์เลี้ยงเข้าพัก :<?php echo $reserves->reserve_nmpet ?> ตัว</p>
                            </td>
                        </tr>
                        <tr>
                            <td class="two wide active">บริการเสริม</td>
                            <td class="six wide" colspan="3">
                                <?php if($reserves->req_swim_type == 1): ?><p>ว่ายน้ำ : <?php echo $reserves->num_swim ?></p><?php endif ?>
                                <?php if($reserves->req_bed_type == 1): ?><p>เตียงเสริม : <?php echo $reserves->num_bed ?></p><?php endif ?>
                                <?php if($reserves->req_breakfast_type == 1): ?><p>อาหารเช้าสำหรับผู้เข้าพัก : <?php echo $reserves->num_food ?></p><?php endif ?>
                                <?php if($reserves->req_bath_type == 1): ?><p>อาบน้ำเป่าขน : <?php echo $reserves->num_bath ?></p><?php endif ?>
                                <?php if($reserves->req_pet_breakfast_type == 1): ?><p>อาหารสำหรับสุนัข : <?php echo $reserves->num_petfood ?></p><?php endif ?>
                            </td>
                        </tr>
                        <?php if(!empty($reserves->pay_id)): ?>
                            <tr>
                                <td class="two wide active">หลักฐานการชำระเงิน</td>
                                <td class="six wide"  colspan="3">
                                    <?php if(empty($reserves->payment)): ?>
                                        รอชำระเงิน
                                    <?php else: ?>
                                        <img class="ui large image" src="<?php echo empty($reserves->payment->pay_img) ? url('').'/themes/image/logo.png' : url("").str_replace("/public","", $reserves->payment->pay_img); ?>">
                                    <?php endif ?>
                                </td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>

        <br>
        <br>

        <div class="fields" style="margin-top: 1em;">
            <div class="sixteen wide field">
                <table class="ui blue table" id="TBL_Room">
                    <thead>
                        <tr>
                            <th>ชื่อสัตว์เลี้ยง</th>
                            <th>เพศสัตว์เลี้ยง</th>
                            <th>วันวันเกิดสัตว์เลี้ยง</th>
                            <th>ประเภทสัตว์เลี้ยง</th>
                            <th>สายพันธุ์สัตว์เลี้ยง</th>
                            <th>รายละเอียดเพิ่มเติม</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(count($petdetailArr) != 0): ?>
                            <?php for ($i=0; $i < count($petdetailArr); $i++): ?>
                                <tr>
                                    <td class="wide two"><?php echo empty($petdetailArr[$i]['pet_name']) ? '-' : $petdetailArr[$i]['pet_name']; ?></td>
                                    <td class="wide two"><?php echo empty($petdetailArr[$i]['pet_gender']) ? '-' : $petdetailArr[$i]['pet_gender']; ?></td>
                                    <td class="wide two"><?php echo empty($petdetailArr[$i]['pet_birthday']) ? '-' : DateThai($petdetailArr[$i]['pet_birthday'], true, false) ?></td>
                                    <td class="wide two"><?php echo empty($petdetailArr[$i]['type_id']) ? '-' : $petdetailArr[$i]['type_id'] ?></td>
                                    <td class="wide two"><?php echo empty($petdetailArr[$i]['species_id']) ? '-' : $petdetailArr[$i]['species_id'] ?></td>
                                    <td class="wide two"><?php echo empty($petdetailArr[$i]['pet_remark']) ? '-' : $petdetailArr[$i]['pet_remark'] ?></td>
                                   
                                </tr>
                            <?php endfor ?>
                        <?php endif ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>

    <?php if($reserves->reserve_status != 'ยกเลิก'): ?>
    <br>
    <div class="ui icon teal message">
        <i class="wallet icon"></i>
        <div class="content">
            <div class="header">
                ข้อมูลสำหรับโอนเงิน
            </div>
            <p><span style="font-weight: bold;">บัญชีธนาคาร : </span>K-bank ธนาคารกสิกรไทย   <span style="font-weight: bold;margin-left: 1.2rem;">ชื่อบัญชี : </span>นางสาวจารุวัจน์ เล็กดำรงศักดิ์   <span style="font-weight: bold;margin-left: 1.2rem;">เลขบัญชี : </span>035-1-49937-0</p>
           <!--  <p></p>
            <p></p> -->
        </div>
    </div>
    <br>

        <?php if(empty($reserves->pay_id)): ?>
            <div class="ui form segment">
                <h4>แนบหลักฐานการชำระเงิน</h4>
                <div class="three fields">
                    <div class="field">
                        <label>วันที่ชำระเงิน</label>
                        <div class="ui calendar" id="pay_date_time">
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input type="text" placeholder="วันที่ชำระเงิน" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label>จำนวนเงินที่ชำระเงิน</label>
                        <input type="number" placeholder="จำนวนเงินที่ชำระเงิน" id="pay_money">
                    </div>
                    <div class="field">
                        <label>ธนาคาร</label>
                        <select class="ui fluid search selection dropdown" name="pay_bank" id="pay_bank" >
                            <option value='' selected>--- กรุณาเลือกธนาคาร ---</option>
                            <option value='ธนาคารกรุงเทพ'>ธนาคารกรุงเทพ</option>
                            <option value='ธนาคารกสิกรไทย'>ธนาคารกสิกรไทย</option>
                            <option value='ธนาคารกรุงไทย'>ธนาคารกรุงไทย</option>
                            <option value='ธนาคารทหารไทย'>ธนาคารทหารไทย</option>
                            <option value='ธนาคารไทยพาณิชย์'>ธนาคารไทยพาณิชย์</option>
                            <option value='ธนาคารกรุงศรีอยุธยา'>ธนาคารกรุงศรีอยุธยา</option>
                            <option value='ธนาคารเกียรตินาคิน'>ธนาคารเกียรตินาคิน</option>
                            <option value='ธนาคารซีไอเอ็มบีไทย'>ธนาคารซีไอเอ็มบีไทย</option>
                            <option value='ธนาคารทิสโก้'>ธนาคารทิสโก้</option>
                            <option value='ธนาคารธนชาต'>ธนาคารธนชาต</option>
                            <option value='ธนาคารยูโอบี'>ธนาคารยูโอบี</option>
                            <option value='ธนาคารออมสิน'>ธนาคารออมสิน</option>
                            <option value='ธนาคารอาคารสงเคราะห์'>ธนาคารอาคารสงเคราะห์</option>
                            <option value='ธนาคารอิสลามแห่งประเทศไทย'>ธนาคารอิสลามแห่งประเทศไทย</option>
                        </select>
                    </div>
                </div>
                <div class="field">
                    <label>หลักฐานการชำระเงิน</label>
                    <input type="file" placeholder="" name="pay_img" id="pay_img">
                </div>
                <div class="ui column grid">
                    <div class="column" style="text-align: end;">
                        <button class="ui blue button btn-save-payment" type="submit">ยืนยันการชำระเงิน</button>
                    </div>
                </div>
            </div>
        <?php else: ?>
            <div class="ui form segment">
                <h4>แก้ไขหลักฐานการชำระเงิน</h4>
                <input type="hidden" name="payid" id="payid" value="<?php echo $getpayment->pay_id; ?>">
                <div class="three fields">
                    <div class="field">
                        <label>วันเวลาที่ชำระเงิน</label>
                        <div class="ui calendar" id="ed_pay_date_time">
                            <div class="ui input left icon">
                                <i class="calendar icon"></i>
                                <input type="text" placeholder="วันที่ชำระเงิน" readonly value="<?php echo $getpayment->pay_date_time; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label>จำนวนเงินที่ชำระเงิน</label>
                        <input type="number" placeholder="จำนวนเงินที่ชำระเงิน" value="<?php echo $getpayment->pay_money; ?>" id="ed_pay_money">
                    </div>
                    <div class="field">
                        <label>ธนาคาร</label>
                        <select class="ui fluid search selection dropdown" name="ed_pay_bank" id="ed_pay_bank" >
                            <option value='' selected>--- กรุณาเลือกธนาคาร ---</option>
                            <option value='ธนาคารกรุงเทพ' <?php echo ($getpayment->pay_bank == 'ธนาคารกรุงเทพ') ? 'selected' : ''; ?> >ธนาคารกรุงเทพ</option>
                            <option value='ธนาคารกสิกรไทย' <?php echo ($getpayment->pay_bank == 'ธนาคารกสิกรไทย') ? 'selected' : ''; ?> >ธนาคารกสิกรไทย</option>
                            <option value='ธนาคารกรุงไทย' <?php echo ($getpayment->pay_bank == 'ธนาคารกรุงไทย') ? 'selected' : ''; ?> >ธนาคารกรุงไทย</option>
                            <option value='ธนาคารทหารไทย' <?php echo ($getpayment->pay_bank == 'ธนาคารทหารไทย') ? 'selected' : ''; ?> >ธนาคารทหารไทย</option>
                            <option value='ธนาคารไทยพาณิชย์' <?php echo ($getpayment->pay_bank == 'ธนาคารไทยพาณิชย์') ? 'selected' : ''; ?> >ธนาคารไทยพาณิชย์</option>
                            <option value='ธนาคารกรุงศรีอยุธยา' <?php echo ($getpayment->pay_bank == 'ธนาคารกรุงศรีอยุธยา') ? 'selected' : ''; ?> >ธนาคารกรุงศรีอยุธยา</option>
                            <option value='ธนาคารเกียรตินาคิน' <?php echo ($getpayment->pay_bank == 'ธนาคารเกียรตินาคิน') ? 'selected' : ''; ?> >ธนาคารเกียรตินาคิน</option>
                            <option value='ธนาคารซีไอเอ็มบีไทย' <?php echo ($getpayment->pay_bank == 'ธนาคารซีไอเอ็มบีไทย') ? 'selected' : ''; ?> >ธนาคารซีไอเอ็มบีไทย</option>
                            <option value='ธนาคารทิสโก้' <?php echo ($getpayment->pay_bank == 'ธนาคารทิสโก้') ? 'selected' : ''; ?> >ธนาคารทิสโก้</option>
                            <option value='ธนาคารธนชาต' <?php echo ($getpayment->pay_bank == 'ธนาคารธนชาต') ? 'selected' : ''; ?> >ธนาคารธนชาต</option>
                            <option value='ธนาคารยูโอบี' <?php echo ($getpayment->pay_bank == 'ธนาคารยูโอบี') ? 'selected' : ''; ?> >ธนาคารยูโอบี</option>
                            <option value='ธนาคารออมสิน' <?php echo ($getpayment->pay_bank == 'ธนาคารออมสิน') ? 'selected' : ''; ?> >ธนาคารออมสิน</option>
                            <option value='ธนาคารอาคารสงเคราะห์' <?php echo ($getpayment->pay_bank == 'ธนาคารอาคารสงเคราะห์') ? 'selected' : ''; ?> >ธนาคารอาคารสงเคราะห์</option>
                            <option value='ธนาคารอิสลามแห่งประเทศไทย' <?php echo ($getpayment->pay_bank == 'ธนาคารอิสลามแห่งประเทศไทย') ? 'selected' : ''; ?> >ธนาคารอิสลามแห่งประเทศไทย</option>
                        </select>
                    </div>
                </div>
                <div class="field">
                    <label>หลักฐานการชำระเงิน</label>
                    <input type="file" placeholder="" name="ed_pay_img" id="ed_pay_img">
                </div>
                <div class="ui column grid">
                    <div class="column" style="text-align: end;">
                        <button class="ui blue button btn-edit-payment" type="submit">บันทึก</button>
                    </div>
                </div>
            </div>
        <?php endif ?>
    <?php endif ?>

    <?php if($reserves->reserve_status != 'ยกเลิก' && empty($reserves->pay_id)): ?>
        <div class="ui column grid">
            <div class="column" style="text-align: end;">
                <button class="ui red button btn-cancel" type="submit">ยกเลิกการจอง</button>
            </div>
        </div>
    <?php endif ?>

</div>



<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='ajax-center-url' data-url="<?php echo \URL::route('booking.ajax_center.post');?>"></div>
<div id='add-url' data-url="<?php echo \URL::route('booking.booking.post');?>"></div>