<br>
<br>
<h1 class="ui header" style="text-align: center;">
    ประเภทห้องพัก
</h1>
<br>

<!-- ส่วนของการ search -->
<div class="ui stackable" style="padding-left: 2rem;padding-right: 2rem;">
    <div class="ui very padded raised segment" style="background-color: #8b8986bf;border-radius: 10px;">
        <div class="ui form " style="margin-bottom: 1rem;">
            <div class="six fields">
                <div class="three wide field required">
                    <label style="color: #fff;">เลือกวันเข้าพัก</label>
                    <div class="ui calendar" id="reserve_data_chkin">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="กรุณาเลือกวันเข้าพัก" style="border-radius: 20px;" value="<?php echo isset($filters['checkin'])? date("d-m-Y", strtotime($filters['checkin'])) : '' ?>">
                        </div>
                    </div>
                </div>
                <div class="three wide field required">
                    <label style="color: #fff;">เลือกวันสิ้นสุด</label>
                    <div class="ui calendar" id="reserve_data_chkout">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="กรุณาเลือกวันสิ้นสุด" style="border-radius: 20px;" value="<?php echo isset($filters['checkout'])? date("d-m-Y", strtotime($filters['checkout'])) : '' ?>">
                        </div>
                    </div>
                </div>
                <div class="two wide field">
                    <label style="color: #fff;">จำนวนผู้เข้าพัก</label>
                    <div class="ui fluid left icon input">
                        <input type="number" placeholder="1" id="reserve_nmpeople" name="reserve_nmpeople" min="1" style="border-radius: 20px;" value="<?php echo isset($filters['persons']) ? $filters['persons'] : 0 ?>">
                        <i class="user icon"></i>
                    </div>
                </div>
                <div class="two wide field">
                    <label style="color: #fff;">สัตว์เลี้ยงพันธุ์เล็ก</label>
                    <div class="ui fluid left icon input">
                        <input type="number" placeholder="1" id="reserve_nmpet" name="reserve_nmpet" min="1" style="border-radius: 20px;" value="<?php echo isset($filters['pet']) ? $filters['pet'] : 0 ?>">
                        <i class="paw icon"></i>
                    </div>
                </div>
                <div class="two wide field">
                    <label style="color: #fff;">สัตว์เลี้ยงพันธุ์ใหญ่</label>
                    <div class="ui fluid left icon input">
                        <input type="number" placeholder="1" id="reserve_nmpetbig" name="reserve_nmpetbig"  value="<?php echo isset($filters['pet_big']) ? $filters['pet_big'] : 0 ?>" min="1" style="border-radius: 20px;">
                        <i class="paw icon"></i>
                    </div>
                </div>
                <div class="two wide field">
                    <label style="color: #fff;">จำนวนห้องพัก</label>
                    <div class="ui fluid left icon input">
                        <input type="number" placeholder="1" id="reserve_nmroom" name="reserve_nmroom" min="1" style="border-radius: 20px;" value="1">
                        <i class="bed icon"></i>
                    </div>
                </div>
                <div class="two wide field" style="margin-top: 1.6rem;">
                    <button class="ui fluid teal button" style="border-radius: 30px;background-color: #886D4D;color: #fff;" type="submit" onclick="gotoBookingSummary(<?php echo $getRoomType->typeroom_id.','.$getRoomType->type_rate; ?>)">จอง</button>
                    <!-- onclick="window.location.href = '/booking/<?php echo $getRoomType->typeroom_id; ?>';" -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ส่วนของการ search -->

<br>
<br>
<br>

<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">
	<?php if(!empty($getRoomType)): ?>
		<div class="ui stackable two column grid" style="padding: 15px;">
			<div class="ten wide column" style="text-align: center;background-color: #886d4d;color: #fff;">
                <!-- class="ui centered large image"  -->
	            <img src="<?php echo empty($getRoomType->room_img) ? url('').'/themes/image/The HOTEL.png' : url("").str_replace("/public","", $getRoomType->room_img); ?>"  height="400px" width="100%" style="margin-left: -5rem;margin-top: -5rem;">
	        </div>
	        <div class="six wide column" style="background-color: #886d4d;color: #fff;">
	        	<br>
				<h3 style="text-align: center;font-size: 28px;"><?php echo $getRoomType->type_nameroom; ?></h3>
				<br>
				<br>
                <div class="ui stackable two column grid">
                    <div class="eight wide column"  style="padding-top: 0rem;">
                        <p style="word-break: break-all;text-align: center;font-size: 16px;">ราคา : </p>
                    </div>
                    <div class="eight wide column"  style="padding-top: 0rem;">
                        <p style="word-break: break-all;text-align: left;font-size: 16px;"><?php echo $getRoomType->type_rate; ?> bath</p>
                    </div>
                </div>

                <div class="ui stackable two column grid">
                    <div class="eight wide column"  style="padding-top: 0rem;">
                        <p style="word-break: break-all;text-align: center;font-size: 16px;">ขนาดห้อง : </p>
                    </div>
                    <div class="eight wide column"  style="padding-top: 0rem;">
                        <p style="word-break: break-all;text-align: left;font-size: 16px;"><?php echo $getRoomType->size; ?> ตารางเมตร</p>
                    </div>
                </div>

                <div class="ui stackable two column grid">
                    <div class="eight wide column"  style="padding-top: 0rem;">
                        <p style="word-break: break-all;text-align: center;font-size: 16px;">สัตว์เลี้ยง : </p>
                    </div>
                    <div class="eight wide column"  style="padding-top: 0rem;">
                        <p style="word-break: break-all;text-align: left;font-size: 16px;">พันธุ์เล็ก <?php echo $getRoomType->smallpet; ?> ตัว</p>
                        <p style="word-break: break-all;text-align: left;font-size: 16px;">พันธุ์ใหญ่ <?php echo $getRoomType->bigpet; ?> ตัว</p>
                    </div>
                </div>

                <div class="ui stackable two column grid">
                    <div class="eight wide column"  style="padding-top: 0rem;">
                        <p style="word-break: break-all;text-align: center;font-size: 16px;">จำนวนผู้เข้าพัก : </p>
                    </div>
                    <div class="eight wide column"  style="padding-top: 0rem;">
                        <p style="word-break: break-all;text-align: left;font-size: 16px;"><?php echo $getRoomType->people; ?> คน  เตียงเสริม <?php echo $getRoomType->extra; ?></p>
                    </div>
                </div>
	           
	            <!-- <br>
				<br>
				<div style="text-align: center;">
					<button class="ui button" style="border-radius: 30px;background-color: #fff;color: #886d4d;" type="submit" onclick="window.location.href = '/booking/<?php echo $getRoomType->typeroom_id; ?>';">BOOK NOW</button>
				</div> -->
	        </div>
	    </div>

        <div class="ui stackable two column grid" style="padding: 15px;">
            <div class="six wide column" style="">
                <h3 style="word-break: break-all;font-size: 18px;">รายละเอียดเพิ่มเติม : </h3>
            </div>
            <div class="ten wide column" style="">
               <!-- < ?php $info_array = explode(" ", $getRoomType->info); ?>
                <?php //foreach ($info_array as $key => $info): ?>
                    <p style="word-break: break-all;font-size: 16px;"><?php //echo $info; ?></p>
                <?php //endforeach ?> -->
                <p style="word-break: break-all;font-size: 16px;"><?php echo $getRoomType->info; ?></p>
            </div>
        </div>

	        <br>

        <div class="ui stackable two column grid" style="padding: 15px;">
            <div class="six wide column" style="">
	        	<h3 style="word-break: break-all;font-size: 18px;">เวลาเช็คอินและเวลาเช็คเอ้าท์ : </h3>
            </div>
            <div class="ten wide column" style="">
                <p style="word-break: break-all;font-size: 16px;">เวลาเช็คอิน : <?php echo $getRoomType->checkindate; ?> PM</p>
	        	<p style="word-break: break-all;font-size: 16px;">เวลาเช็คเอ้าท์ : <?php echo $getRoomType->checkoutdate; ?> AM</p>
            </div>
        </div>

        <br>
        <br>

        <div class="ui stackable column grid" style="padding: 15px;">
            <div class="sixteen wide column" style="text-align: center;">
                <button class="ui button" style="border-radius: 30px;background-color: #886d4d;color: #fff;"onclick="window.location.href = '/roomtype';">ย้อนกลับ</button>
            </div>
        </div>
    <?php endif ?>
</div>
    

<!-- Data -->
<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='ajax-center-url' data-url="<?php echo \URL::route('booking.ajax_center.post');?>"></div>
<div id='add-url' data-url="<?php echo \URL::route('booking.booking.post');?>"></div>
    
<style type="text/css">
    .ui.dimmer.modals{
        background-color: #42424159;
    }
</style>

<div class="ui fullscreen basic modal" style="background-color: #000000d9;">
    <i class="close icon"></i>
    <div class="header">
        <?php echo $getRoomType->type_nameroom; ?>
    </div>
    <div class="scrolling image content">
        <div class="ui big image" id="additionalDiv" style="display: unset;">
            <h3>เพิ่มตัวเลือกพิเศษ</h3>
            <hr>
            <div class="ui message">
                <div class="header">
                    อาบน้ำเป่าขน
                </div>
                <div class="ui form " style="margin-bottom: 1rem;">
                    <div class="three fields">
                        <div class="eight wide field">
                            <label>บริการอาบน้ำเป่าขนให้กับสุนัข</label>
                        </div>
                        <div class="five wide field">
                            <label style="font-size: 20px;">฿300 /ครั้ง</label>
                        </div>
                        <div class="three wide field">
                            <label>จำนวน</label>
                            <input type="number" placeholder="1" id="reserve_nmbathpet" name="reserve_nmbathpet" min="1" style="border-radius: 20px;" onchange="recalculateReservation('reserve_nmbathpet')">
                        </div>
                    </div>
                </div>
            </div>

            <div class="ui message">
                <div class="header">
                    ว่ายน้ำ
                </div>
                <div class="ui form " style="margin-bottom: 1rem;">
                    <div class="three fields">
                        <div class="eight wide field">
                            <label>บริการเสริมสำหรับสุนัขเท่านั้น</label>
                        </div>
                        <div class="five wide field">
                            <label style="font-size: 20px;">฿300 /ครั้ง</label>
                        </div>
                        <div class="three wide field">
                            <label>จำนวน</label>
                            <input type="number" placeholder="1" id="reserve_nmswimpet" name="reserve_nmswimpet" min="1" style="border-radius: 20px;" onchange="recalculateReservation('reserve_nmswimpet')">
                        </div>
                    </div>
                </div>
            </div>

            <div class="ui message">
                <div class="header">
                    อาหารเช้าสำหรับผู้เข้าพัก
                </div>
                <div class="ui form " style="margin-bottom: 1rem;">
                    <div class="three fields">
                        <div class="eight wide field">
                            <label>บริการเสริมอาหารเช้าสำหรับผู้เข้าพัก</label>
                        </div>
                        <div class="five wide field">
                            <label style="font-size: 20px;">฿200 /ท่าน</label>
                        </div>
                        <div class="three wide field">
                            <label>จำนวน</label>
                            <input type="number" placeholder="1" id="reserve_nmfood" name="reserve_nmfood" min="1" style="border-radius: 20px;" onchange="recalculateReservation('reserve_nmfood')">
                        </div>
                    </div>
                </div>
            </div>

            <div class="ui message">
                <div class="header">
                    อาหารสำหรับสุนัข
                </div>
                <div class="ui form " style="margin-bottom: 1rem;">
                    <div class="three fields">
                        <div class="eight wide field">
                            <label>บริการเสริมอาหารสำหรับสุนัข</label>
                        </div>
                        <div class="five wide field">
                            <label style="font-size: 20px;">฿200/ครั้ง</label>
                        </div>
                        <div class="three wide field">
                            <label>จำนวน</label>
                            <input type="number" placeholder="1" id="reserve_nmfoodpet" name="reserve_nmfoodpet" min="1" style="border-radius: 20px;" onchange="recalculateReservation('reserve_nmfoodpet')">
                        </div>
                    </div>
                </div>
            </div>

            <div class="ui message">
                <div class="header">
                    เตียงเสริม
                </div>
                <div class="ui form " style="margin-bottom: 1rem;">
                    <div class="three fields">
                        <div class="eight wide field">
                            <label>บริการเตียงเสริม</label>
                        </div>
                        <div class="five wide field">
                            <label style="font-size: 20px;">฿200 /ท่าน</label>
                        </div>
                        <div class="three wide field">
                            <label>จำนวน</label>
                            <input type="number" placeholder="1" id="reserve_nmbed" name="reserve_nmbed" min="1" style="border-radius: 20px;" onchange="recalculateReservation('reserve_nmbed')">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui big image" id="personalDiv" style="display: none;">
            <div class="ui form " style="margin-bottom: 1rem;">
                <div class="field">
                    <h3 style="color: #fff;">Personal information</h3>
                </div>
                <div class="two fields required">
                    <div class="field">
                        <label style="color: #fff;">ชื่อจริง</label>
                        <input type="text" placeholder="ชื่อจริง" id="firstname" name="firstname" style="border-radius: 20px;" value="<?php echo empty($user->firstname) ? '' : $user->firstname; ?>">
                    </div>
                    <div class="field">
                        <label style="color: #fff;">นามสกุล</label>
                        <input type="text" placeholder="นามสกุล" id="lastname" name="lastname" style="border-radius: 20px;" value="<?php echo empty($user->lastname) ? '' : $user->lastname; ?>">
                    </div>
                </div>
                <div class="two fields">
                    <div class="required field">
                        <label style="color: #fff;">โทรศัพท์</label>
                        <input type="text" placeholder="โทรศัพท์" id="tel" name="tel" maxlength="10" style="border-radius: 20px;" value="<?php echo empty($user->tel) ? '' : $user->tel; ?>">
                    </div>

                    <div class="required field">
                        <label style="color: #fff;">Email</label>
                        <input type="text" placeholder="Email" id="email" name="email" style="border-radius: 20px;" value="<?php echo empty($user->email) ? '' : $user->email; ?>">
                    </div>

                    <div class="field">
                        <label style="color: #fff;">Line ID</label>
                        <input type="text" placeholder="Line ID" id="line_id" name="line_id" style="border-radius: 20px;" value="<?php echo empty($user->line_id) ? '' : $user->line_id; ?>">
                    </div>
                </div>
            </div>
            <div class="ui divider"></div>
            <?php 
                $totalpet = 0;

                $smallpet   = isset($filters['pet']) ? $filters['pet'] : 1;
                $bigpet     = isset($filters['pet_big']) ? $filters['pet_big'] : 1; 

                $totalpet = $bigpet+$smallpet;

            ?>
            <div class="ui form " style="margin-bottom: 1rem;">
                <!-- <div class="two fields"> -->
                    <div class="field">
                        <h3 style="color: #fff;">ข้อมูลสัตว์เลี้ยง</h3>
                    </div>
                    <!-- <div class="six wide field">
                        <div class="ui green button btn-addpet" style="border-radius: 30px;color: #fff;">
                        เพิ่มข้อมูลสัตว์เลี้ยง</div>
                    </div> -->
                <!-- </div> -->
                <?php if($totalpet !== 0): ?>
                    <?php for ($i=0; $i < $totalpet; $i++): ?>
                        <div class="two fields">
                            <div class="field">
                                <label style="color: #fff;">ชื่อสัตว์เลี้ยง (<?php echo $i+1; ?>)</label>
                                <input type="text" placeholder="ชื่อสัตว์เลี้ยง" id="pet_name_<?php echo $i+1; ?>" name="pet_name_<?php echo $i+1; ?>" value="">
                            </div>
                            <div class="field">
                                <label style="color: #fff;">เพศสัตว์เลี้ยง (<?php echo $i+1; ?>)</label>
                                <select class="ui fluid dropdown" name="pet_gender_<?php echo $i+1; ?>" id="pet_gender_<?php echo $i+1; ?>">
                                    <option value="">เลือกเพศสัตว์เลี้ยง</option>
                                    <option value="11">เพศผู้</option>
                                    <option value="22">เพศเมีย</option>
                                </select>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field">
                                <label style="color: #fff;">ประเภทสัตว์เลี้ยง (<?php echo $i+1; ?>)</label>
                                <select class="ui fluid dropdown" name="type_id_<?php echo $i+1; ?>" id="type_id_<?php echo $i+1; ?>" >
                                    <option value="">เลือกประเภทสัตว์เลี้ยง</option>
                                    <?php if(!empty($getpettypes)): ?>
                                        <?php foreach ($getpettypes as $key => $pettype): ?>
                                            <?php if($pettype->status != 'Inactive'): ?>
                                                <option value="<?php echo $pettype->type_id; ?>"><?php echo $pettype->type_pet.' ('.($pettype->type_petsize == 11 ? "ขนาดเล็ก" : "ขนาดใหญ่").')'; ?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </select>
                            </div>
                            <div class="field">
                                <label style="color: #fff;">สายพันธุ์สัตว์เลี้ยง (<?php echo $i+1; ?>)</label>
                                <select class="ui fluid dropdown" name="species_id_<?php echo $i+1; ?>" id="species_id_<?php echo $i+1; ?>">
                                    <option value="">เลือกสายพันธุ์สัตว์เลี้ยง</option>
                                    <?php if(!empty($getspecies)): ?>
                                        <?php foreach ($getspecies as $key => $species): ?>
                                            <?php if($species->status != 'Inactive'): ?>
                                                <option value="<?php echo $species->id ?>"><?php echo $species->name." (".$species->pet_type.")" ?></option>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </select>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field">
                                <label style="color: #fff;">วันเกิดสัตว์เลี้ยง (<?php echo $i+1; ?>)</label>
                                <div class="ui calendar" id="pet_birthday_<?php echo $i+1; ?>">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" placeholder="วันเกิดสัตว์เลี้ยง">
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <label style="color: #fff;">รายละเอียดเพิ่มเติม (<?php echo $i+1; ?>)</label>
                                <textarea rows="3" id="pet_remark_<?php echo $i+1; ?>"></textarea>
                            </div>
                        </div>
                        <hr>
                    <?php endfor ?>
                <?php endif ?>
            </div>
        </div>
        <div class="description">
            <h3>สรุปการจอง</h3>

            <hr>
            <!-- class="ui centered large image" -->

            <img  src="<?php echo empty($getRoomType->room_img) ? url('').'/themes/image/The HOTEL.png' : url("").str_replace("/public","", $getRoomType->room_img); ?>"   height="400px" width="450px">

            <!-- <div class="ui header">ประเภทห้องพัก</div> -->
            <!-- <p>ประเภทห้องพัก : </p> -->
            <br>
            <p>วันที่เข้าพัก : <span class="checkintxt" style="margin-left: 3rem;"></span></p>
            <p>วันที่สิ้นสุด : <span class="checkouttxt" style="margin-left: 3.1rem;"></span></p>
            <p>จำนวนวันที่พัก : <span class="numofnights" style="margin-left: 1.8rem;"></span></p>
            <p>จำนวนผู้เข้าพัก : <span class="persontxt" style="margin-left: 1.5rem;"></span></p>
            <p>จำนวนสัตว์เลี้ย (ขนาดเล็ก) : <span class="petstxt" style="margin-left: 1.1rem;"></span></p>
            <p>จำนวนสัตว์เลี้ยง (ขนาดใหญ่) : <span class="bigpetstxt" style="margin-left: 1.1rem;"></span></p>
            <p>จำนวนห้องพัก : <span class="roomstxt" style="margin-left: 1.8rem;"></span></p>

            <hr>

            <div class="ui form " style="margin-bottom: 1rem;">
                <p><?php echo $getRoomType->type_nameroom; ?> : <?php echo $getRoomType->type_rate; ?> บาท</p>
                <p style="font-weight: bold;text-decoration: underline;">เพิ่มเติม</p>

                <div class="additional">
                    <p class="bath"></p>
                    <p class="swim"></p>
                    <p class="food"></p>
                    <p class="petfood"></p>
                    <p class="bed"></p>
                </div>

                <hr>

                <p class="discount"></p>

                <p class="totalprice"></p>

                <br>
                <div class="two fields">
                    <div class="twelve wide field">
                        <input type="text" placeholder="ใส่โค้ดโปรโมชั่น" id="promo_code" name="promo_code" style="border-radius: 20px;">
                    </div>
                    <div class="four wide field">
                        <a class="btn-apply-code" style="font-weight: bold;cursor: pointer;">Apply</a>
                    </div>
                </div>
                <div class="field thismsgcheckpro" style="display: none;">
                    <div class="ui blue message">
                        <div class="header" id="msgcheckpro"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="actions" style="margin-bottom: 1rem;">
        <div class="ui blue button btn-back" style="border-radius: 30px;color: #fff;display: none;">
            ย้อนกลับ
        </div>
        <div class="ui button btn-next" style="border-radius: 30px;background-color: #886D4D;color: #fff;">ดำเนินการต่อ</div>
        <div class="ui approve button btn-approvenow" style="border-radius: 30px;background-color: #886D4D;color: #fff;display: none;">จองและชำระเงิน</div>
    </div>
</div>