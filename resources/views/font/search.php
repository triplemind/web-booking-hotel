<br>
<br>
<!-- ส่วนของการ search -->
<div class="ui stackable" style="padding-left: 2rem;padding-right: 2rem;">
    <div class="ui very padded raised segment" style="background-color: #8b8986bf;border-radius: 10px;">
        <div class="ui form " style="margin-bottom: 1rem;">
            <div class="six fields">
                <div class="three wide field">
                    <label style="color: #fff;">เลือกวันเข้าพัก</label>
                    <div class="ui calendar" id="checkin">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="กรุณาเลือกวันเข้าพัก" style="border-radius: 20px;" value="<?php echo isset($filters['checkin'])? date("d-m-Y", strtotime($filters['checkin'])) : '' ?>">
                        </div>
                    </div>
                </div>
                <div class="three wide field">
                    <label style="color: #fff;">เลือกวันสิ้นสุด</label>
                    <div class="ui calendar" id="checkout">
                        <div class="ui input left icon">
                            <i class="calendar icon"></i>
                            <input type="text" placeholder="กรุณาเลือกวันสิ้นสุด" style="border-radius: 20px;" value="<?php echo isset($filters['checkout'])? date("d-m-Y", strtotime($filters['checkout'])) : '' ?>">
                        </div>
                    </div>
                </div>
                <div class="two wide field">
                    <label style="color: #fff;">จำนวนผู้เข้าพัก</label>
                    <div class="ui fluid left icon input">
                        <input type="number" placeholder="1" id="persons" name="persons" min="1" style="border-radius: 20px;" value="<?php echo isset($filters['persons']) ? $filters['persons'] : 0 ?>">
                        <i class="user icon"></i>
                    </div>
                </div>
                <div class="two wide field">
                    <label style="color: #fff;">สัตว์เลี้ยงพันธุ์เล็ก</label>
                    <div class="ui fluid left icon input">
                        <input type="number" placeholder="1" id="pet" name="pet" min="1" style="border-radius: 20px;" value="<?php echo isset($filters['pet']) ? $filters['pet'] : 0 ?>">
                        <i class="paw icon"></i>
                    </div>
                </div>
                <div class="two wide field">
                    <label style="color: #fff;">สัตว์เลี้ยงพันธุ์ใหญ่</label>
                    <div class="ui fluid left icon input">
                        <input type="number" placeholder="1" id="pet_big" name="pet_big" min="1" style="border-radius: 20px;" value="<?php echo isset($filters['pet_big']) ? $filters['pet_big'] : 0 ?>">
                        <i class="paw icon"></i>
                    </div>
                </div>
                <div class="two wide field" style="margin-top: 1.6rem;">
                    <button class="ui fluid teal button" style="border-radius: 30px;background-color: #886D4D;color: #fff;" type="submit" id="btn_search">ค้นหา</button>
                </div>

                <div class="two wide field" style="margin-top: 1.6rem;">
                    <button class="ui fluid red button" style="border-radius: 30px;color: #fff;" type="submit" id="btn_clear">ล้าง</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ส่วนของการ search -->

<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">
	<?php if(!empty($getRoomTypes)): ?>
		<?php foreach ($getRoomTypes as $key => $getRoomType): ?>
            <?php if(($key+1)%2 != 0): ?>

    			<div class="ui stackable two column grid" style="padding: 15px;">
    				<div class="ten wide column"> 
                        <!-- class="ui centered large image" -->
    		            <img src="<?php echo empty($getRoomType->room_img) ? url('').'/themes/image/The HOTEL.png' : url("").str_replace("/public","", $getRoomType->room_img); ?>" height="400px" width="450px">
    		        </div>
    		        <div class="six wide column" >
    		        	<br>
    					<h3 style="text-align: center;"><?php echo $getRoomType->type_nameroom; ?></h3>
    					<br>
    					<br>
    		            <p style="word-break: break-all;"><?php echo $getRoomType->info; ?></p>
    		            <br>
    					<br>
    					<div style="text-align: center;">
    						<button class="ui button" style="border-radius: 30px;background-color: #886D4D;color: #fff;" type="submit" onclick="window.location.href = '/roomdetail/<?php echo $getRoomType->typeroom_id; ?>';">MORE INFO</button>
    					</div>
    		        </div>
    		    </div>

            <?php elseif(($key+1)%2 == 0): ?>

                <div class="ui stackable two column grid" style="padding: 15px;">
                    <div class="six wide column" >
                        <br>
                        <h3 style="text-align: center;"><?php echo $getRoomType->type_nameroom; ?></h3>
                        <br>
                        <br>
                        <p style="word-break: break-all;"><?php echo $getRoomType->info; ?></p>
                        <br>
                        <br>
                        <div style="text-align: center;">
                            <button class="ui button" style="border-radius: 30px;background-color: #886D4D;color: #fff;" type="submit" onclick="window.location.href = '/roomdetail/<?php echo $getRoomType->typeroom_id; ?>';">MORE INFO</button>
                        </div>
                    </div>
                    <div class="ten wide column" style="text-align: end;"> 
                        <!-- class="ui centered large image" -->
                        <img src="<?php echo empty($getRoomType->room_img) ? url('').'/themes/image/The HOTEL.png' : url("").str_replace("/public","", $getRoomType->room_img); ?>" height="400px" width="450px">
                    </div>
                </div>

            <?php endif ?>
	    <?php endforeach ?>
    <?php endif ?>
    <?php if($getRoomTypes->count() == 0): ?>
    	<h2 class="ui header" style="text-align: center;">ไม่พบข้อมูลที่ค้นหา</h2>
    <?php endif ?>
</div>


<!-- Data -->
<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='ajax-center-url' data-url="<?php echo \URL::route('font.ajax_center.post');?>"></div>