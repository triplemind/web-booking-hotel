<img  src="<?php echo url('').'/themes/image/Group 5.jpg'; ?>" height="500" width="100%">
<!--  class="ui centered fluid image" -->
<div class="ui stackable container" style="margin-top: 2rem;padding-left: 0%; padding-right: 0%;">

	<div class="ui form">
		<div class="two fields">
			<div class="ten wide field">
				<img class="ui centered big image" src="<?php echo url('').'/themes/image/Group 9.jpg'; ?>"  width="100%">
			</div>
			<div class="six wide field">
				<h1 style="color: #464141;font-weight: bolder;font-size: 35px;margin-top: 15rem;">สระว่ายน้ำสุนัข</h1>

				<p style="color: #464141;font-size: 18px;margin-top: 2rem;">เงื่อนไขการใช้สระว่ายน้ำ เพื่อความปลอดภัยกับสุนัขของท่านและสุนัขอื่นๆ</p>

				<div class="ui ordered list">
					<p class="item">สุนัขต้องไม่มีเห็บ - หมัด </p>
					<p class="item">สุนัขต้องไม่เป็นโรคติดต่อหรือโรคผิวหนัง</p>
					<p class="item">สุนัขตัวเมียต้องไม่อยู่ในช่วงติดสัตว์</p>
					<p class="item">สุนัขมีปัญหาทางสุขภาพหรือโรคประจำตัว ต้องแจ้งเจ้าหน้าที่</p>
					<p class="item">สุนัขดุ หรือไม่สามารถเล่นน้ำร่วมกับตัวอื่นได้ ต้องแจ้งเจ้าหน้าที่</p>
					<p class="item">ถ้าสุนัขขับถ่ายในสระว่ายน้ำ เจ้าของสุนัขต้องรับผิดชอบค่าใช้จ่ายในการเปลี่ยนน้ำในสระว่ายน้ำ 3,000 บาท</p>
				</div>

			</div>
		</div>
	</div>


</div>