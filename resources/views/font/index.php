<br>
<br>
<br>
<h1 class="ui horizontal header" style="text-align: center;">
   <span style="color: #886D4D;">บริการ </span>& ร้านอาหาร
</h1>

<br>
<br>

<div class="ui stackable container" style="padding-left: 0%; padding-right: 0%;">
	<div class="ui three stackable special cards">
		<div class="card">
			<div class="blurring dimmable image">
				<div class="ui dimmer">
					<div class="content">
						<div class="center">
							<div class="ui inverted button" onclick="window.location.href = '/swim';">ว่ายน้ำ</div>
						</div>
					</div>
				</div>
				<img src="<?php echo url('').'/themes/image/1.png'; ?>">
			</div>
		</div>
		<div class="card">
			<div class="blurring dimmable image">
				<div class="ui dimmer">
					<div class="content">
						<div class="center">
							<div class="ui inverted button" onclick="window.location.href = '/shower';">อาบน้ำ & เป่าขน</div>
						</div>
					</div>
				</div>
				<img src="<?php echo url('').'/themes/image/3.png'; ?>">
			</div>
		</div>
		<div class="card">
			<div class="blurring dimmable image">
				<div class="ui dimmer">
					<div class="content">
						<div class="center">
							<div class="ui inverted button" onclick="window.location.href = '/food';">ร้านอาหาร</div>
						</div>
					</div>
				</div>
				<img src="<?php echo url('').'/themes/image/2.png'; ?>">
			</div>
		</div>
	</div>
</div>

<br>
<br>

<div class="ui divider"></div>

<br>
<br>

<h1 class="ui horizontal header" style="text-align: center;">
    โปรโม<span style="color: #886D4D;">ชั่น</span>
</h1>

<br>
<br>

<!-- <div class="ui fluid raised segment" style="background-color: #ae9c86;"> -->
	<img class="ui centered fluid image" src="<?php echo url('').'/themes/image/bg22.jpg'; ?>" onclick="window.location.href = '/roomtype';" style="cursor: pointer;">
<!-- </div> -->


<br>
<br>

<div class="ui divider"></div>

<br>
<br>

<h1 class="ui horizontal header" style="text-align: center;">
    <span style="color: #886D4D;">เกี่ยวกับ</span>เรา
</h1>

<br>
<br>

<div class="ui stackable container" style="padding-left: 0%; padding-right: 0%;">
	<div class="ui form " style="margin-bottom: 1rem;">
		<div class="two fields">
			<div class="ten wide field">
				<img class="ui centered medium image" src="<?php echo url('').'/themes/image/sds.png'; ?>">
			</div>
			<div class="six wide field">
				<h2>Gingerbread House</h2>
				<p>“Gingerbread House ดินแดนแห่งความสุข เป็นโครงการ
					สถานที่พักผ่อนในดินแดนแห่งเทพนิยาย ที่ซึ่งท่านและสุนัขของ
					ท่านสามารถที่จะทำกิจกรรมร่วมกันได้ เป็นที่พบปะสังสรรค์ของ
					กลุ่มคนรักสุนัข ในบรรยากาศที่เป็นธรรมชาติแฝงด้วย”</p>
				<button class="ui teal button" style="border-radius: 30px;background-color: #886D4D;color: #fff" type="submit" onclick="window.location.href = '/gallery';">เพิ่มเติม</button>
			</div>
		</div>
	</div>
</div>


<!-- Data -->
<input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
<div id='ajax-center-url' data-url="<?php echo \URL::route('font.ajax_center.post');?>"></div>