<br>
<br>
<h1 class="ui header" style="text-align: center;">
    รายการจอง
</h1>
<br>

<div class="ui stackable" style="padding-left: 2rem;padding-right: 2rem;margin-top: 4rem;">
	<div class="ui form">
		<div class="fields">
            <div class="sixteen wide field">
                <table class="ui blue table" id="TBL_onlinelist">
                    <thead>
                        <tr>
                            <th>หมายเลขการจอง</th>
                            <th>จำนวนห้องพัก</th>
                            <th>วัน-เวลา ที่จอง</th>
                            <th>การชำระเงิน</th>
                            <th>สถานะการจอง</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody> 
                        <?php if(!empty($reserves)): ?>
                            <?php foreach ($reserves as $key => $reserve):?>
                                <tr>
                                    <td>
                                        <?php echo $reserve->reserve_number ?>
                                    </td>
                                    <td>
                                        <?php echo $reserve->typeroom_id ?>
                                    </td>
                                    <td>
                                        <?php echo DateThai($reserve->reserve_data_time, true, true) ?>
                                    </td>
                                    <td>
                                        <?php echo empty($reserve->pay_id) ? 'รอชำระเงิน' : 'ชำระเงินแล้ว'; ?>
                                    </td>
                                    <td>
                                        <?php 
                                            $lb_color = "";
                                            if($reserve->reserve_status == "รอชำระเงิน"){
                                                $lb_color = "red";
                                            }else if($reserve->reserve_status == "รอตรวจสอบการชำระเงิน"){
                                                $lb_color = "orange";
                                            }else if($reserve->reserve_status == "การจองสำเร็จ"){
                                                $lb_color = "green";
                                            }else if($reserve->reserve_status == "อยู่ระหว่างการใช้งาน"){ 
                                                $lb_color = "blue";
                                            }else if($reserve->reserve_status == "เสร็จสิ้น"){ 
                                                $lb_color = "teal";
                                            }
                                        ?>
                                        
                                        <a class="ui <?php echo $lb_color; ?> label"><?php echo $reserve->reserve_status ?></a>
                                    </td>
                                    <td>
                                        <button class="ui small teal button btn-online-detail" type="submit" onclick="window.location.href = '<?php echo '/bookingdetail/'.$reserve->reserve_id ; ?>';" data-id="<?php echo $reserve->reserve_id; ?>">รายละเอียด</button>
                                        <?php if($reserve->reserve_id == 'รอชำระเงิน'): ?>
                                            <button class="ui violet button btn-view" data-id="<?php echo $reserve->reserve_id; ?>" onclick="getModalPayment(<?php echo $reserve->reserve_id; ?>)" type="submit">ชำระเงิน</button>
                                        <?php endif ?>
                                    </td>
                                </tr> 
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>

                <!-- แสดงตัวเลข page -->
            </div>
        </div>
	</div>
</div>