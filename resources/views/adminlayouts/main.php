<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GINGERBREAD HOUSE</title>

    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="/themes/image/Favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/themes/image/Favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/themes/image/Favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/themes/image/Favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/themes/image/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/themes/image/Favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/themes/image/Favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/themes/image/Favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/themes/image/Favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/themes/image/Favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/themes/image/Favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/themes/image/Favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/themes/image/Favicon/favicon-16x16.png">
    <link rel="manifest" href="/themes/image/Favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/themes/image/Favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <link rel="stylesheet" href="/themes/semantic/semantic.css">
    <link rel="stylesheet" href="/themes/css/style.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/animate.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/sweetalert2.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/dataTables.semanticui.min.css">
    <link href='/themes/fullcalendar/core/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/timeline/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/list/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/resource-timeline/main.css' rel='stylesheet' />
    

    <?php if (file_exists(base_path().'/public/css/'.helperGetModule().'/'.helperGetAction().'.css')): ?>
        <link rel="stylesheet" href="<?php echo '/css/'.helperGetModule().'/'.helperGetAction().'.css?t='.time() ?>">
    <?php endif ?>

    <style type="text/css">
        .ui.menu .active.item:hover, .ui.vertical.menu .active.item:hover{
            background-color: #886d4d8a;
        }
        .ui.vertical.menu .active.item{
            background: #886D4D;
            color:#fff;
        }
    </style>
</head>


<body style="background-color: #e2e2e2;">

    <script src="/themes/js/jquery.min.js"></script>
    <script src="/themes/js/current-device.min.js"></script>
    <script src="/themes/semantic/semantic.min.js"></script>
    <script src="/themes/js/calendarTH.js"></script>
    <script src="/themes/js/moment.js"></script>
    <script src="/themes/js/moment-with-locales.js"></script>
    <script src='/themes/fullcalendar/core/main.js'></script>
    <script src='/themes/fullcalendar/core/locales-all.min.js'></script>
    <script src='/themes/fullcalendar/timeline/main.js'></script>
    <script src='/themes/fullcalendar/list/main.js'></script>
    <script src='/themes/fullcalendar/interaction/main.js'></script>
    <script src='/themes/fullcalendar/resource-common/main.js'></script>
    <script src='/themes/fullcalendar/resource-timeline/main.js'></script>
    <script src='/themes/js/sweetalert2.js'></script>
    <script src='/themes/js/lodash.js'></script>
    <script src='/themes/js/jquery.dataTables.min.js'></script>
    <script src='/themes/js/dataTables.semanticui.min.js'></script>
    <script src="/themes/js/angular.min.js"></script>
    <script src="/themes/js/angular-route.js"></script>
    <script src="/themes/js/angular-animate.js"></script>
    <script src="/themes/js/cleave/cleave.min.js"></script>
    <script src="/themes/js/cleave/addons/cleave-phone.th.js"></script>
    <script src="/themes/ckeditor/ckeditor.js"></script>

    <?php if (file_exists(base_path().'/public/js/'.helperGetModule().'/'.helperGetAction().'.js')): ?>
        <script src="<?php echo '/js/'.helperGetModule().'/'.helperGetAction().'.js?t='.time() ?>"></script>
    <?php endif ?>

        <!-- Header.php -->
        <?php echo view('adminlayouts.headbar', compact('user_Fname', 'user_Lname', 'username', 'user_type', 'user_id'));?>

        <br>
        <br>
        <br>
        <br>
        
        <div class="ui grid">
            <div class="three wide column" style="height: auto;background:#ffffff;border: unset;padding-top: 10px;padding-right: 0px;">

                <!-- ส่วนที่เรียกใช้งานเมนูด้านข้าง -->
                <?php echo view('adminlayouts.sidebar', compact('user_Fname', 'user_Lname', 'username', 'user_type', 'user_id'));?>
                <!-- ส่วนที่เรียกใช้งานเมนูด้านข้าง -->

            </div>
            <div class="thirteen wide stretched column">
                <!-- ส่วนที่เรียกหน้า view อื่นๆมาแสดง -->
                <?php echo view($page, $data);?> 
                <!-- ส่วนที่เรียกหน้า view อื่นๆมาแสดง -->


                <br>
                <br>

                <div class="ui basic segment" style="text-align: center;">
                    <p>Copyright © 2020 GINGERBREAD HOUSE</p>
                </div>

            </div>
        </div>

    <!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id="logout-url"  data-url="<?php echo \URL::route('auth.logout.get');?>">

<script type="text/javascript">
    $(function(){


        $('.btn-logout').on('click', function(){
            Swal.fire({
                title: "คุณต้องการออกจากระบบหรือไม่ ?",
                showCancelButton: true,
                confirmButtonColor: "#59B855",
                cancelButtonText: "ยกเลิก",
                cancelButtonColor: "",
                confirmButtonText: "ตกลง"
            }).then(result => {
                if (result.value) {
                    logout_url = $('#logout-url').data('url');
                    console.log(logout_url);
                    window.location = logout_url;
                }
            });


        });

    });
</script>
</body>

</html>