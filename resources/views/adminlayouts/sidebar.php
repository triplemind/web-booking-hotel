
<div class="ui massive compact vertical labeled icon fluid menu" style="box-shadow: 0 1px 2px 0 rgb(255 255 255 / 15%);border-radius: 0rem;border: 1px solid rgb(255 255 255 / 15%);">
<?php //sd($user_type); ?>
    <?php if(empty($user_type)): ?>
        <a class="item <?php echo helperGetModule() == 'dashboard' ? 'active' : ''; ?>" href="<?php echo \URL::route('dashboard.index.get'); ?>">
            <i class="home icon"></i>
            หน้าหลัก
        </a>
    <?php elseif(!empty($user_type)): ?>
        <?php if($user_type == 'ผู้ดูแลระบบ'): ?>
            <a class="item <?php echo helperGetModule() == 'dashboard' ? 'active' : ''; ?>" href="<?php echo \URL::route('dashboard.index.get'); ?>">
                <i class="home icon"></i>
                หน้าหลัก
            </a>
            <a class="item <?php echo helperGetModule() == 'user' ? 'active' : ''; ?>" href="<?php echo \URL::route('user.index.get'); ?>">
                <i class="user alternate icon"></i>
                จัดการผู้ใช้งานระบบ
            </a>
            <a class="item <?php echo helperGetModule() == 'room' ? 'active' : ''; ?>" href="<?php echo \URL::route('room.index.get'); ?>">
                <i class="bed icon"></i>
                จัดการห้องพัก
            </a>
            <!-- <a class="item <?php echo helperGetModule() == 'roomtype' ? 'active' : ''; ?>" href="<?php echo \URL::route('roomtype.index.get'); ?>">
                <i class="concierge bell icon"></i>
                จัดการประเภทห้องพัก
            </a> -->
            <a class="item <?php echo helperGetModule() == 'pettype' ? 'active' : ''; ?>" href="<?php echo \URL::route('pettype.index.get'); ?>">
                <i class="paw icon"></i>
                จัดการประเภทสัตว์เลี้ยง
            </a>
            <!-- <a class="item <?php //echo helperGetModule() == 'report' ? 'active' : ''; ?>" href="<?php //echo \URL::route('report.index.get'); ?>">
                <i class="clipboard outline icon"></i>
                รายงาน
            </a>  -->
        <?php elseif($user_type == 'ผู้ใช้ทั่วไป'): ?>
            <a class="item <?php echo helperGetModule() == 'dashboard' ? 'active' : ''; ?>" href="<?php echo \URL::route('dashboard.index.get'); ?>">
                <i class="home icon"></i>
                หน้าหลัก
            </a>
            
        <?php endif ?>
    <?php endif ?>
</div>