<div class="ui basic segment">
    <br>
    <div class="ui unstackable three column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="bed icon"></i>
                <div class="content">
                    จัดการห้องพัก
                </div>
            </h3>
        </div>

        <div class="three wide column">
            <div class="field">
                <button class="ui fluid inverted blue button" onclick="window.location.href = '/admin/roomtype';" type="submit" style="border-radius: 30px;">จัดการประเภทห้องพัก</button>
            </div>
        </div>

        <div class="three wide right floated column">
            <div class="field">
                <button class="ui fluid inverted green button btn-addroom" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลห้องพัก</button>
            </div>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <br>
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui blue table" id="TBL_Room">
                    <thead>
                        <tr>
                            <th>รหัสห้องพัก</th>
                            <th>ชื่อห้องพัก</th>
                            <th>ประเภทห้องพัก</th>
                            <th>สถานะ</th>
                            <th>สถานะเปิดใช้งาน</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($getRooms)): ?>
                            <?php foreach ($getRooms as $key => $getRoom):?>
                                <tr>
                                    <!-- <td class="wide two"><?php echo "00".$getRoom->rm_id ?></td> -->
                                    <td class="wide two"><?php echo $getRoom->room_number ?></td>
                                    <td class="wide two"><?php echo empty($getRoom->rm_name) ? '-' : $getRoom->rm_name; ?></td>
                                    <td class="wide two"><?php echo empty($getRoom->roomtype) ? '-' : $getRoom->roomtype->type_nameroom ?></td>
                                    <?php 
                                        $usestatus_txt = ""; 
                                        $usecolor_txt = ""; 

                                        $status_txt = ""; 
                                        $color_txt = ""; 
                                             
                                        if($getRoom->rest_status == 11){
                                            $status_txt = "ว่าง"; 
                                            $color_txt = "teal"; 
                                        }else if($getRoom->rest_status == 22){
                                            $status_txt = "ไม่ว่าง"; 
                                            $color_txt = "red"; 
                                        }

                                        if($getRoom->isusable == true){
                                            $usestatus_txt = "ใช้งานได้"; 
                                            $usecolor_txt = "teal"; 
                                        }else if($getRoom->isusable == false){
                                            $usestatus_txt = "ปิดใช้งานชั่วคราว"; 
                                            $usecolor_txt = "red";
                                        }
                                           
                                    ?>
                                    <td class="wide two"><a class="ui <?php echo $color_txt ?> label"><?php echo $status_txt ?></a></td>
                                    <td class="wide two"><a class="ui <?php echo $usecolor_txt ?> label"><?php echo $usestatus_txt ?></a></td>
                                    <td class="wide two">
                                        <i class="large edit outline icon editroom" data-id="<?php echo $getRoom->rm_id; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deleteroom" data-id="<?php echo $getRoom->rm_id; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('room.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('room.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('room.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('room.delete.post');?>"></div>


<div class="ui medium modal" id="addRoomModal">
    <div class="header">เพิ่มข้อมูลห้องพัก</div>
    <div class="content">
        <div class="ui medium form">
            <div class="field">
                <label>รหัสห้องพัก</label>
                <input type="text" placeholder="" name="room_number" id="room_number" value="<?php echo "00".$roomcount ?>" readonly>
            </div>
            <div class="field">
                <label>ชื่อห้องพัก</label>
                <input type="text" placeholder="" name="rm_name" id="rm_name" value="" >
            </div>
            <div class="two fields">
                <div class="field">
                    <label>ประเภทห้องพัก</label>
                    <select class="ui selection dropdown" name="typeroom" id="typeroom">
                        <option selected>--กรุณาเลือกประเภทห้องพัก--</option>
                        <?php if($getRoomTypes->count() != 0): ?>
                            <?php foreach ($getRoomTypes as $key => $getRoomType): ?>
                                <option value="<?php echo $getRoomType->typeroom_id; ?>"><?php echo $getRoomType->type_nameroom; ?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                </div>
                <div class="field">
                    <label>สถานะ</label>
                    <select class="ui selection dropdown" name="rest_status" id="rest_status">
                        <option selected>--กรุณาเลือกสถานะ--</option>
                        <option value="11">ว่าง</option>
                        <option value="22">ไม่ว่าง</option>
                    </select>
                </div>
            </div>
            <div class="field">
                <label>สถานะเปิดใช้งาน</label>
                <select class="ui selection dropdown" name="rest_statususe" id="rest_statususe">
                    <option selected>--กรุณาเลือกสถานะเปิดใช้งาน--</option>
                    <option value="1">ใช้งานได้</option>
                    <option value="0">ปิดใช้งานชั่วคราว</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="EditRoomModal">
    <div class="header">แก้ไขข้อมูลห้องพัก</div>
    <div class="content">
        <div class="ui medium form">
            <div class="field">
                <label>รหัสห้องพัก</label>
                <input type="text" placeholder="" name="ed_room_number" id="ed_room_number" readonly>
            </div>
            <div class="field">
                <label>ชื่อห้องพัก</label>
                <input type="text" placeholder="" name="ed_rm_name" id="ed_rm_name">
            </div>
            <div class="two fields">
                <div class="field">
                    <label>ประเภทห้องพัก</label>
                    <select class="ui selection dropdown" name="ed_typeroom" id="ed_typeroom">
                        <option selected>--กรุณาเลือกประเภทห้องพัก--</option>
                        <?php if($getRoomTypes->count() != 0): ?>
                            <?php foreach ($getRoomTypes as $key => $getRoomType): ?>
                                <option value="<?php echo $getRoomType->typeroom_id; ?>"><?php echo $getRoomType->type_nameroom; ?></option>
                            <?php endforeach ?>
                        <?php endif ?>
                    </select>
                </div>
                <div class="field">
                    <label>สถานะ</label>
                    <select class="ui selection dropdown" name="ed_rest_status" id="ed_rest_status">
                        <option selected>--กรุณาเลือกสถานะ--</option>
                        <option value="11">ว่าง</option>
                        <option value="22">ไม่ว่าง</option>
                    </select>
                </div>
            </div>
            <div class="field">
                <label>สถานะเปิดใช้งาน</label>
                <select class="ui selection dropdown" name="ed_rest_statususe" id="ed_rest_statususe">
                    <option selected>--กรุณาเลือกสถานะเปิดใช้งาน--</option>
                    <option value="1">ใช้งานได้</option>
                    <option value="0">ปิดใช้งานชั่วคราว</option>
                </select>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>