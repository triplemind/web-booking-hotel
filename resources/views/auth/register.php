<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GINGERBREAD HOUSE</title>

    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="/themes/image/Favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/themes/image/Favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/themes/image/Favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/themes/image/Favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/themes/image/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/themes/image/Favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/themes/image/Favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/themes/image/Favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/themes/image/Favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/themes/image/Favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/themes/image/Favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/themes/image/Favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/themes/image/Favicon/favicon-16x16.png">
    <link rel="manifest" href="/themes/image/Favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/themes/image/Favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <link rel="stylesheet" href="/themes/semantic/semantic.css">
    <link rel="stylesheet" href="/themes/css/style.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/animate.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/sweetalert2.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/dataTables.semanticui.min.css">
    <link href='/themes/fullcalendar/core/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/timeline/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/list/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/resource-timeline/main.css' rel='stylesheet' />
    

    <?php if (file_exists(base_path().'/public/css/'.helperGetModule().'/'.helperGetAction().'.css')): ?>
        <link rel="stylesheet" href="<?php echo '/css/'.helperGetModule().'/'.helperGetAction().'.css?t='.time() ?>">
    <?php endif ?>

    <style type="text/css">
        .ui.menu .active.item:hover, .ui.vertical.menu .active.item:hover{
            background-color: rgb(127 190 24 / 42%);
        }
        .ui.vertical.menu .active.item{
            background: rgb(127 190 24);
            color:#fff;
        }
    </style>
</head>


<body style="background-color: #fff;">

    <script src="/themes/js/jquery.min.js"></script>
    <script src="/themes/js/current-device.min.js"></script>
    <script src="/themes/semantic/semantic.min.js"></script>
    <script src="/themes/js/calendarTH.js"></script>
    <script src="/themes/js/moment.js"></script>
    <script src="/themes/js/moment-with-locales.js"></script>
    <script src='/themes/fullcalendar/core/main.js'></script>
    <script src='/themes/fullcalendar/core/locales-all.min.js'></script>
    <script src='/themes/fullcalendar/timeline/main.js'></script>
    <script src='/themes/fullcalendar/list/main.js'></script>
    <script src='/themes/fullcalendar/interaction/main.js'></script>
    <script src='/themes/fullcalendar/resource-common/main.js'></script>
    <script src='/themes/fullcalendar/resource-timeline/main.js'></script>
    <script src='/themes/js/sweetalert2.js'></script>
    <script src='/themes/js/lodash.js'></script>
    <script src='/themes/js/jquery.dataTables.min.js'></script>
    <script src='/themes/js/dataTables.semanticui.min.js'></script>
    <script src="/themes/js/angular.min.js"></script>
    <script src="/themes/js/angular-route.js"></script>
    <script src="/themes/js/angular-animate.js"></script>
    <script src="/themes/js/cleave/cleave.min.js"></script>
    <script src="/themes/js/cleave/addons/cleave-phone.th.js"></script>
    <script src="/themes/ckeditor/ckeditor.js"></script>

    <?php if (file_exists(base_path().'/public/js/'.helperGetModule().'/'.helperGetAction().'.js')): ?>
        <script src="<?php echo '/js/'.helperGetModule().'/'.helperGetAction().'.js?t='.time() ?>"></script>
       
        <?php //sd('/js/'.helperGetModule().'/'.helperGetAction().'.js?t='); ?>
    <?php endif ?>

    <div class="ui" id="example1">
        <div class="ui grid" style="">
            <div class="computer only row">
                <div class="column">
                    <div class="ui sticky">
                        <div class="ui large menu" style="background-color: #fff;">
                            <a class="<?php echo (helperGetAction() == 'index') ? 'active' : '' ?> item" href="/">
                                <span style="font-size: 24px;color: #403535;">GINGERBREAD <span style="color: #886D4D;">HOUSE</span></span>
                            </a>
                            <a class="<?php echo (helperGetAction() == 'room') ? 'active' : '' ?> item" href="/" style="color:#000;">
                                หน้าหลัก
                            </a>
                            <a class="<?php echo (helperGetAction() == 'roomtype') ? 'active' : '' ?> item" href="/roomtype" style="color:#000;">
                                ประเภทห้องพัก
                            </a>
                            <a class="<?php echo (helperGetAction() == 'activity') ? 'active' : '' ?> item" href="/activity" style="color:#000;">
                                เกี่ยวกับเรา
                            </a>
                            <a class="<?php echo (helperGetAction() == 'gallery') ? 'active' : '' ?> item" href="/gallery" style="color:#000;">
                                ติดต่อเรา
                            </a>
                            <div class="right menu">
                                <?php if(empty($user_type)): ?>
                                    <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/register" style="color:#000;">
                                        ลงทะเบียน
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/login" style="color:#000;">
                                        เข้าสู่ระบบ
                                    </a>
                                <?php else: ?>
                                    <div class="item">
                                        <i class="user circle outline icon" style="margin-left: 1em;cursor: pointer;"></i>
                                        <span class="ui small" style="margin-left: 1em"><?php echo (empty($username))  ? '' : $username; ?></span>
                                        <i class="sign out alternate icon btn-logout" style="margin-left: 1em;cursor: pointer;"></i>
                                    </div>
                                <?php endif ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="tablet mobile only row">
                <div class="column">
                    <div class="ui top fixed large menu" style="<?php echo (helperGetAction() == 'index') ? 'background-color: #fff;' : 'background-color: #FEE265;' ?>">
                        <div class="menu">
                            <div class="ui simple dropdown item">
                                <i class="sidebar icon"></i>
                                <div class="menu">
                                    <a class="<?php echo (helperGetAction() == 'room') ? 'active' : '' ?> item" href="/" style="color:#000;">
                                        หน้าหลัก
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'roomtype') ? 'active' : '' ?> item" href="/roomtype" style="color:#000;">
                                        ประเภทห้องพัก
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'activity') ? 'active' : '' ?> item" href="/activity" style="color:#000;">
                                        เกี่ยวกับเรา
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'gallery') ? 'active' : '' ?> item" href="/gallery" style="color:#000;">
                                        ติดต่อเรา
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="center menu">
                            <a class="item" href="/">
                                <span style="font-size: 24px;color: #403535;">GINGERBREAD <span style="color: #886D4D;">HOUSE</span></span>
                            </a>
                        </div>
                        <div class="right menu">
                            <?php if(empty($user_type)): ?>
                                <div class="ui simple dropdown item">
                                    <i class="user icon"></i>
                                    <div class="menu">
                                        <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/register" style="color:#000;">
                                            ลงทะเบียน
                                        </a>
                                        <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/login" style="color:#000;">
                                            เข้าสู่ระบบ
                                        </a>

                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="item">
                                    <i class="user circle outline icon" style="margin-left: 1em;cursor: pointer;"></i>
                                    <span class="ui small" style="margin-left: 1em"><?php echo (empty($username))  ? '' : $username; ?></span>
                                    <i class="sign out alternate icon btn-logout" style="margin-left: 1em;cursor: pointer;"></i>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui bottom attached  body-background-image" style="height: auto;border: unset;margin-bottom:unset;">
            <!-- container -->
            <br>
            <br>
            <br>
            <div class="ui container" style="padding-left: 0%;padding-right: 0%;">
                <div class="ui two column very relaxed stackable grid">
                    <div class="eigth wide middle aligned content column">
                        <img class="ui centered large image" src="<?php echo url('').'/themes/image/Group21.jpg'; ?>">
                    </div>

                    <div class="eigth wide middle aligned content column">
                        <h2 style="text-align: center;color:#000;">ลงทะเบียน</h2>
                        <form class="ui form" action="javascript:void(0);">
                            <div class="two required fields">
                                <div class="fluid field" style="max-width: unset;">
                                    <label style="color:#000;">ชื่อ</label>
                                    <div class="ui fluid left icon input">
                                        <input type="text" placeholder="ชื่อ" id="firstname" name="firstname" style="border-radius: 20px;">
                                        <!-- <i class="user icon"></i> -->
                                    </div>
                                </div>
                                <div class="fluid field" style="max-width: unset;">
                                    <label style="color:#000;">นามสกุล</label>
                                    <div class="ui fluid left icon input">
                                        <input type="text" placeholder="นามสกุล" id="lastname" name="lastname" style="border-radius: 20px;">
                                        <!-- <i class="user icon"></i> -->
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="two fields"> -->
                            <div class="fluid required field" style="max-width: unset;">
                                <label style="color:#000;">ชื่อผู้ใช้งาน</label>
                                <div class="ui fluid left icon input">
                                    <input type="text" placeholder="ชื่อผู้ใช้งาน" id="username" name="username" style="border-radius: 20px;">
                                    <!-- <i class="user icon"></i> -->
                                </div>
                            </div>
                            <div class="fluid required field" style="max-width: unset;">
                                <label style="color:#000;">อีเมล์</label>
                                <div class="ui fluid left icon input">
                                    <input type="text" placeholder="อีเมล์" id="email" name="email" style="border-radius: 20px;">
                                    <!-- <i class="user icon"></i> -->
                                </div>
                            </div>
                            <!-- </div> -->
                            <div class="two fields">
                                <div class="fluid required field" style="max-width: unset;">
                                    <label style="color:#000;">เบอร์โทรศัพท์</label>
                                    <div class="ui fluid left icon input">
                                        <input type="text" placeholder="เบอร์โทรศัพท์" id="tel" name="tel" maxlength="10" style="border-radius: 20px;">
                                        <!-- <i class="user icon"></i> -->
                                    </div>
                                </div>
                                <div class="fluid field" style="max-width: unset;">
                                    <label style="color:#000;">Line ID</label>
                                    <div class="ui fluid left icon input">
                                        <input type="text" placeholder="Line ID" id="line_id" name="line_id" style="border-radius: 20px;">
                                        <i class="at icon"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="two required fields">
                                <div class="fluid field" style="max-width: unset;">
                                    <label style="color:#000;">รหัสผ่านใหม่</label>
                                    <div class="ui fluid left icon input">
                                        <input type="password" placeholder="รหัสผ่าน" id="password" name="password" style="border-radius: 20px;">
                                    </div>
                                </div>
                                <div class="fluid field" style="max-width: unset;">
                                    <label style="color:#000;">ยืนยันรหัสผ่าน</label>
                                    <div class="ui fluid left icon input">
                                        <input type="password" placeholder="ยืนยันรหัสผ่าน" id="cfpassword" name="cfpassword" style="border-radius: 20px;">
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="fluid field" style="max-width: unset;">
                                <div class="ui hidden" id="cf_txt"></div>
                            </div>
                            <br>
                            <div class="fluid field" style="max-width: unset;text-align: center;">
                                <div class="ui button" id="btn-register" type="submit" style="border-radius: 30px;background-color: #886D4D;color: #fff">ยืนยันลงทะเบียน</div>
                            </div>
                            <div class="field" style="text-align:center">
                                <a href="/auth/login" style="color: #3c3c3c;">เป็นสมาชิกอยู่แล้ว สามารถลงชื่อเข้าใช้งานได้.</a>
                            </div>


                            <div class="ui error message"></div>

                        </form>
                    </div>
                </div>
            </div>
            <br>
            <br>
        </div>

    </div>


    <!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id="register-url"  data-url="<?php echo \URL::route('auth.register.post');?>">

<script type="text/javascript">
    let _cfpassswd_val = true;
    $(function(){

        $('.ui.form').form({
            fields: {
                firstname: {
                    identifier: 'firstname',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please Enter First name'
                        }
                    ]
                },
                lastname: {
                    identifier: 'lastname',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please Enter Last name'
                        }
                    ]
                },
                username: {
                    identifier: 'username',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please Enter Username'
                        }
                    ]
                },
                email: {
                    identifier: 'email',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please Enter Email'
                        }
                    ]
                },
                tel: {
                    identifier: 'tel',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please Enter Phone number'
                        },
                        {
                            type   : 'maxLength[10]',
                            prompt : 'Your phone number cannot be greater than {ruleValue} characters'
                        },
                        {
                            type   : 'minLength[10]',
                            prompt : 'Your phone number must be at least {ruleValue} characters'
                        }
                    ]
                },
                password: {
                    identifier: 'password',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please Enter Password'
                        },
                        {
                            type   : 'minLength[6]',
                            prompt : 'Your password must be at least {ruleValue} characters'
                        }
                    ]
                },
                cfpassword: {
                    identifier: 'cfpassword',
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Please Confirm Password'
                        }
                    ]
                },
              
            },
        });


        // ตรวจสอบรหัสผ่านกับ confirm รหัสผ่าน
        $('#password, #cfpassword').on('keyup', function () {
            $('#cf_txt').removeClass('hidden');

            if ($('#password').val() == $('#cfpassword').val()) {
                // red message
                _cfpassswd_val = true;
                $('#cf_txt').removeClass('message');
                $('#cf_txt').removeClass('red');
                $('#cf_txt').addClass('green');
                $('#cf_txt').addClass('message');
                $('#cf_txt').text('รหัสผ่านตรงกัน');
                
            } else {
                _cfpassswd_val = false;
                $('#cf_txt').removeClass('message');
                $('#cf_txt').removeClass('green');
                $('#cf_txt').addClass('red');
                $('#cf_txt').addClass('message');
                $('#cf_txt').text('รหัสผ่านไม่ตรงกัน');
               
            }
        });

        //กดบันทึก เพื่อส่งข้อมูลที่กรอกไป save 
        $('#btn-register').on('click', function(){
            addNewUser();
            console.log($('.ui.form').form('is valid'));

        });
    });

    // function ที่ส่งข้อมูลไป save
    function addNewUser() {
        var firstname   = $('#firstname').val();
        var lastname    = $('#lastname').val();
        var username    = $('#username').val();
        var email       = $('#email').val();
        var tel         = $('#tel').val();
        var line_id     = $('#line_id').val();
        var password    = $('#password').val();

        var add_url         = $('#register-url').data('url');
        var data            = new FormData();

        data.append('username', username);
        data.append('password',password);
        data.append('firstname',firstname);
        data.append('lastname',lastname);
        data.append('email',email);
        data.append('user_type','ผู้ใช้ทั่วไป');
        data.append('line_id',line_id);
        data.append('tel',tel);

        // check format email
        var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

        console.log($('.ui.form').form('is valid'));

        // if ($('.ui.form').form('is valid')){
        if (testEmail.test(email) && _cfpassswd_val){
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: add_url,
                data: data,
                contentType: false,
                processData:false,
                cache: false,
                success: function(result) {
                    if(result.status == 'success'){
                        setTimeout(function(){ 
                            $("body").toast({
                                class: "success",
                                position: 'bottom right',
                                message: `บันทึกเสร็จสิ้น`
                            });
                            setTimeout(function(){ 
                                window.location.reload()
                            }, 1000);
                        });
                    } 

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.msg
                        });
                    }
                },
                error : function(error) {
                }
            });
        }else{
            if(!_cfpassswd_val){
                $("body").toast({
                    class: "error",
                    displayTime: 10000,
                    position: 'bottom right',
                    message: `กรุณาตรวจสอบรหัสผ่านของท่านอีกครั้ง`
                });
            }else{
                $("body").toast({
                    class: "error",
                    displayTime: 10000,
                    position: 'bottom right',
                    message: `รูปแบบอีเมลไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง`
                });
            }
        }

    }

</script>
</body>

</html>