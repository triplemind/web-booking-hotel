<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GINGERBREAD HOUSE</title>

    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="/themes/image/Favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/themes/image/Favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/themes/image/Favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/themes/image/Favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/themes/image/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/themes/image/Favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/themes/image/Favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/themes/image/Favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/themes/image/Favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/themes/image/Favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/themes/image/Favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/themes/image/Favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/themes/image/Favicon/favicon-16x16.png">
    <link rel="manifest" href="/themes/image/Favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/themes/image/Favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <link rel="stylesheet" href="/themes/semantic/semantic.css">
    <link rel="stylesheet" href="/themes/css/style.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/animate.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/sweetalert2.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/dataTables.semanticui.min.css">
    <link href='/themes/fullcalendar/core/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/timeline/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/list/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/resource-timeline/main.css' rel='stylesheet' />
    
    <!-- เรียกใช้งาน css ของแต่ละหน้า -->
    <?php if (file_exists(base_path().'/public/css/'.helperGetModule().'/'.helperGetAction().'.css')): ?>
        <link rel="stylesheet" href="<?php echo '/css/'.helperGetModule().'/'.helperGetAction().'.css?t='.time() ?>">
    <?php endif ?>
    <!-- เรียกใช้งาน css ของแต่ละหน้า -->

    <style type="text/css">
        .ui.menu .active.item:hover, .ui.vertical.menu .active.item:hover{
            background-color: rgb(127 190 24 / 42%);
        }
        .ui.vertical.menu .active.item{
            background: rgb(127 190 24);
            color:#fff;
        }
    </style>
</head>


<body style="background-color: #fff;">

    <script src="/themes/js/jquery.min.js"></script>
    <script src="/themes/js/current-device.min.js"></script>
    <script src="/themes/semantic/semantic.min.js"></script>
    <script src="/themes/js/calendarTH.js"></script>
    <script src="/themes/js/moment.js"></script>
    <script src="/themes/js/moment-with-locales.js"></script>
    <script src='/themes/fullcalendar/core/main.js'></script>
    <script src='/themes/fullcalendar/core/locales-all.min.js'></script>
    <script src='/themes/fullcalendar/timeline/main.js'></script>
    <script src='/themes/fullcalendar/list/main.js'></script>
    <script src='/themes/fullcalendar/interaction/main.js'></script>
    <script src='/themes/fullcalendar/resource-common/main.js'></script>
    <script src='/themes/fullcalendar/resource-timeline/main.js'></script>
    <script src='/themes/js/sweetalert2.js'></script>
    <script src='/themes/js/lodash.js'></script>
    <script src='/themes/js/jquery.dataTables.min.js'></script>
    <script src='/themes/js/dataTables.semanticui.min.js'></script>
    <script src="/themes/js/angular.min.js"></script>
    <script src="/themes/js/angular-route.js"></script>
    <script src="/themes/js/angular-animate.js"></script>
    <script src="/themes/js/cleave/cleave.min.js"></script>
    <script src="/themes/js/cleave/addons/cleave-phone.th.js"></script>
    <script src="/themes/ckeditor/ckeditor.js"></script>

    <!-- เรียกใช้งาน javascript ของแต่ละหน้า -->
    <?php if (file_exists(base_path().'/public/js/'.helperGetModule().'/'.helperGetAction().'.js')): ?>
        <script src="<?php echo '/js/'.helperGetModule().'/'.helperGetAction().'.js?t='.time() ?>"></script>
    <?php endif ?>
    <!-- เรียกใช้งาน javascript ของแต่ละหน้า -->

    <div class="ui" id="example1">
        <div class="ui grid" style="">
            <div class="computer only row">
                <div class="column">
                    <div class="ui sticky">
                        <div class="ui large menu" style="background-color: #fff;">
                            <a class="<?php echo (helperGetAction() == 'index') ? 'active' : '' ?> item" href="/">
                                <span style="font-size: 24px;color: #403535;">GINGERBREAD <span style="color: #886D4D;">HOUSE</span></span>
                            </a>
                            <a class="<?php echo (helperGetAction() == 'room') ? 'active' : '' ?> item" href="/" style="color:#000;">
                                หน้าหลัก
                            </a>
                            <a class="<?php echo (helperGetAction() == 'roomtype') ? 'active' : '' ?> item" href="/roomtype" style="color:#000;">
                                ประเภทห้องพัก
                            </a>
                            <a class="<?php echo (helperGetAction() == 'activity') ? 'active' : '' ?> item" href="/activity" style="color:#000;">
                                เกี่ยวกับเรา
                            </a>
                            <a class="<?php echo (helperGetAction() == 'gallery') ? 'active' : '' ?> item" href="/gallery" style="color:#000;">
                                ติดต่อเรา
                            </a>
                            <div class="right menu">
                                <?php if(empty($user_type)): ?>
                                    <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/register" style="color:#000;">
                                        ลงทะเบียน
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/login" style="color:#000;">
                                        เข้าสู่ระบบ
                                    </a>
                                <?php else: ?>
                                    <div class="item">
                                        <i class="user circle outline icon" style="margin-left: 1em;cursor: pointer;"></i>
                                        <span class="ui small" style="margin-left: 1em"><?php echo (empty($username))  ? '' : $username; ?></span>
                                        <i class="sign out alternate icon btn-logout" style="margin-left: 1em;cursor: pointer;"></i>
                                    </div>
                                <?php endif ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="tablet mobile only row">
                <div class="column">
                    <div class="ui top fixed large menu" style="<?php echo (helperGetAction() == 'index') ? 'background-color: #fff;' : 'background-color: #FEE265;' ?>">
                        <div class="menu">
                            <div class="ui simple dropdown item">
                                <i class="sidebar icon"></i>
                                <div class="menu">
                                    <a class="<?php echo (helperGetAction() == 'room') ? 'active' : '' ?> item" href="/" style="color:#000;">
                                        หน้าหลัก
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'roomtype') ? 'active' : '' ?> item" href="/roomtype" style="color:#000;">
                                        ประเภทห้องพัก
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'activity') ? 'active' : '' ?> item" href="/activity" style="color:#000;">
                                        เกี่ยวกับเรา
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'gallery') ? 'active' : '' ?> item" href="/gallery" style="color:#000;">
                                        ติดต่อเรา
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="center menu">
                            <a class="item" href="/">
                                <span style="font-size: 24px;color: #403535;">GINGERBREAD <span style="color: #886D4D;">HOUSE</span></span>
                            </a>
                        </div>
                        <div class="right menu">
                            <?php if(empty($user_type)): ?>
                                <div class="ui simple dropdown item">
                                    <i class="user icon"></i>
                                    <div class="menu">
                                        <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/register" style="color:#000;">
                                            ลงทะเบียน
                                        </a>
                                        <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/login" style="color:#000;">
                                            เข้าสู่ระบบ
                                        </a>

                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="item">
                                    <i class="user circle outline icon" style="margin-left: 1em;cursor: pointer;"></i>
                                    <span class="ui small" style="margin-left: 1em"><?php echo (empty($username))  ? '' : $username; ?></span>
                                    <i class="sign out alternate icon btn-logout" style="margin-left: 1em;cursor: pointer;"></i>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui bottom attached  body-background-image" style="height: auto;border: unset;margin-bottom:unset;">
                <!-- container -->
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="ui container" style="padding-left: 0%;padding-right: 0%;">
                    <!-- <div class="ui placeholder segment" style="background: #2f2f2f8f;"> -->
                        <div class="ui two column very relaxed stackable grid">
                            <div class="eigth wide middle aligned content column">
                                <img class="ui centered large image" src="<?php echo url('').'/themes/image/Group21.jpg'; ?>">
                            </div>
                            <div class="eigth wide middle aligned content column">
                                <h2 style="text-align: center;color:#000;">เปลี่ยนรหัสผ่าน</h2>
                                <div class="ui form">
                                    <div class="field">
                                        <label style="color:#000;">อีเมล์</label>
                                        <div class="ui fluid left icon input">
                                            <input type="text" name="email" placeholder="อีเมล์" style="border-radius: 20px;">
                                        </div>
                                    </div>
                                    <div class="field">
                                        <label style="color:#000;">รหัสผ่านใหม่</label>
                                        <div class="ui fluid left icon input">
                                            <input type="password" name="newpassword" class="new_pass" placeholder="รหัสผ่านใหม่" style="border-radius: 20px;">
                                        </div>
                                    </div>
                                    <div class="field">
                                        <label style="color:#000;">ยืนยันรหัสผ่านใหม่</label>
                                        <div class="ui fluid left icon input">
                                            <input type="password" name="cfnewpassword" class="confirm_new_pass" placeholder="ยืนยันรหัสผ่านใหม่" style="border-radius: 20px;">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="field" style="max-width: unset;">
                                        <div class="ui hidden" style="text-align:center" id="cf_txt"></div>
                                    </div>
                                    <br>
                                </div>
                                <div class="ui aligned grid">
                                    <div class="center aligned two column row">
                                        <div  class="column">
                                            <button class="fluid ui button" type="submit" id="btn_back" style="border-radius: 30px;">ยกเลิก</button>
                                        </div>
                                        <div class="column">
                                            <button class="fluid ui primary button" type="submit" id="btn-reset-pass"  style="border-radius: 30px;background-color: #886D4D;color: #fff;">บันทึก</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- </div> -->
                </div>
                    

                <br>
                <br>

        </div>

    </div>


    <!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id="forgot_password_url" data-url="<?php echo \URL::route('auth.forgotpassword.post'); ?>"></div>

<script type="text/javascript">
    $(function(){

        // ตรวจสอบรหัสผ่านกับ confirm รหัสผ่าน
        $('.new_pass, .confirm_new_pass').on('keyup', function () {
            $('#cf_txt').removeClass('hidden');

            if ($('.new_pass').val() == $('.confirm_new_pass').val()) {
                // red message
                _cfpassswd_val = true;
                $('#cf_txt').removeClass('message');
                $('#cf_txt').removeClass('red');
                $('#cf_txt').addClass('green');
                $('#cf_txt').addClass('message');
                $('#cf_txt').text('รหัสผ่านตรงกัน');
                
            } else {
                _cfpassswd_val = false;
                $('#cf_txt').removeClass('message');
                $('#cf_txt').removeClass('green');
                $('#cf_txt').addClass('red');
                $('#cf_txt').addClass('message');
                $('#cf_txt').text('รหัสผ่านไม่ตรงกัน');
               
            }
        });

        // ปุ่มสำหรับ reset password
        $('#btn-reset-pass').on('click', function(){
            // function สำหรับยิง ajax
            postForgotPassword();
        });

        $('#btn_back').on('click', function(){
            window.location.href = '/auth/login';
        });

    });

    //function สำหรับ get data และ ยิง ajax ส่งไปหา ForgotPasswordController
    function postForgotPassword(){
        var new_pass            = $('input[name=newpassword]').val();
        var confirm_new_pass    = $('input[name=cfnewpassword]').val();
        var email               = $('input[name=email]').val();
        var forgot_password_url = $('#forgot_password_url').data('url');

        // check format email
        var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

        if (testEmail.test(email)){
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: "POST",
                url: forgot_password_url,
                data: {
                    new_pass            : new_pass, 
                    confirm_new_pass    : confirm_new_pass,
                    email               : email,
                },
                success: function(result) {
                    if(result.status == 'success'){
                        // redirect ไปหน้า login
                        $('body').toast({
                            class: 'success',
                            position: 'bottom right',
                            message: `รหัสผ่านของท่านถูกเปลี่ยนเรียบร้อยแล้ว`
                        },window.location.href = "/auth/login");
                       
                    }
                    if(result.status == "error"){
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: result.msg,
                        });
                    }
                }
            });

        }else{
            Swal.fire({
                icon: 'error',
                title: 'รูปแบบอีเมลของคุณไม่ถูกต้อง',
                text: 'กรุณาตรวจสอบรูปแบบอีเมลของคุณอีกครั้ง',
            });
        }
        
    }


</script>
</body>

</html>