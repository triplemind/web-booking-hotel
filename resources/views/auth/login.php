<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GINGERBREAD HOUSE</title>

    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="/themes/image/Favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/themes/image/Favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/themes/image/Favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/themes/image/Favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/themes/image/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/themes/image/Favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/themes/image/Favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/themes/image/Favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/themes/image/Favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/themes/image/Favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/themes/image/Favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/themes/image/Favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/themes/image/Favicon/favicon-16x16.png">
    <link rel="manifest" href="/themes/image/Favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/themes/image/Favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <link rel="stylesheet" href="/themes/semantic/semantic.css">
    <link rel="stylesheet" href="/themes/css/style.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/animate.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/sweetalert2.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/dataTables.semanticui.min.css">
    <link href='/themes/fullcalendar/core/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/timeline/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/list/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/resource-timeline/main.css' rel='stylesheet' />
    

    <?php if (file_exists(base_path().'/public/css/'.helperGetModule().'/'.helperGetAction().'.css')): ?>
        <link rel="stylesheet" href="<?php echo '/css/'.helperGetModule().'/'.helperGetAction().'.css?t='.time() ?>">
    <?php endif ?>

    <style type="text/css">
        .ui.menu .active.item:hover, .ui.vertical.menu .active.item:hover{
            background-color: rgb(127 190 24 / 42%);
        }
        .ui.vertical.menu .active.item{
            background: rgb(127 190 24);
            color:#fff;
        }
        .body-background-image{
            position: absolute;
            z-index: -1;
            width: 100%;
            height: 100%;
            /*background-image: url('http://127.0.0.1:8001/themes/image/bgnow.png');*/
            background-size: cover;
            background-color: #fff;
            background: linear-gradient(to right, #bbad9b 54%, #ffffff 52%);
            /*opacity: 0.7;*/
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
    </style>
</head>
<!-- background-color: #fff;background-image: url('../themes/image/hotelBG.jpg'); -->

<body class="body-background-image">

    <script src="/themes/js/jquery.min.js"></script>
    <script src="/themes/js/current-device.min.js"></script>
    <script src="/themes/semantic/semantic.min.js"></script>
    <script src="/themes/js/calendarTH.js"></script>
    <script src="/themes/js/moment.js"></script>
    <script src="/themes/js/moment-with-locales.js"></script>
    <script src='/themes/fullcalendar/core/main.js'></script>
    <script src='/themes/fullcalendar/core/locales-all.min.js'></script>
    <script src='/themes/fullcalendar/timeline/main.js'></script>
    <script src='/themes/fullcalendar/list/main.js'></script>
    <script src='/themes/fullcalendar/interaction/main.js'></script>
    <script src='/themes/fullcalendar/resource-common/main.js'></script>
    <script src='/themes/fullcalendar/resource-timeline/main.js'></script>
    <script src='/themes/js/sweetalert2.js'></script>
    <script src='/themes/js/lodash.js'></script>
    <script src='/themes/js/jquery.dataTables.min.js'></script>
    <script src='/themes/js/dataTables.semanticui.min.js'></script>
    <script src="/themes/js/angular.min.js"></script>
    <script src="/themes/js/angular-route.js"></script>
    <script src="/themes/js/angular-animate.js"></script>
    <script src="/themes/js/cleave/cleave.min.js"></script>
    <script src="/themes/js/cleave/addons/cleave-phone.th.js"></script>
    <script src="/themes/ckeditor/ckeditor.js"></script>

    <?php if (file_exists(base_path().'/public/js/'.helperGetModule().'/'.helperGetAction().'.js')): ?>
        <script src="<?php echo '/js/'.helperGetModule().'/'.helperGetAction().'.js?t='.time() ?>"></script>
       
        <?php //sd('/js/'.helperGetModule().'/'.helperGetAction().'.js?t='); ?>
    <?php endif ?>

    <div class="ui" id="example1">
        <div class="ui grid" style="">
            <div class="computer only row">
                <div class="column">
                    <div class="ui sticky">
                        <div class="ui large menu" style="background-color: #fff;">
                            <a class="<?php echo (helperGetAction() == 'index') ? 'active' : '' ?> item" href="/">
                                <span style="font-size: 24px;color: #403535;">GINGERBREAD <span style="color: #886D4D;">HOUSE</span></span>
                            </a>
                            <a class="<?php echo (helperGetAction() == 'room') ? 'active' : '' ?> item" href="/" style="color:#000;">
                                หน้าหลัก
                            </a>
                            <a class="<?php echo (helperGetAction() == 'roomtype') ? 'active' : '' ?> item" href="/roomtype" style="color:#000;">
                                ประเภทห้องพัก
                            </a>
                            <a class="<?php echo (helperGetAction() == 'activity') ? 'active' : '' ?> item" href="/activity" style="color:#000;">
                                เกี่ยวกับเรา
                            </a>
                            <a class="<?php echo (helperGetAction() == 'gallery') ? 'active' : '' ?> item" href="/gallery" style="color:#000;">
                                ติดต่อเรา
                            </a>
                            <div class="right menu">
                                <?php if(empty($user_type)): ?>
                                    <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/register" style="color:#000;">
                                        ลงทะเบียน
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/login" style="color:#000;">
                                        เข้าสู่ระบบ
                                    </a>
                                <?php else: ?>
                                    <div class="item">
                                        <i class="user circle outline icon" style="margin-left: 1em;cursor: pointer;"></i>
                                        <span class="ui small" style="margin-left: 1em"><?php echo (empty($username))  ? '' : $username; ?></span>
                                        <i class="sign out alternate icon btn-logout" style="margin-left: 1em;cursor: pointer;"></i>
                                    </div>
                                <?php endif ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="tablet mobile only row">
                <div class="column">
                    <div class="ui top fixed large menu" style="<?php echo (helperGetAction() == 'index') ? 'background-color: #fff;' : 'background-color: #FEE265;' ?>">
                        <div class="menu">
                            <div class="ui simple dropdown item">
                                <i class="sidebar icon"></i>
                                <div class="menu">
                                    <a class="<?php echo (helperGetAction() == 'room') ? 'active' : '' ?> item" href="/" style="color:#000;">
                                        หน้าหลัก
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'roomtype') ? 'active' : '' ?> item" href="/roomtype" style="color:#000;">
                                        ประเภทห้องพัก
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'activity') ? 'active' : '' ?> item" href="/activity" style="color:#000;">
                                        เกี่ยวกับเรา
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'gallery') ? 'active' : '' ?> item" href="/gallery" style="color:#000;">
                                        ติดต่อเรา
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="center menu">
                            <a class="item" href="/">
                                <span style="font-size: 24px;color: #403535;">GINGERBREAD <span style="color: #886D4D;">HOUSE</span></span>
                            </a>
                        </div>
                        <div class="right menu">
                            <?php if(empty($user_type)): ?>
                                <div class="ui simple dropdown item">
                                    <i class="user icon"></i>
                                    <div class="menu">
                                        <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/register" style="color:#000;">
                                            ลงทะเบียน
                                        </a>
                                        <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/login" style="color:#000;">
                                            เข้าสู่ระบบ
                                        </a>

                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="item">
                                    <i class="user circle outline icon" style="margin-left: 1em;cursor: pointer;"></i>
                                    <span class="ui small" style="margin-left: 1em"><?php echo (empty($username))  ? '' : $username; ?></span>
                                    <i class="sign out alternate icon btn-logout" style="margin-left: 1em;cursor: pointer;"></i>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui bottom attached  body-background-image" style="height: auto;border: unset;margin-bottom:unset;">
                <!-- container -->
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="ui container" style="padding-left: 0%;padding-right: 0%;">
                    <div class="ui placeholder segment" style="background: #886d4d8c;">
                        
                        <div class="ui two column very relaxed stackable grid">
                            <div class="ten wide middle aligned content column" style="background-color: #ffffff;">
                                <h2 style="text-align: center;color:#000;">เข้าสู่ระบบ</h2>
                                <br>
                                <form class="ui form" id="sign_in" method="POST" action="<?php echo \URL::route('auth.login.post'); ?>">
                                    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
                                    <!-- <div class="ui form"> -->
                                    <div class="field" style="max-width: 30rem;">
                                        <label style="color:#000;">ชื่อผู้ใช้งานหรืออีเมล์</label>
                                        <div class="ui fluid left icon input">
                                            <input type="text" placeholder="อีเมล์" id="username" name="username" style="border-radius: 20px;">
                                            <i class="user icon"></i>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="field" style="max-width: 30rem;">
                                        <label style="color:#000;">รหัสผ่าน</label>
                                        <div class="ui fluid left icon input">
                                            <input type="password" placeholder="รหัสผ่าน" id="password" name="password" style="border-radius: 20px;">
                                            <i class="lock icon"></i>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="field" style="text-align:center">
                                        <a href="/auth/forgotpassword" style="color: #3c3c3c;">ลืมรหัสผ่าน?</a>
                                    </div>
                                    <br>
                                    <!--<div class="field" style="max-width: unset;">
                                        <div class="ui hidden red message" style="text-align:center" id="errTXT"></div>
                                    </div> -->
                                    <div class="field" style="max-width: unset;text-align: center;">
                                        <?php if (count($errors) > 0): ?>
                                            <div class="alert alert-danger alert-dismissible text-center" style="color: red;font-weight: 700;">
                                                <?php foreach ($errors->all() as $error): ?>
                                                    <?php echo $error ?>
                                                <?php endforeach ?>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                    <br>
                                    <button class="ui teal button" style="border-radius: 30px;background-color: #886D4D;color: #fff;" type="submit" id="btn_login">เข้าสู่ระบบ</button>
                                </form>
                            </div>
                            <div class="six wide middle aligned column">
                                <img class="ui fluid image" src="/themes/image/PETS CHILDREN KIDS PHOTOGRAPHY (1).png" style="margin-bottom: -2.4rem;">
                            </div>
                        </div>

                        <!-- <div class="ui vertical divider"></div> -->

                    </div>
                </div>
                    

                <br>
                <br>

        </div>

    </div>


    <!-- Data -->
    <!-- <input type="hidden" name="_token" id="csrf-token" value="<?php //echo csrf_token() ?>" /> -->
    <!-- <div id="login-url"  data-url="<?php //echo \URL::route('auth.login.post');?>"> -->

<script type="text/javascript">
    $(function(){

        //กดบันทึก เพื่อส่งข้อมูลที่กรอกไป save 
        // $('#btn-login').on('click', function(){
        //     console.log('dsds');
        //     // checkLogin();
        // });

    });

    // function ที่ส่งข้อมูลไป save
    function checkLogin(){
        var username    = $('#login_username').val();
        var password    = $('#login_passsword').val();

        var check_url         = $('#login-url').data('url');
        var data            = new FormData();

        console.log(username);
        console.log(password);

        data.append('username', username);
        data.append('password',password);

        $.ajax({
            headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
            type: 'post',
            url: check_url,
            data: data,
            contentType: false,
            processData:false,
            cache: false,
            success: function(result) {
                // if(result.status == 'success'){
                //     setTimeout(function(){ 
                //         if(result.userType){
                //             if(result.userType == 'ผู้ดูแลระบบ'){
                //                 window.location.href = '/admin/dashboard';
                //             }

                //             if(result.userType == 'ผู้ใช้ทั่วไป'){
                //                 window.location.href = '/';
                //             }
                //         }
                //         // $("body").toast({
                //         //     class: "success",
                //         //     position: 'bottom right',
                //         //     message: `บันทึกเสร็จสิ้น`
                //         // });
                //         // setTimeout(function(){ 
                //         //     window.location.reload()
                //         // }, 1000);
                //     });
                // } 

                // if(result.status == 'error'){
                //     $('#errTXT').removeClass('hidden');
                //     $('#errTXT').text(result.msg);
                //     // $("body").toast({
                //     //     class: "error",
                //     //     position: 'bottom right',
                //     //     message: result.msg
                //     // });
                // }
            },
            error : function(error) {
            }
        });
        
    }


</script>
</body>

</html>