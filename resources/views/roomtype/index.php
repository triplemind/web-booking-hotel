<div class="ui basic segment">
    <br>
    <div class="ui unstackable four column grid segment">
        <div class="ten wide column">
            <h3 class="ui black header" style="margin-top: 0px;font-size: 28px;">
                <i class="concierge bell icon"></i>
                <div class="content">
                    จัดการประเภทห้องพัก
                </div>
            </h3>
        </div>

        <div class="two wide column">
            <div class="field">
                <button class="ui fluid inverted secondary button" onclick="window.location.href = '/admin/room';" type="submit" style="border-radius: 30px;">ย้อนกลับ</button>
            </div>
        </div>

        <div class="four wide right floated column">
            <div class="fields">
                <div class="field">
                    <button class="ui fluid inverted green button btn-addroomtype" type="submit" style="border-radius: 30px;">เพิ่มข้อมูลประเภทห้องพัก</button>
                </div>
            </div>
        </div>

    </div>

    <br>

    <div class="ui form segment">
        <br>
        <br>
        <br>
        <div class="fields">
            <div class="sixteen wide field">
                <table class="ui blue table" id="TBL_RoomType">
                    <thead>
                        <tr>
                            <th>รูปภาพ</th>
                            <th>ชื่อห้องพัก</th>
                            <th>ราคา</th>
                            <th>ขนาด</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($getRoomTypes)): ?>
                            <?php foreach ($getRoomTypes as $key => $getRoomType):?>
                                <tr>
                                    <td class="wide two">
                                        <img src="<?php echo empty($getRoomType->room_img) ? url('').'/themes/image/The HOTEL.png' : url("").str_replace("/public","", $getRoomType->room_img); ?>" height="60px" width="80px">
                                    </td>
                                    <td class="wide two"><?php echo $getRoomType->type_nameroom ?></td>
                                    <td class="wide two"><?php echo $getRoomType->type_rate ?></td>
                                    <td class="wide two"><?php echo $getRoomType->size ?> ตารางเมตร</td>
                                    <td class="wide two">
                                        <i class="large edit outline icon editroomtype" data-id="<?php echo $getRoomType->typeroom_id; ?>"  style="cursor: pointer;"></i>
                                        <i class="large trash alternate outline icon deleteroomtype" data-id="<?php echo $getRoomType->typeroom_id; ?>" style="cursor: pointer;"></i>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        <?php endif ?>
                    </tbody>
                </table>
                <!-- แสดงตัวเลข page -->
            </div>
        </div>
    </div>
</div>

<!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id='ajax-center-url' data-url="<?php echo \URL::route('roomtype.ajax_center.post');?>"></div>
    <div id='add-url' data-url="<?php echo \URL::route('roomtype.add.post');?>"></div>
    <div id='edit-url' data-url="<?php echo \URL::route('roomtype.edit.post');?>"></div>
    <div id='delete-url' data-url="<?php echo \URL::route('roomtype.delete.post');?>"></div>


<div class="ui medium modal" id="addRoomTypeModal">
    <div class="header">เพิ่มข้อมูลประเภทห้องพัก</div>
    <div class="scrolling content">
        <div class="ui medium form">
            <div class="field">
                <label>ชื่อห้องพัก</label>
                <input type="text" placeholder="" name="type_nameroom" id="type_nameroom">
            </div>
            <div class="two fields">
                <div class="field">
                    <label>ราคา</label>
                    <input type="number" placeholder="" name="type_rate" id="type_rate">
                </div>
                <div class="field">
                    <label>ขนาด</label>
                    <input type="number" placeholder="" name="size" id="size">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>จำนวนผู้เข้าพัก</label>
                    <input type="number" placeholder="" name="people" id="people">
                </div>
                <div class="field">
                    <label>จำนวนเตียงเสริม</label>
                    <input type="number" placeholder="" name="extra" id="extra">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>จำนวนสัตว์เลี้ยงขนาดใหญ่</label>
                    <input type="number" placeholder="" name="bigpet" id="bigpet">
                </div>
                <div class="field">
                   <label>จำนวนสัตว์เลี้ยงขนาดเล็ก</label>
                    <input type="number" placeholder="" name="smallpet" id="smallpet">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>เวลา CheckIn</label>
                    <div class="ui calendar" id="checkindate">
                        <div class="ui input left icon">
                            <i class="time icon"></i>
                            <input type="text" placeholder="Time" readonly>
                        </div>
                    </div>
                </div>
                <div class="field">
                   <label>เวลา CheckOut</label>
                    <div class="ui calendar" id="checkoutdate">
                        <div class="ui input left icon">
                            <i class="time icon"></i>
                            <input type="text" placeholder="Time" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>รูปภาพ</label>
                <input type="file" placeholder="" name="room_img" id="room_img">
            </div>
            <div class="field">
                <label>รายละเอียดเพิ่มติม</label>
                <textarea name="info" id="info" rows="4"></textarea>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>


<div class="ui medium modal" id="EditRoomTypeModal">
    <div class="header">แก้ไขข้อมูลประเภทห้องพัก</div>
    <div class="scrolling content">
        <div class="ui medium form">
            <input type="hidden" placeholder="" name="typeroom_id" id="typeroom_id">
            <div class="field">
                <label>ชื่อห้องพัก</label>
                <input type="text" placeholder="" name="ed_type_nameroom" id="ed_type_nameroom">
            </div>
            <div class="two fields">
                <div class="field">
                    <label>ราคา</label>
                    <input type="number" placeholder="" name="ed_type_rate" id="ed_type_rate">
                </div>
                <div class="field">
                    <label>ขนาด</label>
                    <input type="number" placeholder="" name="ed_size" id="ed_size">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>จำนวนผู้เข้าพัก</label>
                    <input type="number" placeholder="" name="ed_people" id="ed_people">
                </div>
                <div class="field">
                    <label>จำนวนเตียงเสริม</label>
                    <input type="number" placeholder="" name="ed_extra" id="ed_extra">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>จำนวนสัตว์เลี้ยงขนาดใหญ่</label>
                    <input type="number" placeholder="" name="ed_bigpet" id="ed_bigpet">
                </div>
                <div class="field">
                   <label>จำนวนสัตว์เลี้ยงขนาดเล็ก</label>
                    <input type="number" placeholder="" name="ed_smallpet" id="ed_smallpet">
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>เวลา CheckIn</label>
                    <div class="ui calendar" id="ed_checkindate">
                        <div class="ui input left icon">
                            <i class="time icon"></i>
                            <input type="text" placeholder="Time" readonly>
                        </div>
                    </div>
                </div>
                <div class="field">
                   <label>เวลา CheckOut</label>
                    <div class="ui calendar" id="ed_checkoutdate">
                        <div class="ui input left icon">
                            <i class="time icon"></i>
                            <input type="text" placeholder="Time" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <label>รูปภาพ</label>
                <input type="file" placeholder="" name="ed_room_img" id="ed_room_img">
            </div>
            <div class="field">
                <label>รายละเอียดเพิ่มติม</label>
                <textarea name="ed_info" id="ed_info" rows="4"></textarea>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="ui approve green button">บันทึก</div>
        <div class="ui red cancel button">ยกเลิก</div>
    </div>
</div>