<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>GINGERBREAD HOUSE</title>

    <!-- Favicon-->
    <link rel="apple-touch-icon" sizes="57x57" href="/themes/image/Favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/themes/image/Favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/themes/image/Favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/themes/image/Favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/themes/image/Favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/themes/image/Favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/themes/image/Favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/themes/image/Favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/themes/image/Favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/themes/image/Favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/themes/image/Favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/themes/image/Favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/themes/image/Favicon/favicon-16x16.png">
    <link rel="manifest" href="/themes/image/Favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/themes/image/Favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">



    <link rel="stylesheet" href="/themes/semantic/semantic.css">
    <link rel="stylesheet" href="/themes/css/style.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/animate.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/sweetalert2.css?ccc=2">
    <link rel="stylesheet" href="/themes/css/dataTables.semanticui.min.css">
    <link href='/themes/fullcalendar/core/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/timeline/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/list/main.css' rel='stylesheet' />
    <link href='/themes/fullcalendar/resource-timeline/main.css' rel='stylesheet' />
    <link rel="stylesheet" type="text/css" href="/themes/slick-1.8.1/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="/themes/slick-1.8.1/slick/slick-theme.css"/>
    
    <!-- เรียกใช้งาน css ของแต่ละหน้า -->
    <?php if (file_exists(base_path().'/public/css/'.helperGetModule().'/'.helperGetAction().'.css')): ?>
        <link rel="stylesheet" href="<?php echo '/css/'.helperGetModule().'/'.helperGetAction().'.css?t='.time() ?>">
    <?php endif ?>
    <!-- เรียกใช้งาน css ของแต่ละหน้า -->

    <style type="text/css">
        .ui.menu .active.item:hover, .ui.vertical.menu .active.item:hover{
            background-color: rgb(171 229 236 / 0.58)
        }
        .ui.vertical.menu .active.item{
            background: rgb(171 229 236);
            color:#000;
        }
        .body-background-image{
            position: absolute;
            z-index: -1;
            width: 100%;
            height: 100%;
            background-image: url('../themes/image/bg.png');
            background-size: cover;
            /*opacity: 0.7;*/
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }
    </style>
</head>


<body style="background-color: #fff;">

    <script src="/themes/js/jquery.min.js"></script>
    <script src="/themes/js/current-device.min.js"></script>
    <script src="/themes/semantic/semantic.min.js"></script>
    <script src="/themes/js/calendarTH.js"></script>
    <script src="/themes/js/moment.js"></script>
    <script src="/themes/js/moment-with-locales.js"></script>
    <script src='/themes/fullcalendar/core/main.js'></script>
    <script src='/themes/fullcalendar/core/locales-all.min.js'></script>
    <script src='/themes/fullcalendar/timeline/main.js'></script>
    <script src='/themes/fullcalendar/list/main.js'></script>
    <script src='/themes/fullcalendar/interaction/main.js'></script>
    <script src='/themes/fullcalendar/resource-common/main.js'></script>
    <script src='/themes/fullcalendar/resource-timeline/main.js'></script>
    <script src='/themes/js/sweetalert2.js'></script>
    <script src='/themes/js/lodash.js'></script>
    <script src='/themes/js/jquery.dataTables.min.js'></script>
    <script src='/themes/js/dataTables.semanticui.min.js'></script>
    <script src="/themes/js/angular.min.js"></script>
    <script src="/themes/js/angular-route.js"></script>
    <script src="/themes/js/angular-animate.js"></script>
    <script src="/themes/js/cleave/cleave.min.js"></script>
    <script src="/themes/js/cleave/addons/cleave-phone.th.js"></script>
    <script src="/themes/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/themes/slick-1.8.1/slick/slick.min.js"></script>

    <!-- เรียกใช้งาน javascript ของแต่ละหน้า -->
    <?php if (file_exists(base_path().'/public/js/'.helperGetModule().'/'.helperGetAction().'.js')): ?>
        <script src="<?php echo '/js/'.helperGetModule().'/'.helperGetAction().'.js?t='.time() ?>"></script>
    <?php endif ?>
    <!-- เรียกใช้งาน javascript ของแต่ละหน้า -->

    <div class="ui" id="example1">
        <!-- เมนูบาร์ computer view-->
        <div class="ui grid" style="">
            <div class="computer only row">
                <div class="column">
                    <div class="ui sticky">
                        <div class="ui large menu" style="background-color: #fff;">
                            <a class="<?php echo (helperGetAction() == 'index') ? 'active' : '' ?> item" href="/">
                                <span style="font-size: 24px;color: #403535;">GINGERBREAD <span style="color: #886D4D;">HOUSE</span></span>
                            </a>
                            <a class="<?php echo (helperGetAction() == 'room') ? 'active' : '' ?> item" href="/" style="color:#000;">
                                หน้าหลัก
                            </a>
                            <a class="<?php echo (helperGetAction() == 'roomtype') ? 'active' : '' ?> item" href="/roomtype" style="color:#000;">
                                ประเภทห้องพัก
                            </a>
                            <a class="<?php echo (helperGetAction() == 'gallery') ? 'active' : '' ?> item" href="/gallery" style="color:#000;">
                                เกี่ยวกับเรา
                            </a>
                            <a class="<?php echo (helperGetAction() == 'activity') ? 'active' : '' ?> item" href="/activity" style="color:#000;">
                                ติดต่อเรา
                            </a>
                            <div class="right menu">
                                <?php if(empty($user_type)): ?>
                                    <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/register" style="color:#000;">
                                        ลงทะเบียน
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/login" style="color:#000;">
                                        เข้าสู่ระบบ
                                    </a>
                                <?php else: ?>
                                    <a class="<?php echo (helperGetAction() == 'bookinghistory') ? 'active' : '' ?> item" href="/bookinghistory" style="color:#000;">
                                        รายการจอง
                                    </a>
                                    <div class="item">
                                        <i class="user circle outline icon" style="margin-left: 1em;cursor: pointer;"></i>
                                        <span class="ui small" style="margin-left: 1em"><?php echo (empty($username))  ? '' : $username; ?></span>
                                        <i class="sign out alternate icon btn-logout" style="margin-left: 1em;cursor: pointer;"></i>
                                    </div>
                                <?php endif ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- เมนูบาร์ tablet mobile view-->
            <div class="tablet mobile only row">
                <div class="column">
                    <div class="ui top fixed large menu" style="<?php echo (helperGetAction() == 'index') ? 'background-color: #fff;' : 'background-color: #FEE265;' ?>">
                        <div class="menu">
                            <div class="ui simple dropdown item">
                                <i class="sidebar icon"></i>
                                <div class="menu">
                                    <a class="<?php echo (helperGetAction() == 'room') ? 'active' : '' ?> item" href="/" style="color:#000;">
                                        หน้าหลัก
                                    </a>
                                    <!-- <a class="<?php echo (helperGetAction() == 'activity') ? 'active' : '' ?> item" href="/activity" style="color:#000;">
                                        ACTIVITIES
                                    </a> -->
                                    <a class="<?php echo (helperGetAction() == 'roomtype') ? 'active' : '' ?> item" href="/roomtype" style="color:#000;">
                                        ประเภทห้องพัก
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'gallery') ? 'active' : '' ?> item" href="/gallery" style="color:#000;">
                                        เกี่ยวกับเรา
                                    </a>
                                    <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/activity" style="color:#000;">
                                        ติดต่อเรา
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="center menu">
                            <a class="item" href="/">
                                <span style="font-size: 24px;color: #403535;">GINGERBREAD <span style="color: #886D4D;">HOUSE</span></span>
                            </a>
                        </div>
                        <div class="right menu">
                            <?php if(empty($user_type)): ?>
                                <div class="ui simple dropdown item">
                                    <i class="user icon"></i>
                                    <div class="menu">
                                        <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/register" style="color:#000;">
                                            ลงทะเบียน
                                        </a>
                                        <a class="<?php echo (helperGetAction() == 'contact') ? 'active' : '' ?> item" href="/auth/login" style="color:#000;">
                                            เข้าสู่ระบบ
                                        </a>

                                    </div>
                                </div>
                            <?php else: ?>
                                <a class="<?php echo (helperGetAction() == 'bookinghistory') ? 'active' : '' ?> item" href="/bookinghistory" style="color:#000;">
                                    รายการจอง
                                </a>
                                <div class="item">
                                    <i class="user circle outline icon" style="margin-left: 1em;cursor: pointer;"></i>
                                    <span class="ui small" style="margin-left: 1em"><?php echo (empty($username))  ? '' : $username; ?></span>
                                    <i class="sign out alternate icon btn-logout" style="margin-left: 1em;cursor: pointer;"></i>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <!-- ส่วนของสไลด์ และ ส่วนของการ search แสดงเฉพาะหน้าหลัก-->
            <?php if(helperGetAction() == 'index'): ?>
                <!-- ส่วนของสไลด์ -->
                <div class="slider-nav" style="position: relative">
                    <img class="ui centered massive image" src="<?php echo url('').'/themes/image/bg11.jpg'; ?>">
                    <img class="ui centered massive image" src="<?php echo url('').'/themes/image/bg22.jpg'; ?>" onclick="window.location.href = '/roomtype';">
                    <img class="ui centered massive image" src="<?php echo url('').'/themes/image/bg33.jpg'; ?>">
                    <!-- <img class="ui centered massive image" src="<?php echo url('').'/themes/image/img1.jpg'; ?>"> -->
                </div>
                <!-- ส่วนของสไลด์ -->

                <!-- ส่วนของการ search -->
                <div class="ui stackable" style="margin-top: -5rem;padding-left: 2rem;padding-right: 2rem;">
                    <div class="ui very padded raised segment" style="background-color: #8b8986bf;border-radius: 10px;">
                        <div class="ui form " style="margin-bottom: 1rem;">
                            <div class="five fields">
                                <div class="four wide field">
                                    <label style="color: #fff;">เลือกวันเข้าพัก</label>
                                    <div class="ui calendar" id="checkin">
                                        <div class="ui input left icon">
                                            <i class="calendar icon"></i>
                                            <input type="text" placeholder="กรุณาเลือกวันเข้าพัก" style="border-radius: 20px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="four wide field">
                                    <label style="color: #fff;">เลือกวันสิ้นสุด</label>
                                    <div class="ui calendar" id="checkout">
                                        <div class="ui input left icon">
                                            <i class="calendar icon"></i>
                                            <input type="text" placeholder="กรุณาเลือกวันสิ้นสุด" style="border-radius: 20px;">
                                        </div>
                                    </div>
                                </div>
                                <div class="two wide field">
                                    <label style="color: #fff;">จำนวนผู้เข้าพัก</label>
                                    <div class="ui fluid left icon input">
                                        <input type="number" placeholder="1" id="persons" name="persons" min="1" style="border-radius: 20px;">
                                        <i class="user icon"></i>
                                    </div>
                                </div>
                                <div class="two wide field">
                                    <label style="color: #fff;">สัตว์เลี้ยงพันธุ์เล็ก</label>
                                    <div class="ui fluid left icon input">
                                        <input type="number" placeholder="1" id="pet" name="pet" min="1" style="border-radius: 20px;">
                                        <i class="paw icon"></i>
                                    </div>
                                </div>
                                <div class="two wide field">
                                    <label style="color: #fff;">สัตว์เลี้ยงพันธุ์ใหญ่</label>
                                    <div class="ui fluid left icon input">
                                        <input type="number" placeholder="1" id="pet_big" name="pet_big" min="1" style="border-radius: 20px;">
                                        <i class="paw icon"></i>
                                    </div>
                                </div>
                                <div class="two wide field" style="margin-top: 1.6rem;">
                                    <button class="ui fluid teal button" style="border-radius: 30px;background-color: #886D4D;color: #fff;" type="submit" id="btn_search">ค้นหา</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ส่วนของการ search -->
            <?php endif ?>
            <!-- ส่วนของสไลด์ และ ส่วนของการ search แสดงเฉพาะหน้าหลัก-->
        </div>



        <div class="ui bottom attached" style="height: auto;border: unset;margin-bottom:unset;">
                <!-- container -->
                
                    <!-- ส่วนที่เรียนหน้า view อื่นๆมาแสดง -->
                    <?php echo view($page,$data);?> 
                    <!-- ส่วนที่เรียนหน้า view อื่นๆมาแสดง -->

                <br>
                <br>

        </div>

    </div>


    <!-- Data -->
    <input type="hidden" name="_token" id="csrf-token" value="<?php echo csrf_token() ?>" />
    <div id="logout-url"  data-url="<?php echo \URL::route('auth.logout.get');?>">

<script type="text/javascript">
    $(function(){
        // คำสั่งให้สไลด์ทำงาน ใช้ slick ในการสร้าง สไลด์
        $('.slider-nav').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
            // centerMode: true,
            mobileFirst: true,
        });
        // คำสั่งให้สไลด์ทำงาน ใช้ slick ในการสร้าง สไลด์

        // คำสั่งสำหรับการ logout
        $('.btn-logout').on('click', function(){
            Swal.fire({
                title: "คุณต้องการออกจากระบบหรือไม่ ?",
                showCancelButton: true,
                confirmButtonColor: "#59B855",
                cancelButtonText: "ยกเลิก",
                cancelButtonColor: "",
                confirmButtonText: "ตกลง"
            }).then(result => {
                if (result.value) {
                    logout_url = $('#logout-url').data('url');
                    console.log(logout_url);
                    window.location = logout_url;
                }
            });
        });
        // คำสั่งสำหรับการ logout

    });
</script>
</body>

<footer style="background-color: #886D4D;">
    <div class="ui basic segment" style="text-align: center;color: #000;padding: 3rem;font-size: 17px;">
        <div class="ui grid">
            <div class="eight wide column" style="padding-top: 2em;text-align: left;">
                <div class="ui grid">
                    <div class="sixteen wide column">
                        <p style="color: #fff;">76/16 M.12 Nong Saentor Phornhamani</p>
                        <p style="color: #fff;">Muang Nakhonnayok 26000</p>
                        <p style="color: #fff;">gingerbreadhouse.nakonnayok@gmail.com</p>
                    </div>
                    <div class="sixteen wide column" style="margin-top: 3rem;">
                        <p style="color: #fff;">© 2020 by Gingerbreadhouse</p>
                        <p style="color: #fff;">© Copyrights</p>
                    </div>
                </div>
            </div>
            <div class="eight wide column" style="padding-top: 2em;text-align: left;">
                <div class="ui grid">
                    <div class="sixteen wide column">
                        <p style="color: #fff;">Tel : 092 268 8272</p>
                        <p style="color: #fff;">Line@ : gingerbread1</p>
                    </div>
                    <div class="sixteen wide column">
                        <i class="large facebook icon" style="color: #fff;"></i>
                        <i class="large instagram icon" style="color: #fff;"></i>
                        <i class="large linechat icon" style="color: #fff;"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

</html>