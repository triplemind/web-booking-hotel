$(function(){

	// $('#reserve_data_chkin').calendar({
	// 	type: 'date',
	// 	endCalendar: $('#reserve_data_chkout')
	// });
	// $('#reserve_data_chkout').calendar({
	// 	type: 'date',
	// 	startCalendar: $('#reserve_data_chkin')
	// });

	// $('#btn_booking').on('click', function(){
		// postAddBooking();
	// });


});

function postAddBooking(){

	var detail_val 			= [];
	var firstname 			= $('#firstname').val();
	var lastname 			= $('#lastname').val();
	var tel 				= $('#tel').val();
	var email 				= $('#email').val();
	var line_id 			= $('#line_id').val();
	var reserve_nmpeople 	= $('#reserve_nmpeople').val();
	var reserve_nmpet 		= $('#reserve_nmpet').val();
	var reserve_nmroom 		= $('#reserve_nmroom').val();
	// var info 				= $('#info').val();

	var reserve_data_chkin		= $('#reserve_data_chkin').calendar('get date');
	var reserve_data_chkout		= $('#reserve_data_chkout').calendar('get date');
	var typeroom_id				= $('#typeroom_id').val();
	var reserve_price			= $('#reserve_price').val();

	// var req_swim_type			= $('#req_swim_type').is(":checked");
	// var req_bed_type			= $('#req_bed_type').is(":checked");
	// var req_breakfast_type		= $('#req_breakfast_type').is(":checked");

	

	// console.log(req_swim_type, req_bed_type, req_breakfast_type);
	// console.log($("#req_swim_type").val());

	var add_url		= $('#add-url').data('url');
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: add_url,
		data: {
		  	'detail_val' : detail_val,
		  	'firstname' : firstname,
		  	'lastname' : lastname,
		  	'tel' : tel,
		  	'email' : email,
		  	'line_id' : line_id,
		  	'reserve_nmpeople' : reserve_nmpeople,
		  	'reserve_nmpet' : reserve_nmpet,
		  	'reserve_nmroom' : reserve_nmroom,
		  	'reserve_data_chkin' : moment(reserve_data_chkin).format('YYYY-MM-DD'),
		  	'reserve_data_chkout' : moment(reserve_data_chkout).format('YYYY-MM-DD'),
		  	'typeroom_id' : typeroom_id,
		  	'reserve_price' : reserve_price,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		
		}
	});

}