$(function(){

	$('#pay_bank').dropdown();
    $('#ed_pay_bank').dropdown();


    $('#pay_date_time').calendar({
        // type: 'date',
        popupOptions: {
            observeChanges: false
        },
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        },
    });

    $('#ed_pay_date_time').calendar({
        // type: 'date',
        popupOptions: {
            observeChanges: false
        },
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                    var day = date.getDate();
                    var month = date.getMonth() + 1;
                    var year = date.getFullYear();
                return day + '/' + month + '/' + year;
            }
        },
    });


    //ยังไม่เสร็จ
    $('.btn-save-payment').on('click', function(){
        var addslip_url     = $('#ajax-center-url').data('url');
        var id              = $('#id').val();
        var pay_money       = $('#pay_money').val();
        var pay_date_time   = $('#pay_date_time').calendar('get date');
        var pay_bank    	= $('#pay_bank').dropdown('get value');

        var data        = new FormData();
        if (typeof($('#pay_img')[0]) !== 'undefined') {

            jQuery.each(jQuery('#pay_img')[0].files, function(i, file) {
                data.append('pay_img', file);
                // console.log(file);
            });
        }
        data.append('method','addPayment');
        data.append('id',id);
        data.append('pay_money',pay_money);
        data.append('pay_date_time',moment(pay_date_time).locale('th').format('YYYY-MM-DD h:mm:ss'));
        data.append('pay_bank',pay_bank);


		console.log(moment(pay_date_time).locale('th').format('YYYY-MM-DD h:mm:ss'));        

        if(pay_date_time != null || pay_bank != '' || pay_money != ''){
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: addslip_url,
                data: data,
                contentType: false,
                processData:false,
                cache: false,
                success: function(result) {
                    if(result.status == 'success'){
                       setTimeout(function(){ 
                            $("body").toast({
                                class: "success",
                                position: 'bottom right',
                                message: `บันทึกเสร็จสิ้น`
                            });
                            setTimeout(function(){ 
                                window.location.reload();
                            }, 1000);
                        });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }else{
            $("body").toast({
                class: "error",
                position: 'bottom right',
                message: 'กรุณากรอกข้อมูลให้ครบ'
            });
        }

    });



	$('.btn-edit-payment').on('click', function(){
        var addslip_url     = $('#ajax-center-url').data('url');
        var id              = $('#id').val();
        var payid           = $('#payid').val();
        var pay_money       = $('#ed_pay_money').val();
        var pay_date_time   = $('#ed_pay_date_time').calendar('get date');
        var pay_bank    	= $('#ed_pay_bank').dropdown('get value');

        var data        = new FormData();
        if (typeof($('#ed_pay_img')[0]) !== 'undefined') {

            jQuery.each(jQuery('#ed_pay_img')[0].files, function(i, file) {
                data.append('pay_img', file);
                // console.log(file);
            });
        }
        data.append('method','editPayment');
        data.append('payid',payid);
        data.append('id',id);
        data.append('pay_money',pay_money);
        data.append('pay_date_time',moment(pay_date_time).locale('th').format('YYYY-MM-DD h:mm:ss'));
        data.append('pay_bank',pay_bank);

        if(pay_date_time != null || pay_bank != '' || pay_money != ''){
            $.ajax({
                headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                type: 'post',
                url: addslip_url,
               	data: data,
    	        contentType: false,
    	        processData:false,
    	        cache: false,
                success: function(result) {
                    if(result.status == 'success'){
                       setTimeout(function(){ 
    	                    $("body").toast({
    	                        class: "success",
    	                        position: 'bottom right',
    	                        message: `บันทึกเสร็จสิ้น`
    	                    });
    	                    setTimeout(function(){ 
    	                        window.location.reload();
    	                    }, 1000);
    	                });
                    } // End if check s tatus success.

                    if(result.status == 'error'){
                        $("body").toast({
                            class: "error",
                            position: 'bottom right',
                            message: result.data
                        });
                    }
                }
            }); 
        }else{
            $("body").toast({
                class: "error",
                position: 'bottom right',
                message: 'กรุณากรอกข้อมูลให้ครบ'
            });
        }

    });



    $('.btn-cancel').on('click', function(){
        console.log("fdf");
        Swal.fire({
            title: "คุณต้องการยกเลิกรายการจองนี้ใช่หรือไม่ ?",
            showCancelButton: true,
            confirmButtonColor: "#18db32",
            cancelButtonText: "ไม่ใช่",
            cancelButtonColor: "",
            confirmButtonText: "ใช่"
        }).then(result => {
            if (result.value) {
                var addslip_url     = $('#ajax-center-url').data('url');
                var id              = $('#id').val();
                var status          = 'ยกเลิก';

                var data        = new FormData();

                data.append('method','CancelBooking');
                data.append('id',id);
                data.append('status',status);

                console.log(id, status);

                $.ajax({
                    headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
                    type: 'post',
                    url: addslip_url,
                    data: data,
                    contentType: false,
                    processData:false,
                    cache: false,
                    success: function(result) {
                        if(result.status == 'success'){
                           setTimeout(function(){ 
                                $("body").toast({
                                    class: "success",
                                    position: 'bottom right',
                                    message: `บันทึกเสร็จสิ้น`
                                });
                                setTimeout(function(){ 
                                    window.location.reload();
                                }, 1000);
                            });
                        } // End if check s tatus success.

                        if(result.status == 'error'){
                            $("body").toast({
                                class: "error",
                                position: 'bottom right',
                                message: result.data
                            });
                        }
                    }
                }); 
            }
        });

    });




});
