let _multimenu;

$(function(){

	DTRender('#TBL_user', true);

	// console.log('ssss');
	$('.btn-adduser').on('click', function(){
		$('#user_type').dropdown();
		$("#addUserModal").modal({
			closable: false,
			onDeny: function() {
			},
			onApprove: function() {
				addNewUser();
			}
		}).modal("show");
	});


	// ทำ edit กับ delete ต่อ
	$('.edituser').on('click', function(){
		var user_id = $(this).data('id');

		var method 		= 'getUserData';
		var ajax_url	= $('#ajax-center-url').data('url');
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: ajax_url,
			data: {
			  	'method' : method,
			  	'user_id' : user_id,
			},
			success: function(result) {
				if(result.status == 'success'){

					$('#user_id').val(result.data.user_id);
					$('#ed_username').val(result.data.username);
					$('#ed_password').val(result.data.password);
					$('#ed_firstname').val(result.data.firstname);
					$('#ed_lastname').val(result.data.lastname);
					$('#ed_email').val(result.data.email);
					$('#ed_line_id').val(result.data.line_id);
					$('#ed_user_type').dropdown('set selected',result.data.user_type);
					$('#ed_tel').val(result.data.tel);

					console.log(result.data.district);

					setTimeout(function(){
						$("#EditUserModal").modal({
							closable: false,
							onDeny: function() {
							},
							onApprove: function() {
								editUser(user_id);
							}
						}).modal("show");
			        });
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.data
					});
				}
			}
		});

	});

	$('.deleteuser').on('click', function(){
		console.log($(this).data('id'));
		var user_id = $(this).data('id');
		Swal.fire({
	        title: "คุณต้องการลบผู้ใช้งานนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            deleteUser(user_id);
	        }
	    });
	});

});


function addNewUser() {
	var username 		= $('#username').val();
	var password 		= $('#password').val();
	var firstname 		= $('#firstname').val();
	var lastname 		= $('#lastname').val();
	var email 			= $('#email').val();
	var line_id 		= $('#line_id').val();
	var tel 			= $('#tel').val();
	var user_type		= $('#user_type').dropdown('get value');

	var add_url			= $('#add-url').data('url');
	var data  			= new FormData();

	data.append('username', username);
	data.append('password',password);
	data.append('firstname',firstname);
	data.append('lastname',lastname);
	data.append('email',email);
	data.append('user_type',user_type);
	data.append('line_id',line_id);
	data.append('tel',tel);

	// check format email
	var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

	if (testEmail.test(email)){
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: add_url,
			data: data,
			contentType: false,
	        processData:false,
	        cache: false,
			success: function(result) {
				if(result.status == 'success'){
					setTimeout(function(){ 
						$("body").toast({
							class: "success",
							position: 'bottom right',
							message: `บันทึกเสร็จสิ้น`
						});
						setTimeout(function(){ 
			                window.location.href = '/admin/user';
			            }, 1000);
					});
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.msg
					});
				}
			},
			error : function(error) {
				// showForm(form, action, error, data);
			}
		});
	}else{
		$("body").toast({
			class: "error",
			displayTime: 10000,
			position: 'bottom right',
			message: `รูปแบบอีเมลไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง`
		});
	}

}




function editUser(user_id) {
	var user_id 		= $('#user_id').val();
	var username 		= $('#ed_username').val();
	var password 		= $('#ed_password').val();
	var firstname 		= $('#ed_firstname').val();
	var lastname 		= $('#ed_lastname').val();
	var email 			= $('#ed_email').val();
	var line_id 		= $('#ed_line_id').val();
	var tel 			= $('#ed_tel').val();
	var user_type		= $('#ed_user_type').dropdown('get value');

	var edit_url		= $('#edit-url').data('url');
	var data  			= new FormData();


	data.append('user_id', user_id);
	data.append('username', username);
	data.append('password',password);
	data.append('firstname',firstname);
	data.append('lastname',lastname);
	data.append('email',email);
	data.append('user_type',user_type);
	data.append('line_id',line_id);
	data.append('tel',tel);
	
	// check format email
	var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;

	if (testEmail.test(email)){
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: edit_url,
			data: data,
			contentType: false,
	        processData:false,
	        cache: false,
			success: function(result) {
				if(result.status == 'success'){
					setTimeout(function(){ 
						$("body").toast({
							class: "success",
							position: 'bottom right',
							message: `บันทึกเสร็จสิ้น`
						});
						setTimeout(function(){ 
			                window.location.href = '/admin/user';
			            }, 1000);
					});
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.msg
					});
				}
			},
			error : function(error) {
				// showForm(form, action, error, data);
			}
		});
	}else{
		$("body").toast({
			class: "error",
			displayTime: 10000,
			position: 'bottom right',
			message: `รูปแบบอีเมลไม่ถูกต้อง กรุณาตรวจสอบอีกครั้ง`
		});
	}

}



function deleteUser(user_id){
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'user_id' : user_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.href = '/admin/user';
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}

const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}