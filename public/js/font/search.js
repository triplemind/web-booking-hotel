$(function(){

	$('#checkin').calendar({
		type: 'date',
		minDate: new Date(),
		monthFirst: false,
        formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		endCalendar: $('#checkout')
	});
	$('#checkout').calendar({
		type: 'date',
		minDate: new Date(),
		monthFirst: false,
        formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		startCalendar: $('#checkin')
	});

	$('#btn_search').on('click', function(){
        msg_waiting();
        addFilterToSession();
    });


    $('#btn_clear').on('click', function(){
        msg_waiting();
    	var ajax_url 		= $('#ajax-center-url').data('url');
    	var method 			= 'clearFilterToSession'
    	$.ajax({
	        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
	        type: 'post',
	        url: ajax_url,
	        data: {
	            'method' : method,
	        },
	        success: function(result) {
	            if (result.status === 'success')       window.location.reload();
	        },
	    });
    });


	$('.special.cards .image').dimmer({
	  on: 'hover'
	});
	
});


function msg_waiting() {
	let timerInterval;
	Swal.fire({
		title: '',
		html: 'LOADING',
		timer: 2000,
		timerProgressBar: true,
		onBeforeOpen: () => {
			Swal.showLoading()
			timerInterval = setInterval(() => {
				const content = Swal.getContent()
				if (content) {
					const b = content.querySelector('b')
					if (b) {
						b.textContent = Swal.getTimerLeft()
					}
				}
			}, 100)
		},
		onClose: () => {
			clearInterval(timerInterval)
		}
	}).then((result) => {
		/* Read more about handling dismissals below */
		if (result.dismiss === Swal.DismissReason.timer) {
			console.log('I was closed by the timer')
		}
	});
}


function addFilterToSession() {
	var checkin   	= $('#checkin').calendar('get date');
    var checkout	= $('#checkout').calendar('get date');
    var persons 	= $('#persons').val();
    var pet 		= $('#pet').val();
    var pet_big 	= $('#pet_big').val();

    console.log(moment(checkin).format('YYYY-MM-DD'), moment(checkout).format('YYYY-MM-DD'));

    var ajax_url        = $('#ajax-center-url').data('url');
    var method          = 'addFilterToSession'

    $.ajax({
        headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
        type: 'post',
        url: ajax_url,
        data: {
            'method' : method,
            'checkin' : checkin ? moment(checkin).format('YYYY-MM-DD') : '',
            'checkout' : checkout ? moment(checkout).format('YYYY-MM-DD') : '',
            'persons' : (persons != 'all') ? persons : '',
            'pet' : (pet != 'all') ? pet : '',
            'pet_big' : (pet_big != 'all') ? pet_big : '',
        },
        success: function(result) {
            if (result.status === 'success')       window.location.reload();
        },
    });
}