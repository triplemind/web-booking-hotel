let _totalprice = 0,
	_bathprice = 0,
	_swimprice = 0,
	_foodprice = 0,
	_petfoodprice = 0,
	_bedprice = 0,
	_numofnights = 0,
	_discount = 0,
	_finaltotal = 0;

let _typeroom_id;

let _totalpet = 0;

$(function(){


	$('.btn-apply-code').on('click', function(){
		var promo_code 		= $('#promo_code').val();

		checkPormoCode(promo_code);

	});


	$('#reserve_data_chkin').calendar({
		type: 'date',
		minDate: new Date(),
		monthFirst: false,
        formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		endCalendar: $('#reserve_data_chkout')
	});
	$('#reserve_data_chkout').calendar({
		type: 'date',
		minDate: new Date(),
		monthFirst: false,
        formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
		startCalendar: $('#reserve_data_chkin')
	});


	$('.special.cards .image').dimmer({
	  on: 'hover'
	});

	$('.btn-next').on('click', function(){
		$('.btn-back').css('display','unset');
		$('.btn-approvenow').css('display','unset');
		$('.btn-next').css('display','none');

		$('#personalDiv').css('display','unset');
		$('#additionalDiv').css('display','none');
	});


	$('.btn-back').on('click', function(){
		$('.btn-back').css('display','none');
		$('.btn-approvenow').css('display','none');
		$('.btn-next').css('display','unset');

		$('#personalDiv').css('display','none');
		$('#additionalDiv').css('display','unset');
	});
	
	

});

function gotoBookingSummary(typeroom_id,_price) {

	msg_waiting();

	setTimeout(function(){ 
		if($('#reserve_data_chkin').calendar('get date') && $('#reserve_data_chkout').calendar('get date')){

			var ajax_url 	= $('#ajax-center-url').data('url');
			var method 		= 'CheckBooking';
			$.ajax({
				headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
				type: 'post',
				url: ajax_url,
				data: {
				  	'method' : method,
				  	'reserve_nmpeople' : $('#reserve_nmpeople').val(),
				  	'reserve_nmpet' : $('#reserve_nmpet').val(),
				  	'reserve_nmroom' : $('#reserve_nmpetbig').val(),
				  	'reserve_nmpetbig' : $('#reserve_nmpetbig').val(),
				  	'reserve_data_chkin' : moment($('#reserve_data_chkin').calendar('get date')).locale('th').format('YYYY-MM-DD'),
				  	'reserve_data_chkout' : moment($('#reserve_data_chkout').calendar('get date')).locale('th').format('YYYY-MM-DD'),
				  	'typeroom_id' : typeroom_id,
				},
				success: function(result) {
					msg_stop();
					if(result.status == 'success'){
						
						_totalpet = parseInt(parseInt($('#reserve_nmpet').val())+parseInt($('#reserve_nmpetbig').val()));

						console.log(_totalpet);

						for (var i = 0; i < _totalpet; i++) {
							var j = i+1;
							$('#type_id_'+j).dropdown();
							$('#species_id_'+j).dropdown();
							$('#pet_gender_'+j).dropdown();

							$('#pet_birthday_'+j).calendar({
								type: 'date',
								monthFirst: false,
								popup:'show',
						        formatter: {
									date: function (date, settings) {
										if (!date) return '';
										var day = date.getDate();
										var month = date.getMonth() + 1;
										var year = date.getFullYear();
										return day + '/' + month + '/' + year;
									}
								},
							});

							console.log(j);
						}

						_totalprice = _price;
						_typeroom_id = typeroom_id;

						$('.checkintxt').text('');
						$('.checkouttxt').text('');
						$('.persontxt').text('');
						$('.petstxt').text('');
						$('.bigpetstxt').text('');
						$('.roomstxt').text('');
						$('.numofnights').text('');

					// if($('#reserve_data_chkin').calendar('get date') && $('#reserve_data_chkout').calendar('get date')){

						_numofnights = moment($('#reserve_data_chkout').calendar('get date')).diff(moment($('#reserve_data_chkin').calendar('get date')), 'days');
						_totalprice = parseFloat(_totalprice * _numofnights);
						_totalprice = parseFloat(_totalprice * $('#reserve_nmroom').val());

						$('.totalprice').html('');
						$('.totalprice').append('รวม : <span class="bedprice" style="margin-left: 13rem;">'+ _totalprice +' บาท</span>');

						$('.checkintxt').text(moment($('#reserve_data_chkin').calendar('get date')).locale('th').format('LL'));
						$('.checkouttxt').text(moment($('#reserve_data_chkout').calendar('get date')).locale('th').format('LL'));
						$('.persontxt').text($('#reserve_nmpeople').val());
						$('.petstxt').text($('#reserve_nmpet').val());
						$('.bigpetstxt').text($('#reserve_nmpetbig').val());
						$('.roomstxt').text($('#reserve_nmroom').val());
						$('.numofnights').text(_numofnights);
						
						// postAddBooking();
						// $('.ui.fullscreen.modal').modal('show');

						$('.ui.fullscreen.modal').modal({
							autofocus: false,
				        	detachable: false,
							onDeny: function() {
							},
							onApprove: function() {

								var firstname 			= $('#firstname').val();
								var lastname 			= $('#lastname').val();
								var tel 				= $('#tel').val();
								var email 				= $('#email').val();

								if(firstname && lastname && tel && email){
									msg_waiting();
									postAddBooking();
								}else{

									$("body").toast({
										class: "error",
										position: 'bottom right',
										message: 'กรุณากรอกข้อมูลส่วนตัวให้ครบถ้วน'
									});
									setTimeout(function(){ 
										$('.ui.fullscreen.modal').modal("show");
									},1000);
								}
							}
						}).modal("show");
					
					} // End if check s tatus success.

					if(result.status == 'error'){
						$("body").toast({
							class: "error",
							displayTime: 5000,
							position: 'bottom right',
							message: 'ไม่สามารถจองได้ เนื่องจากห้องพักประเภทนี้ ถูกจองเต็มแล้ว'
						});
					}
				
				}
			});

		}else{
			$("body").toast({
				class: "error",
				position: 'bottom right',
				message: 'กรุณาเลือกวันเข้าพักและวันสิ้นสุด'
			});
		}
	});
	// window.location.href = "/";
}


function recalculateReservation(_key){
	switch(_key) {
		case 'reserve_nmbathpet':
			// console.log($('#reserve_nmbathpet').val());
			// console.log(parseFloat($('#reserve_nmbathpet').val() * 300));

			_bathprice = parseFloat($('#reserve_nmbathpet').val() * 300);

			$('.bath').html('');
			$('.bath').append('อาบน้ำเป่าขน : x' + $('#reserve_nmbathpet').val() +'<span class="bathprice" style="margin-left: 8.2rem;">'+ parseFloat($('#reserve_nmbathpet').val() * 300) +' บาท</span>');

			break;

		case 'reserve_nmswimpet':
			// $('.swim').text('');
			// $('.swim').text('ว่ายน้ำ : x' + $('#reserve_nmswimpet').val());

			// console.log($('#reserve_nmswimpet').val());
			// console.log(parseFloat($('#reserve_nmswimpet').val() * 300));

			_swimprice = parseFloat($('#reserve_nmswimpet').val() * 300);

			$('.swim').html('');
			$('.swim').append('ว่ายน้ำ : x' + $('#reserve_nmswimpet').val() +'<span class="swimprice" style="margin-left: 11.2rem;">'+ parseFloat($('#reserve_nmswimpet').val() * 300) +' บาท</span>');


			break;

		case 'reserve_nmfood':
			// $('.food').text('');
			// $('.food').text('อาหารเช้าสำหรับผู้เข้าพัก : x' + $('#reserve_nmfood').val());

			// console.log($('#reserve_nmfood').val());
			// console.log(parseFloat($('#reserve_nmfood').val() * 300));

			_foodprice = parseFloat($('#reserve_nmfood').val() * 200);

			$('.food').html('');
			$('.food').append('อาหารเช้าสำหรับผู้เข้าพัก : x' + $('#reserve_nmfood').val() +'<span class="foodprice" style="margin-left: 4rem;">'+ parseFloat($('#reserve_nmfood').val() * 200) +' บาท</span>');


			break;

		case 'reserve_nmfoodpet':
			// $('.petfood').text('');
			// $('.petfood').text('อาหารสำหรับสุนัข : x' + $('#reserve_nmfoodpet').val());

			// console.log($('#reserve_nmfoodpet').val());
			// console.log(parseFloat($('#reserve_nmfoodpet').val() * 300));

			_petfoodprice = parseFloat($('#reserve_nmfoodpet').val() * 200);

			$('.petfood').html('');
			$('.petfood').append('อาหารสำหรับสุนัข : x' + $('#reserve_nmfoodpet').val() +'<span class="petfoodprice" style="margin-left: 7rem;">'+ parseFloat($('#reserve_nmfoodpet').val() * 200) +' บาท</span>');


			break;

		case 'reserve_nmbed':
			// $('.bed').text('');
			// $('.bed').text('เตียงเสริม : x' + $('#reserve_nmbed').val());	

			// console.log($('#reserve_nmbed').val());
			// console.log(parseFloat($('#reserve_nmbed').val() * 300));

			_bedprice = parseFloat($('#reserve_nmbed').val() * 200);

			$('.bed').html('');
			$('.bed').append('เตียงเสริม : x' + $('#reserve_nmbed').val() +'<span class="bedprice" style="margin-left: 10rem;">'+ parseFloat($('#reserve_nmbed').val() * 200) +' บาท</span>');


			break;

		default:
		break;
	}

	_finaltotal = parseFloat(_totalprice + _bathprice + _swimprice + _foodprice + _petfoodprice + _bedprice);
	
	console.log(_finaltotal);

	$('.totalprice').html('');
	$('.totalprice').append('รวม : <span class="bedprice" style="margin-left: 13rem;">'+ _finaltotal +' บาท</span>');
}


function postAddBooking(){

	msg_waiting();
	
	setTimeout(function(){ 
		// var detail_val 			= [];
		var firstname 			= $('#firstname').val();
		var lastname 			= $('#lastname').val();
		var tel 				= $('#tel').val();
		var email 				= $('#email').val();
		var line_id 			= $('#line_id').val();

		var reserve_nmpeople 		= $('#reserve_nmpeople').val();
		var reserve_nmpet 			= $('#reserve_nmpet').val();
		var reserve_nmroom 			= $('#reserve_nmroom').val();
		var reserve_data_chkin		= $('#reserve_data_chkin').calendar('get date');
		var reserve_data_chkout		= $('#reserve_data_chkout').calendar('get date');

		var typeroom_id				= _typeroom_id;
		var reserve_price			= _finaltotal == 0 ? _totalprice : _finaltotal;
		var numofnights				= _numofnights;

		var bathprice				= $('#reserve_nmbathpet').val();
		var swimprice				= $('#reserve_nmswimpet').val();
		var foodprice				= $('#reserve_nmfood').val();
		var petfoodprice			= $('#reserve_nmfoodpet').val();
		var bedprice				= $('#reserve_nmbed').val();

		let addPetinfo = [];
		
		console.log(_finaltotal);

		//ยังไม่เสร็จ
		for (var i = 0; i < _totalpet; i++) {
			var j = i+1;
			$('#type_id_'+j).dropdown('get value');
			$('#species_id_'+j).dropdown('get value');
			$('#pet_gender_'+j).dropdown('get value');
			$('#line_id').val();

			if($('#pet_name_'+j).val() && $('#pet_gender_'+j).dropdown('get value') && $('#type_id_'+j).dropdown('get value') && $('#species_id_'+j).dropdown('get value')){
				var petInfo = {
					pet_name 	: $('#pet_name_'+j).val(),
					pet_gender 	: $('#pet_gender_'+j).dropdown('get value'),
					type_id 	: $('#type_id_'+j).dropdown('get value'),
					species_id 	: $('#species_id_'+j).dropdown('get value'),
					pet_birthday: $('#pet_birthday_'+j).calendar('get date') ? moment($('#pet_birthday_'+j).calendar('get date')).format('YYYY-MM-DD') : null,
					pet_remark 	: $('#pet_remark_'+j).val()
				}

				addPetinfo.push(petInfo);

			}

			console.log(j);
		}

		console.log(addPetinfo);

		var add_url		= $('#add-url').data('url');
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: add_url,
			data: {
			  	// 'detail_val' : detail_val,
			  	'firstname' : firstname,
			  	'lastname' : lastname,
			  	'tel' : tel,
			  	'email' : email,
			  	'line_id' : line_id,
			  	'reserve_nmpeople' : reserve_nmpeople,
			  	'reserve_nmpet' : reserve_nmpet,
			  	'reserve_nmroom' : reserve_nmroom,
			  	'reserve_data_chkin' : moment(reserve_data_chkin).format('YYYY-MM-DD'),
			  	'reserve_data_chkout' : moment(reserve_data_chkout).format('YYYY-MM-DD'),
			  	'typeroom_id' : typeroom_id,
			  	'reserve_price' : reserve_price,
			  	'numofnights' : numofnights,
			  	'bathprice' : bathprice,
			  	'swimprice' : swimprice,
			  	'foodprice' : foodprice,
			  	'petfoodprice' : petfoodprice,
			  	'bedprice' : bedprice,
			  	'addPetinfo' : JSON.stringify(addPetinfo),
			},
			success: function(result) {
				msg_stop();
				if(result.status == 'success'){
					setTimeout(function(){ 
						$("body").toast({
							class: "success",
							position: 'bottom right',
							message: `บันทึกเสร็จสิ้น`
						});

						window.location.href = '/bookingdetail/'+result.reserveID;

						// setTimeout(function(){ 
						// 	Swal.fire({
					 //            title: "ท่านสามารถลงชื่อเข้าใช้งาน เพื่อตรวจสอบสถานะโดยใช้ username และ password นี้",
					 //            text: 'username : '+result.username+' | password : '+result.password,
					 //            icon: 'info',
					 //            showCancelButton: false,
					 //            confirmButtonColor: "#59B855",
					 //            // cancelButtonText: "ยกเลิก",
					 //            // cancelButtonColor: "",
					 //            confirmButtonText: "ตกลง"
					 //        }).then(result => {
					 //            if (result.value) {
			   //              		window.location.href = '/';
					 //            }
					 //        });
			   //          }, 1000);
					});
				} // End if check s tatus success.

				if(result.status == 'error'){

					setTimeout(function(){ 
						Swal.fire({
				            // title: "ท่านสามารถลงชื่อเข้าใช้งาน เพื่อตรวจสอบสถานะโดยใช้ username และ password นี้",
				            title: "ไม่สามารถจองได้ !!",
				            // text: 'username : '+result.username+' | password : '+result.password,
				            text: 'เนื่องจากห้องพักประเภทนี้ ถูกจองเต็มแล้ว กรุณาทำรายการใหม่',
				            icon: 'error',
				            showCancelButton: false,
				            confirmButtonColor: "#59B855",
				            // cancelButtonText: "ยกเลิก",
				            // cancelButtonColor: "",
				            confirmButtonText: "ตกลง"
				        }).then(result => {
				            if (result.value) {
		                		window.location.href = '/';
				            }
				        });
		            }, 1000);

					// $("body").toast({
					// 	class: "error",
					// 	position: 'bottom right',
					// 	message: result.msg
					// });
				}
			
			}
		});

	});
}


function msg_waiting() {
    Swal.fire({title: '',html: 'LOADING', imageUrl: '../themes/image/Spinner.gif',  showConfirmButton: false});
}


function msg_stop() {
    Swal.close();
}

// function msg_waiting() {
// 	let timerInterval;
// 	Swal.fire({
// 		title: '',
// 		html: 'LOADING',
// 		timer: 2000,
// 		timerProgressBar: true,
// 		onBeforeOpen: () => {
// 			Swal.showLoading()
// 			timerInterval = setInterval(() => {
// 				const content = Swal.getContent()
// 				if (content) {
// 					const b = content.querySelector('b')
// 					if (b) {
// 						b.textContent = Swal.getTimerLeft()
// 					}
// 				}
// 			}, 100)
// 		},
// 		onClose: () => {
// 			clearInterval(timerInterval)
// 		}
// 	}).then((result) => {
// 		/* Read more about handling dismissals below */
// 		if (result.dismiss === Swal.DismissReason.timer) {
// 			console.log('I was closed by the timer')
// 		}
// 	});
// }



function checkPormoCode(promo_code){
	console.log('checkPormoCode',promo_code);
    
    $('.discount').text('');
	
    if(promo_code == 'M1010'){
        $('.thismsgcheckpro').css('display', 'unset');
        $('#msgcheckpro').text('โค้ดส่วนลดนี้สามารถใช้ได้');

        var discountper = parseFloat((_finaltotal / 100)*10);
      	_discount 	=  parseFloat(_finaltotal - discountper);
      	_finaltotal = _discount;
       
        console.log(discountper);
        console.log(_discount);

        $('.discount').text('ส่วนลด '+discountper+' บาท (คุณได้รับส่วนลด 10% จากราคาจริง)');

        $('.totalprice').html('');
		$('.totalprice').append('รวม : <span class="bedprice" style="margin-left: 13rem;">'+ _finaltotal +' บาท</span>');
        
    }else{
    	$('.thismsgcheckpro').css('display', 'unset');
    	$('#msgcheckpro').text('โค้ดส่วนลดไม่ถูกต้อง');
    }

}