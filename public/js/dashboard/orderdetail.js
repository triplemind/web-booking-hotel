$(function() {

	$('#room_id').dropdown();

	// $("#idCard").inputFilter(function(value) {
 //  		return /^-?\d*$/.test(value); 
 //  	});

	$('#reserve_data_chkin').calendar({
		type: 'date',
		monthFirst: false,
        ampm: false,
        formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
	});

	$('#reserve_time_chkin').calendar({
		type: 'time',
		monthFirst: false,
        ampm: false,
	});


	$('#reserve_data_chkout').calendar({
		type: 'date',
		monthFirst: false,
        ampm: false,
        formatter: {
			date: function (date, settings) {
				if (!date) return '';
				var day = date.getDate();
				var month = date.getMonth() + 1;
				var year = date.getFullYear();
				return day + '/' + month + '/' + year;
			}
		},
	});

	$('#reserve_data_time').calendar({
		type: 'time',
		monthFirst: false,
        ampm: false,
	});


	$('.btn-back').on('click', function(){
		window.location.href = '/admin/dashboard';
	});

	$('.btn-confirm').on('click', function(){
		var status = $(this).data('status');
		console.log(status);
		Swal.fire({
	        title: "คุณต้องการแก้ไขสถานะของการจองนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#18db32",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            CompletedOrder(status);
	        }
	    });
	});

	$('.btn-cancel').on('click', function(){
		Swal.fire({
	        title: "คุณต้องการยกเลิกการจองนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            CancelOrder();
	        }
	    });
	});



	$('.btn-checkin').on('click', function(){
		var status = $(this).data('status');
		var action = 'checkin';
		console.log(status);

        saveDateTimeIN(status,action);
	});


	$('.btn-checkout').on('click', function(){
		var status = $(this).data('status');
		var action = 'checkout';
		console.log(status);
		
        saveDateTimeOut(status,action);
	});

});


function CancelOrder() {
	var reserve_id = $('#reserve_id').val();

	var method 		= 'CancelOrder';
	var ajax_url	= $('#ajax-center-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
		  	 'reserve_id' : reserve_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ยกเลิกรายการเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}

function CompletedOrder(status) {
	var reserve_id = $('#reserve_id').val();

	var method 		= 'CompletedOrder';
	var ajax_url	= $('#ajax-center-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
		  	'reserve_id' : reserve_id,
		  	'status' : status,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ดำเนินการเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}


function saveDateTimeIN(status,action){
	var idCard 				= $('#idCard').val();
	var realname 			= $('#realname').val();
	var reserve_id 			= $('#reserve_id').val();
	var reserve_data_chkin 	= $('#reserve_data_chkin').calendar('get date');
	var reserve_time_chkin 	= $('#reserve_time_chkin').calendar('get date');
	var room_id 			= $('#room_id').dropdown('get value');

	var method 		= 'saveDateTimeINOut';
	var ajax_url	= $('#ajax-center-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
		  	'action' : action,
		  	'realname' : realname,
		  	'idCard' : idCard,
		  	'status' : status,
		  	'reserve_id' : reserve_id,
		  	'reserve_data_chkin' : moment(reserve_data_chkin).locale('th').format('YYYY-MM-DD'),
		  	'reserve_time_chkin' : moment(reserve_time_chkin).locale('th').format('h:mm'),
		  	'room_id' : room_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ดำเนินการเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}


function saveDateTimeOut(status,action){
	var reserve_id 			= $('#reserve_id').val();
	var reserve_data_chkout = $('#reserve_data_chkout').calendar('get date');
	var reserve_data_time 	= $('#reserve_data_time').calendar('get date');

	var method 		= 'saveDateTimeINOut';
	var ajax_url	= $('#ajax-center-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: ajax_url,
		data: {
			'method' : method,
		  	'status' : status,
		  	'action' : action,
		  	'reserve_id' : reserve_id,
		  	'reserve_data_chkout' : moment(reserve_data_chkout).locale('th').format('YYYY-MM-DD'),
		  	'reserve_data_time' : moment(reserve_data_time).locale('th').format('h:mm'),
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ดำเนินการเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}