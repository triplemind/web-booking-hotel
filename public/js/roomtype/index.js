$(function(){

	DTRender('#TBL_RoomType', true);

	$('.btn-addroomtype').on('click', function(){
		$('#checkindate').calendar({
			type: 'time',
			popupOptions: {
	            observeChanges: false
	        },
		});

		$('#checkoutdate').calendar({
			type: 'time',
			popupOptions: {
	            observeChanges: false
	        },
		});

		// $('#user_type').dropdown();
		$("#addRoomTypeModal").modal({
			closable: false,
			onDeny: function() {
			},
			onApprove: function() {
				addNewRoomType();
			}
		}).modal("show");
	});


	$('.editroomtype').on('click', function(){
		var typeroom_id = $(this).data('id');

		$('#ed_checkindate').calendar({
			type: 'time',
			popupOptions: {
	            observeChanges: false
	        },
		});

		$('#ed_checkoutdate').calendar({
			type: 'time',
			popupOptions: {
	            observeChanges: false
	        },
		});

		var method 		= 'getRoomTypeData';
		var ajax_url	= $('#ajax-center-url').data('url');
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: ajax_url,
			data: {
			  	'method' : method,
			  	'typeroom_id' : typeroom_id,
			},
			success: function(result) {
				if(result.status == 'success'){

					$('#typeroom_id').val(result.data.typeroom_id);
					$('#ed_type_nameroom').val(result.data.type_nameroom);
					$('#ed_type_rate').val(result.data.type_rate);
					// $('#ed_cost').val(result.data.cost);
					$('#ed_size').val(result.data.size);
					$('#ed_people').val(result.data.people);
					$('#ed_extra').val(result.data.extra);
					$('#ed_bigpet').val(result.data.bigpet);
					$('#ed_smallpet').val(result.data.smallpet);
					$('#ed_info').val(result.data.info);
					$('#ed_checkindate').calendar('set date', result.data.checkindate);
					$('#ed_checkoutdate').calendar('set date', result.data.checkoutdate);

					setTimeout(function(){
						$("#EditRoomTypeModal").modal({
							closable: false,
							onDeny: function() {
							},
							onApprove: function() {
								editRoomType(typeroom_id);
							}
						}).modal("show");
			        });
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.data
					});
				}
			}
		});

	});

	$('.deleteroomtype').on('click', function(){
		console.log($(this).data('id'));
		var typeroom_id = $(this).data('id');
		Swal.fire({
	        title: "คุณต้องการลบประเภทห้องพักนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            deleteRoomType(typeroom_id);
	        }
	    });
	});

});


function addNewRoomType() {
	var type_nameroom 		= $('#type_nameroom').val();
	var type_rate 			= $('#type_rate').val();
	// var cost 				= $('#cost').val();
	var size 				= $('#size').val();
	var people 				= $('#people').val();
	var extra 				= $('#extra').val();
	var bigpet 				= $('#bigpet').val();
	var smallpet 			= $('#smallpet').val();
	var info 				= $('#info').val();
	var checkindate			= $('#checkindate').calendar('get date');
	var checkoutdate		= $('#checkoutdate').calendar('get date');

	var add_url			= $('#add-url').data('url');
	var data  			= new FormData();

	if (typeof($('#room_img')[0]) !== 'undefined') {
        jQuery.each(jQuery('#room_img')[0].files, function(i, file) {
            data.append('room_img', file);
        });
    }

	data.append('type_nameroom', type_nameroom);
	data.append('type_rate',type_rate);
	// data.append('cost',cost);
	data.append('size',size);
	data.append('people',people);
	data.append('extra',extra);
	data.append('bigpet',bigpet);
	data.append('smallpet',smallpet);
	data.append('info',info);
	data.append('checkindate',moment(checkindate).locale('th').format('h:mm'));
	data.append('checkoutdate',moment(checkoutdate).locale('th').format('h:mm'));



	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: add_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});

}


function editRoomType(typeroom_id) {
	var type_nameroom 		= $('#ed_type_nameroom').val();
	var type_rate 			= $('#ed_type_rate').val();
	// var cost 				= $('#ed_cost').val();
	var size 				= $('#ed_size').val();
	var people 				= $('#ed_people').val();
	var extra 				= $('#ed_extra').val();
	var bigpet 				= $('#ed_bigpet').val();
	var smallpet 			= $('#ed_smallpet').val();
	var info 				= $('#ed_info').val();
	var checkindate			= $('#ed_checkindate').calendar('get date');
	var checkoutdate		= $('#ed_checkoutdate').calendar('get date');

	var edit_url		= $('#edit-url').data('url');
	var data  			= new FormData();

	if (typeof($('#ed_room_img')[0]) !== 'undefined') {
        jQuery.each(jQuery('#ed_room_img')[0].files, function(i, file) {
            data.append('room_img', file);
        });
    }

	data.append('typeroom_id', typeroom_id);
	data.append('type_nameroom', type_nameroom);
	data.append('type_rate',type_rate);
	// data.append('cost',cost);
	data.append('size',size);
	data.append('people',people);
	data.append('extra',extra);
	data.append('bigpet',bigpet);
	data.append('smallpet',smallpet);
	data.append('info',info);
	data.append('checkindate',moment(checkindate).locale('th').format('h:mm'));
	data.append('checkoutdate',moment(checkoutdate).locale('th').format('h:mm'));

	console.log(moment(checkindate).locale('th').format('LT'));
	console.log(moment(checkoutdate).locale('th').format('LT'));
	
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: edit_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});

}



function deleteRoomType(typeroom_id){
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'typeroom_id' : typeroom_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}

const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}