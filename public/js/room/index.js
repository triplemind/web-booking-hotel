$(function(){

	DTRender('#TBL_Room', true);

	$('.btn-addroom').on('click', function(){
		$('#typeroom').dropdown();
		$('#rest_status').dropdown();
		$('#rest_statususe').dropdown();

		$("#addRoomModal").modal({
			closable: false,
			onDeny: function() {
			},
			onApprove: function() {
				addNewRoom();
			}
		}).modal("show");
	});


	$('.editroom').on('click', function(){
		var rm_id = $(this).data('id');

		$('#ed_typeroom').dropdown();
		$('#ed_rest_status').dropdown();
		$('#ed_rest_statususe').dropdown();

		var method 		= 'getRoomData';
		var ajax_url	= $('#ajax-center-url').data('url');
		$.ajax({
			headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
			type: 'post',
			url: ajax_url,
			data: {
			  	'method' : method,
			  	'rm_id' : rm_id,
			},
			success: function(result) {
				if(result.status == 'success'){

					$('#rm_id').val(result.data.rm_id);
					$('#ed_rm_name').val(result.data.rm_name);
					$('#ed_room_number').val(result.data.room_number);
					$('#ed_typeroom').dropdown('set selected', result.data.typeroom_id);
					$('#ed_typeroom').dropdown('set selected', result.data.typeroom_id);
					$('#ed_rest_status').dropdown('set selected', result.data.rest_status);
					$('#ed_rest_statususe').dropdown('set selected', result.data.isusable);

					setTimeout(function(){
						$("#EditRoomModal").modal({
							closable: false,
							onDeny: function() {
							},
							onApprove: function() {
								editRoomType(rm_id);
							}
						}).modal("show");
			        });
				} // End if check s tatus success.

				if(result.status == 'error'){
					$("body").toast({
						class: "error",
						position: 'bottom right',
						message: result.data
					});
				}
			}
		});

	});

	$('.deleteroom').on('click', function(){
		console.log($(this).data('id'));
		var rm_id = $(this).data('id');
		Swal.fire({
	        title: "คุณต้องการลบห้องพักนี้ใช่หรือไม่ ?",
	        showCancelButton: true,
	        confirmButtonColor: "#db1818",
	        cancelButtonText: "ยกเลิก",
	        cancelButtonColor: "",
	        confirmButtonText: "ตกลง"
	    }).then(result => {
	        if (result.value) {
	            deleteRoomType(rm_id);
	        }
	    });
	});

});


function addNewRoom() {
	var typeroom_id 	= $('#typeroom').dropdown('get value');
	var rest_status 	= $('#rest_status').dropdown('get value');
	var rest_statususe 	= $('#rest_statususe').dropdown('get value');
	var room_number 	= $('#room_number').val();
	var rm_name 		= $('#rm_name').val();

	console.log(typeroom_id);

	var add_url			= $('#add-url').data('url');
	var data  			= new FormData();

	data.append('typeroom_id', typeroom_id);
	data.append('rest_status',rest_status);
	data.append('room_number',room_number);
	data.append('rest_statususe',rest_statususe);
	data.append('rm_name',rm_name);
	
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: add_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});

}


function editRoomType(rm_id) {
	var typeroom_id 	= $('#ed_typeroom').dropdown('get value');
	var rest_status 	= $('#ed_rest_status').dropdown('get value');
	var room_number 	= $('#ed_room_number').val();
	var rm_name 		= $('#ed_rm_name').val();
	var rest_statususe 	= $('#ed_rest_statususe').dropdown('get value');

	var edit_url		= $('#edit-url').data('url');
	var data  			= new FormData();

	console.log(typeroom_id);
	console.log(room_number);

	data.append('typeroom_id', typeroom_id);
	data.append('rm_name', rm_name);
	data.append('rest_status', rest_status);
	data.append('rm_id',rm_id);
	data.append('room_number',room_number);
	data.append('rest_statususe',rest_statususe);
	
	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: edit_url,
		data: data,
		contentType: false,
        processData:false,
        cache: false,
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `บันทึกเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
			// showForm(form, action, error, data);
		}
	});

}



function deleteRoomType(rm_id){
	var delete_url		= $('#delete-url').data('url');

	$.ajax({
		headers: { 'X-CSRF-Token' : $('input[name=_token]').attr('value') },
		type: 'post',
		url: delete_url,
		data: {
		  	 'rm_id' : rm_id,
		},
		success: function(result) {
			if(result.status == 'success'){
				setTimeout(function(){ 
					$("body").toast({
						class: "success",
						position: 'bottom right',
						message: `ลบข้อมูลเสร็จสิ้น`
					});
					setTimeout(function(){ 
		                window.location.reload();
		            }, 1000);
				});
			} // End if check s tatus success.

			if(result.status == 'error'){
				$("body").toast({
					class: "error",
					position: 'bottom right',
					message: result.msg
				});
			}
		},
		error : function(error) {
		}
	});
}

const DTRender = (data, state = false) => {
	const table = $(`${data}`);
	state ? table.DataTable().destroy() : state;
	const option = {
		retrieve: true,
		dom:
		"<'ui stackable grid'" +
		"<'row dt-table'" +
		"<'sixteen wide column'tr>" +
		">" +
		"<'row'" +
		"<'seven wide column'i>" +
		"<'right aligned nine wide column'p>" +
		">" +
		">",
		ordering: false,
		lengthChange: false,
		pageLength: 10,
		language: {
			decimal: "",
			emptyTable: "ไม่มีรายการแสดง",
			info: "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
			infoEmpty: "แสดง 0 ถึง 0 จาก 0 รายการ",
			infoFiltered: "(กรองจาก _MAX_ รายการทั้งหมด)",
			infoPostFix: "",
			thousands: ",",
			lengthMenu: "แสดง _MENU_ รายการ",
			loadingRecords: "กำลังโหลด...",
			processing: "กำลังคำนวน...",
			search: "ค้นหา:",
			zeroRecords: "ไม่เจอรายการที่ค้นหา",
			paginate: {
				first: "หน้าแรก",
				last: "หน้าสุดท้าย",
				next: ">",
				previous: "<"
			},
			aria: {
				sortAscending: ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปหามาก",
				sortDescending: ": เปิดใช้งานเพื่อเรียงลำดับคอลัมน์จากมากไปหาน้อย"
			}
		}
	};
	setTimeout(function(){
		table.DataTable(option);
	});
	
	return true;
}