<?php

namespace App\Http\Controllers\PetType;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\RegisterStay\RegisterStay;
use App\Services\Reserve\Reserve;
use App\Services\Room\Room;
use App\Services\RoomType\RoomType;
use App\Services\Species\Species;

class PetTypeController extends Controller
{
    public function getIndex()
    {
        $getpettypes = PetType::where('isDelete', false)->get();
        $getspecies = Species::where('isDelete', false)->get();
        
        return $this->view_admin('pettype.index', compact('getpettypes', 'getspecies'));
    }

    public function postAdd()
    {

        $type_pet       = \Input::has('type_pet') ? \Input::get('type_pet') : '';
        $status         = \Input::has('status') ? \Input::get('status') : '';
        $speciesid      = \Input::has('speciesid') ? \Input::get('speciesid') : '';
        $species        = \Input::has('species') ? \Input::get('species') : '';
        $type_petsize   = \Input::has('type_petsize') ? \Input::get('type_petsize') : '';

        $new_pettype                = new PetType;
        $new_pettype->type_pet      = $type_pet;
        $new_pettype->status        = $status;
        $new_pettype->species_id    = $speciesid;
        $new_pettype->species_pet   = $species;
        $new_pettype->type_petsize  = $type_petsize;
        $new_pettype->isDelete      = false;
        $new_pettype->save();
        
        return ['status' => 'success'];
    }

    public function postEdit()
    {
        $id             = \Input::has('id') ? \Input::get('id') : '';
        $type_pet       = \Input::has('type_pet') ? \Input::get('type_pet') : '';
        $status         = \Input::has('status') ? \Input::get('status') : '';
        $speciesid      = \Input::has('speciesid') ? \Input::get('speciesid') : '';
        $species        = \Input::has('species') ? \Input::get('species') : '';
        $type_petsize   = \Input::has('type_petsize') ? \Input::get('type_petsize') : '';

        // sd($id);

        $pettype  = PetType::where('type_id', $id)->first();
        if(empty($pettype)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
        
        $pettype->type_pet      = $type_pet;
        $pettype->status        = $status;
        $pettype->species_id    = $speciesid;
        $pettype->species_pet   = $species;
        $pettype->type_petsize  = $type_petsize;
        $pettype->isDelete      = false;
        $pettype->save();
        
        return ['status' => 'success'];
    }

    public function postRemove()
    {
        $id    = \Input::has('id') ? \Input::get('id') : '';

        $pettype  = PetType::where('type_id', $id)->first();
        if(empty($pettype)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $pettype->isDelete  = true;
        $pettype->save();

        return ['status' => 'success'];
    }


    public function postAddSpecies()
    {
        $name       = \Input::has('name') ? \Input::get('name') : '';
        $shortname  = \Input::has('shortname') ? \Input::get('shortname') : '';
        $status     = \Input::has('status') ? \Input::get('status') : '';
        $pet_type   = \Input::has('pet_type') ? \Input::get('pet_type') : '';

        $new_species                = new Species;
        $new_species->name          = $name;
        $new_species->shortname     = $shortname;
        $new_species->status        = $status;
        $new_species->pet_type      = $pet_type;
        $new_species->save();

        return ['status' => 'success'];
    }


    public function postEditSpecies()
    {
        $species_id = \Input::has('id') ? \Input::get('id') : '';
        $name       = \Input::has('name') ? \Input::get('name') : '';
        $shortname  = \Input::has('shortname') ? \Input::get('shortname') : '';
        $status     = \Input::has('status') ? \Input::get('status') : '';
        $pet_type   = \Input::has('pet_type') ? \Input::get('pet_type') : '';

        $species  = Species::where('id', $species_id)->first();
        if(empty($species)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
        
        $species->name          = $name;
        $species->shortname     = $shortname;
        $species->status        = $status;
        $species->pet_type      = $pet_type;
        $species->save();

        return ['status' => 'success'];
    }


    public function postRemoveSpecies()
    {
        $species_id    = \Input::has('id') ? \Input::get('id') : '';

        $species  = Species::where('id', $species_id)->first();
        if(empty($species)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $species->isDelete  = true;
        $species->save();

        return ['status' => 'success'];
    }

    public function ajaxCenter()
    {
        $method = \Input::has('method') ? \Input::get('method') : '';

        switch ($method) {

            case 'getSpeciesData':

                $species_id = \Input::has('id') ? \Input::get('id') : '';

                $species  = Species::where('id', $species_id)->first();
                // if(empty($species)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                if(!empty($species)){
                    return ['status' => 'success', 'data' => $species];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            case 'getPetTypeData':

                $id = \Input::has('id') ? \Input::get('id') : '';

                $pettype  = PetType::where('type_id', $id)->first();

                if(!empty($pettype)){
                    return ['status' => 'success', 'data' => $pettype];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            default:
                return ['status' => 'error'];
                break;
        }
    }
}
