<?php

namespace App\Http\Controllers\Room;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\RegisterStay\RegisterStay;
use App\Services\Reserve\Reserve;
use App\Services\Room\Room;
use App\Services\RoomType\RoomType;

class RoomController extends Controller
{
    public function getIndex()
    {
        $getRooms = Room::with(['roomtype'])->where('isDelete', false)->get();

        $getRoomTypes = RoomType::where('isDelete', 0)->get();

        $allrooms = Room::with(['roomtype'])->get();


        $roomcount = $allrooms->count();
        $roomcount = $roomcount+1;
        // sd($roomcount);
        
        return $this->view_admin('room.index',compact('getRooms', 'getRoomTypes','roomcount'));
    }

    public function postAdd()
    {
        $typeroom_id  = \Input::has('typeroom_id') ? \Input::get('typeroom_id') : '';
        $rest_status  = \Input::has('rest_status') ? \Input::get('rest_status') : '';
        $room_number  = \Input::has('room_number') ? \Input::get('room_number') : '';
        $rest_statususe  = \Input::has('rest_statususe') ? \Input::get('rest_statususe') : '';
        $rm_name        = \Input::has('rm_name') ? \Input::get('rm_name') : '';

        $new_room                   = new Room;
        $new_room->typeroom_id      = $typeroom_id;
        $new_room->rm_name          = $rm_name;
        $new_room->rest_status      = $rest_status;
        $new_room->room_number      = $room_number;
        $new_room->isusable         = ($rest_statususe == 1) ? true : false;
        $new_room->isDelete         = false;
        $new_room->save();

        return ['status' => 'success'];
    }

    public function postEdit()
    {
        $rm_id        = \Input::has('rm_id') ? \Input::get('rm_id') : '';
        $typeroom_id  = \Input::has('typeroom_id') ? \Input::get('typeroom_id') : '';
        $rest_status  = \Input::has('rest_status') ? \Input::get('rest_status') : '';
        $room_number  = \Input::has('room_number') ? \Input::get('room_number') : '';
        $rest_statususe  = \Input::has('rest_statususe') ? \Input::get('rest_statususe') : '';
        $rm_name        = \Input::has('rm_name') ? \Input::get('rm_name') : '';

        $room = Room::where('rm_id', $rm_id)->first();
        if(empty($room)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $room->rm_name          = $rm_name;
        $room->typeroom_id      = $typeroom_id;
        $room->room_number      = $room_number;
        $room->isusable         = ($rest_statususe == 1) ? true : false;
        $room->rest_status      = $rest_status;
        $room->save();

        return ['status' => 'success'];
    }

    public function postRemove()
    {
        $rm_id    = \Input::has('rm_id') ? \Input::get('rm_id') : '';

        $room = Room::where('rm_id', $rm_id)->first();
        if(empty($room)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $room->isDelete = true;
        $room->save();
        
        return ['status' => 'success'];
    }

    public function ajaxCenter()
    {
        $method = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
            case 'getRoomData':

                $rm_id = \Input::has('rm_id') ? \Input::get('rm_id') : '';

                $room = Room::where('rm_id', $rm_id)->first();

                if(!empty($room)){
                    return ['status' => 'success', 'data' => $room];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }
                
                break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
