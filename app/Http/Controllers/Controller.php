<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Services\Users\UserObject;
use App\Services\Users\User;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public $userObject;


    public function __construct()
    {
        $this->userObject   = new UserObject;
    }
     public function view($page, $data = [])
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_Fname     = empty($user_obj->firstname) ? "" : $user_obj->firstname;
        $user_Lname     = empty($user_obj->lastname) ? "" : $user_obj->lastname;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_id        = empty($user_obj->user_id) ? "" : $user_obj->user_id;


        $layout     = 'layouts/main';

        return view($layout, compact('page', 'data', 'user_Fname', 'user_Lname', 'username', 'user_type', 'user_id'));
    }

    public function view_admin($page, $data = [])
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_Fname     = empty($user_obj->firstname) ? "" : $user_obj->firstname;
        $user_Lname     = empty($user_obj->lastname) ? "" : $user_obj->lastname;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_id        = empty($user_obj->user_id) ? "" : $user_obj->user_id;


        
        // $count_all = \Session::has('count_all') ? \Session::get('count_all') : 0;
        // $userObject = $this->userObject;
        $layout     = 'adminlayouts/main';

        return view($layout, compact('page', 'data', 'user_Fname', 'user_Lname', 'username', 'user_type', 'user_id'));
    }
}
