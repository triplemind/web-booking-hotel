<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\RegisterStay\RegisterStay;
use App\Services\Reserve\Reserve;
use App\Services\Room\Room;
use App\Services\RoomType\RoomType;

class UserController extends Controller
{
    public function getIndex()
    {
        // $filters = \Session::has('user_filters') ? \Session::get('user_filters') : [];
        
        $users = User::where('user_id', '!=', 0)->get();

        
        // sd($users->toArray());

    	return $this->view_admin('user.index',compact('users'));
    }

    public function postAdd()
    {
        $firstname     = \Input::has('firstname') ? \Input::get('firstname') : '';
        $lastname      = \Input::has('lastname') ? \Input::get('lastname') : '';
        $username      = \Input::has('username') ? \Input::get('username') : '';
        $user_type     = \Input::has('user_type') ? \Input::get('user_type') : '';
        $tel           = \Input::has('tel') ? \Input::get('tel') : '';
        $email         = \Input::has('email') ? \Input::get('email') : '';
        $password      = \Input::has('password') ? \Input::get('password') : '';
        $line_id       = \Input::has('line_id') ? \Input::get('line_id') : '';
       

        $new_user               = new User;
        $new_user->firstname    = $firstname;
        $new_user->lastname     = $lastname;
        $new_user->email        = $email;
        $new_user->user_type    = $user_type;
        $new_user->username     = $username;
        $new_user->password     = $password;
        $new_user->line_id      = $line_id;
        $new_user->tel          = $tel;
        $new_user->save();

        return ['status' => 'success'];
    }


    public function postEdit()
    {
        $user_id       = \Input::has('user_id') ? \Input::get('user_id') : '';
        $firstname     = \Input::has('firstname') ? \Input::get('firstname') : '';
        $lastname      = \Input::has('lastname') ? \Input::get('lastname') : '';
        $username      = \Input::has('username') ? \Input::get('username') : '';
        $user_type     = \Input::has('user_type') ? \Input::get('user_type') : '';
        $tel           = \Input::has('tel') ? \Input::get('tel') : '';
        $email         = \Input::has('email') ? \Input::get('email') : '';
        $password      = \Input::has('password') ? \Input::get('password') : '';
        $line_id       = \Input::has('line_id') ? \Input::get('line_id') : '';

        
        $chk_username   = User::where('username', $username)->count();
        if($chk_username > 1) return ['status' => 'error', 'msg' => 'ชื่อผู้ใช้งานนี้มีอยู่ในระบบแล้ว'];

        $chk_email  = User::where('email', $email)->count();
        if($chk_email > 1) return ['status' => 'error', 'msg' => 'อีเมลนี้มีอยู่ในระบบแล้ว'];

        $user       = User::where('user_id', $user_id)->first();
        if(empty($user)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

       
        $user->firstname    = $firstname;
        $user->lastname     = $lastname;
        $user->email        = $email;
        $user->user_type    = $user_type;
        $user->username     = $username;
        $user->password     = $password;
        $user->line_id      = $line_id;
        $user->tel          = $tel;
        $user->save();

        return ['status' => 'success'];
    }



    public function postRemove()
    {
        $user_id    = \Input::has('user_id') ? \Input::get('user_id') : '';
        $user       = User::where('user_id', $user_id)->first();

        if(empty($user)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        $user->delete();

        return ['status' => 'success'];
    }



    public function ajaxCenter()
    {
        $method         = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
            case 'getUserData':

                $user_id = \Input::has('user_id') ? \Input::get('user_id') : '';

                $user = User::where('user_id', $user_id)->first();

                if(!empty($user)){
                    return ['status' => 'success', 'data' => $user];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }

                
                break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
