<?php

namespace App\Http\Controllers\Auth;

// use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Auths\UserAuth;
use App\Services\Users\User;


class RegisterController extends Controller
{
    public function getRegister(){

        // เรียกหน้า view register
        return view('auth.register');
    }


    // function สำหรับ save ข้อมูลลง database 
    public function postRegister(){
        $firstname     = \Input::has('firstname') ? \Input::get('firstname') : '';
        $lastname      = \Input::has('lastname') ? \Input::get('lastname') : '';
        $username      = \Input::has('username') ? \Input::get('username') : '';
        $user_type     = \Input::has('user_type') ? \Input::get('user_type') : '';
        $email         = \Input::has('email') ? \Input::get('email') : '';
        $password      = \Input::has('password') ? \Input::get('password') : '';
        $tel           = \Input::has('tel') ? \Input::get('tel') : '';
        $line_id       = \Input::has('line_id') ? \Input::get('line_id') : '';

        // ตรวจสอบว่ามี username นี้ในระบบแล้วหรือยัง
        $chk_username   = User::where('username', $username)->count();
        // ถ้ามีก็ return error กลับไป
        if($chk_username >= 1) return ['status' => 'error', 'msg' => 'Username is ready exits.'];

        // ตรวจสอบว่ามี username นี้ในระบบแล้วหรือยัง
        $chk_email  = User::where('email', $email)->count();
        // ถ้ามีก็ return error กลับไป
        if($chk_email >= 1) return ['status' => 'error', 'msg' => 'Email is ready exits.'];

        // save ข้อมูลลง database
        $new_user               = new User;
        $new_user->firstname    = $firstname;
        $new_user->lastname     = $lastname;
        $new_user->email        = $email;
        $new_user->user_type    = $user_type;
        $new_user->username     = $username;
        $new_user->password     = $password;
        $new_user->tel          = $tel;
        $new_user->line_id      = $line_id;
        $new_user->save();

        return ['status' => 'success'];
    }


    public function ajaxCenter()
    {
        $method         = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
            case '':

                
                
                break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
