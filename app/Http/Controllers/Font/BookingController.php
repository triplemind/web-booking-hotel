<?php

namespace App\Http\Controllers\Font;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\RegisterStay\RegisterStay;
use App\Services\Reserve\Reserve;
use App\Services\Room\Room;
use App\Services\RoomType\RoomType;
use App\Services\Species\Species;

class BookingController extends Controller
{
    public function getIndex(Request $request, $id)
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_Fname     = empty($user_obj->first_name) ? "" : $user_obj->first_name;
        $user_Lname     = empty($user_obj->last_name) ? "" : $user_obj->last_name;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->user_id) ? "" : $user_obj->user_id;

        $getRoomType = RoomType::where('typeroom_id', $id)
                        ->where('isDelete', false)
                        ->first();

        $user = User::where('user_id', $user_id)->first();
        
        return $this->view('font.booking',compact('getRoomType', 'user'));
    }

    public function getBookingDetail(Request $request, $id)
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_Fname     = empty($user_obj->first_name) ? "" : $user_obj->first_name;
        $user_Lname     = empty($user_obj->last_name) ? "" : $user_obj->last_name;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->user_id) ? "" : $user_obj->user_id;


        $reserves = Reserve::with(['roomtype','payment'])->where('reserve_id', $id)->first();

        $getpettypes = PetType::where('isDelete', false)->where('status', 'Active')->get();
        $getspecies = Species::where('isDelete', false)->where('status', 'Active')->get();


        if(!empty($reserves->pay_id)){

            $getpayment = Payment::where('pay_id', $reserves->pay_id)->first();
        }else{
            $getpayment = null;
        }

        // d(json_decode($reserves->pet_id,true));
        // d($reserves->pet_id);

        $petdetailArr = array();

        if(!empty($reserves->pet_id)){
            $petID = json_decode($reserves->pet_id,true);

            for ($i=0; $i < count($petID); $i++) { 
                $getpet = Pet::with(['species', 'type', 'user'])->where('pet_id', $petID[$i])->first();

                if(!empty($getpet)){

                    $species_id = empty($getpet->species) ? '-' :  $getpet->species->name." (".$getpet->species->shortname.")";
                    $type_id    = empty($getpet->type) ? '-' :  $getpet->type->type_pet." ".(($getpet->type->type_petsize == 11) ? 'ขนาดเล็ก' : 'ขนาดใหญ่');
                    $user_id    = empty($getpet->user) ? '-' : $getpet->user->firstname." ".$getpet->user->lastname;

                    $petArr = array(
                        "pet_id"        => $getpet->pet_id,
                        "pet_name"      => $getpet->pet_name,
                        "pet_gender"    => ($getpet->pet_gender == 11) ? 'เพศผู้' : 'เพศเมีย',
                        "species_id"    => $species_id,
                        "pet_birthday"  => $getpet->pet_birthday,
                        "pet_remark"    => $getpet->pet_remark,
                        "type_id"       => $type_id,
                        "user_id"       => $user_id,
                    );

                    array_push($petdetailArr, $petArr);
                }
            }

            // d(count($petdetailArr));
            // sd($petdetailArr);

        }

        // d($getspecies->toArray());
        // sd($getpettypes->toArray());


        return $this->view('font.bookingdetail',compact('reserves','getpettypes','getspecies', 'getpayment', 'petdetailArr'));
    }

    public function getBookingHistory()
    {
        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_Fname     = empty($user_obj->first_name) ? "" : $user_obj->first_name;
        $user_Lname     = empty($user_obj->last_name) ? "" : $user_obj->last_name;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->user_id) ? "" : $user_obj->user_id;


        $reserves = Reserve::with(['room', 'payment', 'roomtype'])->where('user_id', $user_id)->get();

        return $this->view('font.bookinghistory',compact('reserves'));
    }

    public function postAddBooking()
    {
        $reserve_data_chkin     = \Input::has('reserve_data_chkin') ? \Input::get('reserve_data_chkin') : '';
        $reserve_data_chkout    = \Input::has('reserve_data_chkout') ? \Input::get('reserve_data_chkout') : '';
        $reserve_nmroom         = \Input::has('reserve_nmroom') ? \Input::get('reserve_nmroom') : '';
        $reserve_nmpeople       = \Input::has('reserve_nmpeople') ? \Input::get('reserve_nmpeople') : '';
        $reserve_nmpet          = \Input::has('reserve_nmpet') ? \Input::get('reserve_nmpet') : '';
        $typeroom_id            = \Input::has('typeroom_id') ? \Input::get('typeroom_id') : '';

        $reserve_price     = \Input::has('reserve_price') ? \Input::get('reserve_price') : '';
        $numofnights       = \Input::has('numofnights') ? \Input::get('numofnights') : '';
        $bath              = \Input::has('bathprice') ? \Input::get('bathprice') : '';
        $swim              = \Input::has('swimprice') ? \Input::get('swimprice') : '';
        $food              = \Input::has('foodprice') ? \Input::get('foodprice') : '';
        $petfood           = \Input::has('petfoodprice') ? \Input::get('petfoodprice') : '';
        $bed               = \Input::has('bedprice') ? \Input::get('bedprice') : '';
        
        $firstname          = \Input::has('firstname') ? \Input::get('firstname') : '';
        $lastname           = \Input::has('lastname') ? \Input::get('lastname') : '';
        $email              = \Input::has('email') ? \Input::get('email') : '';
        $tel                = \Input::has('tel') ? \Input::get('tel') : '';
        $line_id            = \Input::has('line_id') ? \Input::get('line_id') : '';

        $addPetinfo         = \Input::has('addPetinfo') ? \Input::get('addPetinfo') : '';

        // BOOKING STATUS 
        // รอชำระเงิน
        // รอตรวจสอบการชำระเงิน
        // การจองสำเร็จ
        // อยู่ระหว่างการใช้งาน
        // เสร็จสิ้น

        $allowbooking = $this->CheckBookingRoom($reserve_data_chkin,$reserve_data_chkout,$typeroom_id,$reserve_nmroom);

        // sd($allowbooking);

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_Fname     = empty($user_obj->first_name) ? "" : $user_obj->first_name;
        $user_Lname     = empty($user_obj->last_name) ? "" : $user_obj->last_name;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->user_id) ? "" : $user_obj->user_id;
       
        // sd(json_decode($addPetinfo, true));
        // d($req_bed_type);
        // sd($req_breakfast_type);

        $petinfo = json_decode($addPetinfo, true);

        $user = User::where('user_id', $user_id)->first();
        // if(empty($user)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        if($allowbooking == true){
            if(!empty($user)){
                $user->firstname    = $firstname;
                $user->lastname     = $lastname;
                $user->email        = $email;
                $user->line_id      = $line_id;
                $user->tel          = $tel;
                $user->save();
            }else{
                $user               = new User;
                $user->firstname    = $firstname;
                $user->lastname     = $lastname;
                $user->email        = $email;
                $user->user_type    = 'ผู้ใช้ทั่วไป';
                $user->username     = $firstname;
                $user->password     = $firstname.'1234';
                $user->line_id      = $line_id;
                $user->tel          = $tel;
                $user->save();
            }

            $petidArr = array();

            if(count($petinfo) != 0){
                for ($i=0; $i < count($petinfo); $i++) { 

                    $newpet                 = new Pet;
                    $newpet->pet_name       = $petinfo[$i]['pet_name'];
                    $newpet->pet_gender     = $petinfo[$i]['pet_gender'];
                    $newpet->species_id     = $petinfo[$i]['species_id'];
                    $newpet->pet_birthday   = $petinfo[$i]['pet_birthday'];
                    $newpet->pet_remark     = $petinfo[$i]['pet_remark'];
                    $newpet->type_id        = $petinfo[$i]['type_id'];
                    $newpet->user_id        = $user->user_id;
                    $newpet->save();

                    array_push($petidArr, $newpet->pet_id);
                }
            }

            $reserve_number = $this->generateRandomString();


            $new_reserve                            = new Reserve;
            $new_reserve->reserve_number            = $reserve_number;
            $new_reserve->reserve_data_chkin        = $reserve_data_chkin;
            $new_reserve->reserve_data_chkout       = $reserve_data_chkout;
            $new_reserve->reserve_time_chkin        = null;
            // $new_reserve->reserve_data_tim       = $reserve_data_time;
            $new_reserve->reserve_nmroom            = $reserve_nmroom;
            $new_reserve->reserve_nmpeople          = $reserve_nmpeople;
            $new_reserve->reserve_nmpet             = $reserve_nmpet;
            $new_reserve->user_id                   = empty($user) ? null : $user->user_id;
            $new_reserve->pet_id                    = json_encode($petidArr);
            $new_reserve->typeroom_id               = $typeroom_id;
            $new_reserve->req_swim_type             = ($swim == 0) ? 0 : 1;
            $new_reserve->req_bed_type              = ($bed == 0) ? 0 : 1;
            $new_reserve->req_bath_type             = ($bath == 0) ? 0 : 1;
            $new_reserve->req_breakfast_type        = ($food == 0) ? 0 : 1;
            $new_reserve->req_pet_breakfast_type    = ($petfood == 0) ? 0 : 1;

            $new_reserve->num_swim                  = $swim;
            $new_reserve->num_bed                   = $bed;
            $new_reserve->num_bath                  = $bath;
            $new_reserve->num_food                  = $food;
            $new_reserve->num_petfood               = $petfood;
            $new_reserve->numofnights               = $numofnights;
            $new_reserve->reserve_price             = $reserve_price;
            $new_reserve->reserve_status            = 'รอชำระเงิน';
            // $new_roomtype->isDelete              = false;
            $new_reserve->save();
            
            \Session::forget('search_filters');

            return ['status' => 'success', 'username' => $user->username, 'password' => $user->password, 'reserveID' => $new_reserve->reserve_id];
        }else{
            return ['status' => 'error'];
        }

    }

    public function CheckBookingRoom($reserve_data_chkin,$reserve_data_chkout,$typeroom_id,$reserve_nmroom){

        $roomtype = Room::with(['roomtype']);
        $roomtype = $roomtype->where('typeroom_id', $typeroom_id);
        $roomtype = $roomtype->where('rest_status', 11);
        $roomtype = $roomtype->get();


        $getRoomTypes = RoomType::where('isDelete', false);
        $getRoomTypes = $getRoomTypes->where('typeroom_id', $typeroom_id);
        $getRoomTypes = $getRoomTypes->first();

        $getreserve = Reserve::whereBetween('reserve_data_chkin', [$reserve_data_chkin, $reserve_data_chkout]);
        $getreserve = $getreserve->orWhereBetween('reserve_data_chkout', [$reserve_data_chkin, $reserve_data_chkout]);
        $getreserve = $getreserve->orWhere(function($q) use($reserve_data_chkin, $reserve_data_chkout) {
                    $q->where('reserve_data_chkin', '<', $reserve_data_chkin)->where('reserve_data_chkout', '>', $reserve_data_chkout);
                });
        $getreserve = $getreserve->where(function($getreserve){
                    $getreserve = $getreserve->orWhere('reserve_status', '!=', 'รอชำระเงิน');
                    // $getreserve = $getreserve->orWhere('reserve_status', '!=', 'อยู่ระหว่างการใช้งาน');
                });
        $getreserve = $getreserve->get();

        if(!empty($roomtype) && !empty($getreserve))
        {
            if($roomtype->count() >= $reserve_nmroom && $roomtype->count() > $getreserve->count())
            {
                return true;
            }else{
                return false;
            } 
        }else{
            if(empty($roomtype)){
                return false;
            }

            if(empty($getreserve)){
                return true;
            }

        }


    }

    public function generateRandomString($length = 7) {
        // $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return "RESERVE0".$randomString;
    }

    public function ajaxCenter()
    {
        $method = \Input::has('method') ? \Input::get('method') : '';

        switch ($method) {

            case 'CancelBooking':

                $id          = \Input::has('id') ? \Input::get('id') : '';
                $status      = \Input::has('status') ? \Input::get('status') : '';
                
                $reserves = Reserve::with(['roomtype'])->where('reserve_id', $id)->first();
                if(empty($reserves)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $reserves->reserve_status        = $status;
                $reserves->save();

                return ['status' => 'success'];
                break;



            case 'addPayment':

                $id             = \Input::has('id') ? \Input::get('id') : '';
                $pay_money      = \Input::has('pay_money') ? \Input::get('pay_money') : '';
                $pay_date_time  = \Input::has('pay_date_time') ? \Input::get('pay_date_time') : '';
                $pay_bank       = \Input::has('pay_bank') ? \Input::get('pay_bank') : '';
                $pay_img        = \Input::hasFile('pay_img') ? \Input::file('pay_img') : '';

                $reserves = Reserve::with(['roomtype'])->where('reserve_id', $id)->first();
                if(empty($reserves)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                // sd($pay_date_time);

                $image_name = "";
                $date = date('Y-m-d');

                $path = base_path();
                if(!file_exists($path."/public/PaymentImg")){
                    $oldmask = umask(0);
                    mkdir($path."/public/PaymentImg", 0777);
                    umask($oldmask);
                }

                if(!empty($pay_img)){
                    $image_name = "/public/PaymentImg/".$date."-".$pay_img->getClientOriginalName();
                    copy($pay_img, $path.$image_name);
                }


                $newpay                 = new Payment;
                $newpay->pay_img        = $image_name;
                $newpay->pay_money      = $pay_money;
                $newpay->pay_date_time  = $pay_date_time;
                $newpay->pay_bank       = $pay_bank;
                $newpay->pay_status     = 'รอตรวจสอบการชำระเงิน';
                $newpay->save();

                if(!empty($reserves)){
                    $reserves->pay_id           = $newpay->pay_id;
                    $reserves->reserve_status   = 'รอตรวจสอบการชำระเงิน';
                    $reserves->save();
                }

                return ['status' => 'success'];
                break;


            case 'editPayment':

                $id             = \Input::has('id') ? \Input::get('id') : '';
                $payid          = \Input::has('payid') ? \Input::get('payid') : '';
                $pay_money      = \Input::has('pay_money') ? \Input::get('pay_money') : '';
                $pay_date_time  = \Input::has('pay_date_time') ? \Input::get('pay_date_time') : '';
                $pay_bank       = \Input::has('pay_bank') ? \Input::get('pay_bank') : '';
                $pay_img        = \Input::hasFile('pay_img') ? \Input::file('pay_img') : '';

                $reserves = Reserve::with(['roomtype'])->where('reserve_id', $id)->first();
                if(empty($reserves)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $payment = Payment::where('pay_id', $id)->first();
                if(empty($payment)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $image_name = "";
                $date = date('Y-m-d');

                $path = base_path();
                if(!file_exists($path."/public/PaymentImg")){
                    $oldmask = umask(0);
                    mkdir($path."/public/PaymentImg", 0777);
                    umask($oldmask);
                }

                if(!empty($pay_img)){
                    $image_name = "/public/PaymentImg/".$date."-".$pay_img->getClientOriginalName();
                    copy($pay_img, $path.$image_name);
                }

                $payment->pay_img        = $image_name;
                $payment->pay_money      = $pay_money;
                $payment->pay_date_time  = $pay_date_time;
                $payment->pay_bank       = $pay_bank;
                $payment->pay_status     = 'รอตรวจสอบการชำระเงิน';
                $payment->save();

                if(!empty($reserves)){
                    $reserves->pay_id           = $payment->pay_id;
                    $reserves->reserve_status   = 'รอตรวจสอบการชำระเงิน';
                    $reserves->save();
                }

                return ['status' => 'success'];
                break;


            case 'CheckBooking':

                $reserve_data_chkin     = \Input::get('reserve_data_chkin');
                $reserve_data_chkout    = \Input::get('reserve_data_chkout');
                $reserve_nmroom         = \Input::get('reserve_nmroom');
                $reserve_nmpeople       = \Input::get('reserve_nmpeople');
                $reserve_nmpet          = \Input::get('reserve_nmpet');
                $reserve_nmpetbig       = \Input::get('reserve_nmpetbig');
                $typeroom_id            = \Input::get('typeroom_id');

                // BOOKING STATUS 
                // รอชำระเงิน
                // รอตรวจสอบการชำระเงิน
                // การจองสำเร็จ
                // อยู่ระหว่างการใช้งาน
                // เสร็จสิ้น

               
                $roomtype = Room::with(['roomtype']);
                $roomtype = $roomtype->where('typeroom_id', $typeroom_id);
                $roomtype = $roomtype->where('rest_status', 11);
                $roomtype = $roomtype->get();


                $getRoomTypes = RoomType::where('isDelete', false);
                $getRoomTypes = $getRoomTypes->where('typeroom_id', $typeroom_id);
                $getRoomTypes = $getRoomTypes->first();

                $getreserve = Reserve::whereBetween('reserve_data_chkin', [$reserve_data_chkin, $reserve_data_chkout]);
                $getreserve = $getreserve->orWhereBetween('reserve_data_chkout', [$reserve_data_chkout, $reserve_data_chkout]);
                $getreserve = $getreserve->orWhere(function($q) use($reserve_data_chkin, $reserve_data_chkout) {
                            $q->where('reserve_data_chkin', '<', $reserve_data_chkin)->where('reserve_data_chkout', '>', $reserve_data_chkout);
                        });
                $getreserve = $getreserve->where(function($getreserve){
                            $getreserve->orWhere('reserve_status', '!=', 'รอชำระเงิน');
                            $getreserve = $getreserve->orWhere('reserve_status', '!=', 'อยู่ระหว่างการใช้งาน');
                        });
                $getreserve = $getreserve->get();

                // sd($getreserve);

                $filters = \Session::has('search_filters') ? \Session::get('search_filters') : [];

                if(!empty($roomtype) && !empty($getreserve))
                {
                    // d('1');
                    if($roomtype->count() >= $reserve_nmroom && $roomtype->count() > $getreserve->count())
                    {
                        // d('2');
                        if($getRoomTypes->people >= $reserve_nmpeople && ($getRoomTypes->smallpet >= $reserve_nmpet || $getRoomTypes->bigpet >= $reserve_nmpetbig))
                        {
                            // d('3');
                            if(!isset($filters)){
                                $filters                    = [];
                                $filters['checkin']         = \Input::get('reserve_data_chkin');
                                $filters['checkout']        = \Input::get('reserve_data_chkout');
                                $filters['persons']         = \Input::get('reserve_nmpeople');
                                $filters['pet']             = \Input::get('reserve_nmpet');
                                $filters['pet_big']         = \Input::get('reserve_nmpetbig');

                                \Session::put('search_filters', $filters);
                            }

                            return ['status' => 'success'];
                        
                        }else{
                            return ['status' => 'error'];
                        }
                    }else{
                        return ['status' => 'error'];
                    } 
                }else{
                    return ['status' => 'error'];
                }

                return ['status' => 'success'];
                break;

            default:
                return ['status' => 'error'];
                break;
        }
    }
}
