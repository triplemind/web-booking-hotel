<?php

namespace App\Http\Controllers\Font;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\RegisterStay\RegisterStay;
use App\Services\Reserve\Reserve;
use App\Services\Room\Room;
use App\Services\RoomType\RoomType;
use App\Services\Species\Species;

class FontController extends Controller
{
    public function getIndex()
    {
        
        return $this->view('font.index');
    }

    public function getActivityPage()
    {
        
        return $this->view('font.activity');
    }

    public function getGalleryPage()
    {
        
        return $this->view('font.gallery');
    }

    public function getSwimPage()
    {
        
        return $this->view('font.swim');
    }

    public function getFoodPage()
    {
        
        return $this->view('font.food');
    }

    public function getShowerPage()
    {
        
        return $this->view('font.shower');
    }


    public function getMyProfile()
    {
        
        return $this->view('font.profile');
    }

    public function postEditMyProfile()
    {
        
        return ['status' => 'success'];
    }

    public function getRoomTypePage()
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_Fname     = empty($user_obj->first_name) ? "" : $user_obj->first_name;
        $user_Lname     = empty($user_obj->last_name) ? "" : $user_obj->last_name;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->user_id) ? "" : $user_obj->user_id;

        $user = User::where('user_id', $user_id)->first();

        $getRoomTypes = RoomType::where('isDelete', false)->get();
        // sd($getRoomTypes->toArray());
        $ddd = "sfsfs";

        return $this->view('font.roomtype',compact('getRoomTypes', 'ddd', 'user'));
    }

    public function getRoomDetail(Request $request, $id)
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_Fname     = empty($user_obj->first_name) ? "" : $user_obj->first_name;
        $user_Lname     = empty($user_obj->last_name) ? "" : $user_obj->last_name;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->user_id) ? "" : $user_obj->user_id;

        $user = User::where('user_id', $user_id)->first();

        $getRoomType = RoomType::where('typeroom_id', $id)
                        ->where('isDelete', false)
                        ->first();

        $filters = \Session::has('search_filters') ? \Session::get('search_filters') : [];

        $getpettypes = PetType::where('isDelete', false)->where('status', 'Active')->get();
        $getspecies = Species::where('isDelete', false)->where('status', 'Active')->get();

        // d($getspecies->toArray());
        // sd($user->toArray());

        return $this->view('font.roomdetail',compact('getRoomType', 'filters', 'getspecies', 'getpettypes', 'user'));
    }

    public function getSearchPage()
    {
        $filters = \Session::has('search_filters') ? \Session::get('search_filters') : [];

        // sd($filters);

        $getRoomTypes = RoomType::where('isDelete', false);

        // if(!empty($filters['checkin']) && !empty($filters['checkout'])){
        //     $rangecheckout = date('Y-m-d',strtotime($filters['checkout'] . "+1 days"));

        //     $getRoomTypes = $getRoomTypes->whereBetween('delivery_date', [$filters['checkin'], $rangecheckout]);
        // }

        if(!empty($filters['persons'])){
            $getRoomTypes = $getRoomTypes->where('people', '>=' ,$filters['persons']);
        }

        if(!empty($filters['pet'])){
            $getRoomTypes = $getRoomTypes->where('smallpet', '>=' ,$filters['pet']);
           
        }

        if(!empty($filters['pet_big'])){
            $getRoomTypes = $getRoomTypes->where('bigpet', '>=' ,$filters['pet_big']);
           
        }

        $getRoomTypes = $getRoomTypes->get();
        
        return $this->view('font.search', compact('filters', 'getRoomTypes'));
    }

    public function ajaxCenter()
    {
        $method = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
            case 'addFilterToSession':

                $filters                    = [];
                $filters['checkin']         = \Input::get('checkin');
                $filters['checkout']        = \Input::get('checkout');
                $filters['persons']         = \Input::get('persons');
                $filters['pet']             = \Input::get('pet');
                $filters['pet_big']         = \Input::get('pet_big');

                \Session::put('search_filters', $filters);

                return ['status' => 'success'];
                break;

             case 'clearFilterToSession':

                \Session::forget('search_filters');
                
                return ['status' => 'success'];
                break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
