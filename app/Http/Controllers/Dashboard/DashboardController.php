<?php

namespace App\Http\Controllers\Dashboard;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\RegisterStay\RegisterStay;
use App\Services\Reserve\Reserve;
use App\Services\Room\Room;
use App\Services\RoomType\RoomType;

class DashboardController extends Controller
{
    public function getIndex()
    {
        // BOOKING STATUS 
        // รอชำระเงิน
        // รอตรวจสอบการชำระเงิน
        // การจองสำเร็จ
        // อยู่ระหว่างการใช้งาน
        // เสร็จสิ้น
        
        $reserves = Reserve::whereDate('created_at' , Carbon::today())
                    ->get();

        $datetoday = date('Y-m-d H:i:s');
        $datetoday = DateThai($datetoday, true, true);

        $total_all = 0;
        

    	return $this->view_admin('dashboard.index',compact('reserves', 'datetoday', 'total_all'));
    }

    public function getReserveDetail(Request $request, $id)
    {

        $user_obj       = \Session::has('current_user') ? \Session::get('current_user') : '';
        $user_Fname     = empty($user_obj->first_name) ? "" : $user_obj->first_name;
        $user_Lname     = empty($user_obj->last_name) ? "" : $user_obj->last_name;
        $username       = empty($user_obj->username) ? "" : $user_obj->username;
        $user_type      = empty($user_obj->user_type) ? "" : $user_obj->user_type;
        $user_img       = empty($user_obj->user_img) ? "" : $user_obj->user_img;
        $user_id        = empty($user_obj->user_id) ? "" : $user_obj->user_id;


        $reserves = Reserve::with(['roomtype', 'payment', 'room'])->where('reserve_id', $id)->first();

        $getrooms = Room::with(['roomtype']);
        $getrooms = $getrooms->where('typeroom_id', $reserves->typeroom_id);
        $getrooms = $getrooms->where('rest_status', 11);
        $getrooms = $getrooms->where('isusable', true);
        $getrooms = $getrooms->where('isDelete', false);
        $getrooms = $getrooms->get();


        // sd($reserves->toArray());
        // sd($reserves->toArray());

        return $this->view_admin('dashboard.orderdetail', compact('reserves','user_type', 'getrooms'));
    }


    public function getHitoriesOrder()
    {
        // BOOKING STATUS 
        // รอชำระเงิน
        // รอตรวจสอบการชำระเงิน
        // การจองสำเร็จ
        // อยู่ระหว่างการใช้งาน
        // เสร็จสิ้น

        $filters = \Session::has('histories_filters') ? \Session::get('histories_filters') : [];

        $reserves = Reserve::with(['room', 'payment', 'roomtype'])->where('reserve_id', '!=', 0);

        if(!empty($filters['rangestart']) && !empty($filters['rangeend'])){
            $rangeend_add = date('Y-m-d',strtotime($filters['rangeend'] . "+1 days"));
            $reserves = $reserves->whereBetween('created_at', [$filters['rangestart'], $rangeend_add]);
        }

        if(!empty($filters['order_status'])){
            $reserves = $reserves->where('order_status', $filters['order_status']);
        }

        $reserves = $reserves->get();

        return $this->view_admin('dashboard.histories', compact('reserves', 'filters'));
    }

    public function ajaxCenter()
    {
        $method = \Input::has('method') ? \Input::get('method') : '';

        switch ($method) {

            case 'CompletedOrder':

                $reserve_id = \Input::has('reserve_id') ? \Input::get('reserve_id') : '';
                $status = \Input::has('status') ? \Input::get('status') : '';

                $reserves = Reserve::with(['roomtype'])->where('reserve_id', $reserve_id)->first();

                if(empty($reserves)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $reserves->reserve_status = $status;
                $reserves->save();

                return ['status' => 'success'];
                break;

            case 'CancelOrder':

                $reserve_id = \Input::has('reserve_id') ? \Input::get('reserve_id') : '';

                $reserves = Reserve::with(['roomtype'])->where('reserve_id', $reserve_id)->first();

                if(empty($reserves)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                $reserves->reserve_status = "ยกเลิก";
                $reserves->save();


                return ['status' => 'success'];
                break;


            case 'saveDateTimeINOut':

                $reserve_id             = \Input::has('reserve_id') ? \Input::get('reserve_id') : '';
                $reserve_data_chkout    = \Input::has('reserve_data_chkout') ? \Input::get('reserve_data_chkout') : '';
                $reserve_data_time      = \Input::has('reserve_data_time') ? \Input::get('reserve_data_time') : '';

                $reserve_data_chkin     = \Input::has('reserve_data_chkin') ? \Input::get('reserve_data_chkin') : '';
                $reserve_time_chkin     = \Input::has('reserve_time_chkin') ? \Input::get('reserve_time_chkin') : '';
                $room_id                = \Input::has('room_id') ? \Input::get('room_id') : '';
                $status                 = \Input::has('status') ? \Input::get('status') : '';
                $action                 = \Input::has('action') ? \Input::get('action') : '';
                $realname               = \Input::has('realname') ? \Input::get('realname') : '';
                $idCard                 = \Input::has('idCard') ? \Input::get('idCard') : '';

                $reserves = Reserve::with(['roomtype'])->where('reserve_id', $reserve_id)->first();

                if(empty($reserves)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

                // sd(date('H:i:s', strtotime($reserve_data_time)));

                if($action == 'checkin'){
                    $reserves->reserve_time_chkin   = empty($reserve_time_chkin) ? null : date('H:i:s', strtotime($reserve_time_chkin));
                    $reserves->reserve_data_chkin   = $reserve_data_chkin;
                    $reserves->room_id              = $room_id;
                    $reserves->realname             = $realname;
                    $reserves->idCard               = $idCard;
                }
                if($action == 'checkout'){
                    $reserves->reserve_data_time    = empty($reserve_data_time) ? null : date('H:i:s', strtotime($reserve_data_time));
                    $reserves->reserve_data_chkout  = $reserve_data_chkout;
                }
                $reserves->reserve_status           = $status;
                $reserves->save();


                return ['status' => 'success'];
                break;


            case 'addFilterToSession':

                $filters                    = [];
                $filters['rangestart']      = \Input::get('rangestart');
                $filters['rangeend']        = \Input::get('rangeend');
                $filters['order_status']    = \Input::get('order_status');
                $filters['search_txt']      = \Input::get('search_txt');

                \Session::put('histories_filters', $filters);

                return ['status' => 'success'];
                break;

             case 'clearFilterToSession':

                \Session::forget('histories_filters');
                
                return ['status' => 'success'];
                break;

            default:
                return ['status' => 'error'];
                break;
        }
    }
}
