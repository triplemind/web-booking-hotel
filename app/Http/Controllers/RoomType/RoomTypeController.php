<?php

namespace App\Http\Controllers\RoomType;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\Users\User;
use App\Services\Payment\Payment;
use App\Services\Pet\Pet;
use App\Services\PetType\PetType;
use App\Services\RegisterStay\RegisterStay;
use App\Services\Reserve\Reserve;
use App\Services\Room\Room;
use App\Services\RoomType\RoomType;

class RoomTypeController extends Controller
{
    public function getIndex()
    {
        $getRoomTypes = RoomType::where('typeroom_id', '!=', 0)->get();
        
        return $this->view_admin('roomtype.index',compact('getRoomTypes'));
    }

    public function postAdd()
    {
        $type_nameroom  = \Input::has('type_nameroom') ? \Input::get('type_nameroom') : '';
        $type_rate      = \Input::has('type_rate') ? \Input::get('type_rate') : '';
        // $cost           = \Input::has('cost') ? \Input::get('cost') : '';
        $size           = \Input::has('size') ? \Input::get('size') : '';
        $people         = \Input::has('people') ? \Input::get('people') : '';
        $extra          = \Input::has('extra') ? \Input::get('extra') : '';
        $bigpet         = \Input::has('bigpet') ? \Input::get('bigpet') : '';
        $smallpet       = \Input::has('smallpet') ? \Input::get('smallpet') : '';
        $info           = \Input::has('info') ? \Input::get('info') : '';
        $checkindate    = \Input::has('checkindate') ? \Input::get('checkindate') : '';
        $checkoutdate   = \Input::has('checkoutdate') ? \Input::get('checkoutdate') : '';
        $room_img       = \Input::hasFile('room_img') ? \Input::file('room_img') : '';

        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/RoomTypeImg")){
            $oldmask = umask(0);
            mkdir($path."/public/RoomTypeImg", 0777);
            umask($oldmask);
        }

        if(!empty($room_img)){
            $image_name = "/public/RoomTypeImg/".$date."-".$room_img->getClientOriginalName();
            copy($room_img, $path.$image_name);
        }


        $new_roomtype                   = new RoomType;
        $new_roomtype->type_nameroom    = $type_nameroom;
        $new_roomtype->type_rate        = $type_rate;
        // $new_roomtype->cost             = $cost;
        $new_roomtype->size             = $size;
        $new_roomtype->people           = $people;
        $new_roomtype->extra            = $extra;
        $new_roomtype->bigpet           = $bigpet;
        $new_roomtype->smallpet         = $smallpet;
        $new_roomtype->info             = $info;
        $new_roomtype->checkindate      = $checkindate;
        $new_roomtype->checkoutdate     = $checkoutdate;
        $new_roomtype->isDelete         = false;
        $new_roomtype->room_img         = (empty($image_name)) ? "" : $image_name;
        $new_roomtype->save();


        return ['status' => 'success'];
    }

    public function postEdit()
    {
        $typeroom_id    = \Input::has('typeroom_id') ? \Input::get('typeroom_id') : '';
        $type_nameroom  = \Input::has('type_nameroom') ? \Input::get('type_nameroom') : '';
        $type_rate      = \Input::has('type_rate') ? \Input::get('type_rate') : '';
        // $cost           = \Input::has('cost') ? \Input::get('cost') : '';
        $size           = \Input::has('size') ? \Input::get('size') : '';
        $people         = \Input::has('people') ? \Input::get('people') : '';
        $extra          = \Input::has('extra') ? \Input::get('extra') : '';
        $bigpet         = \Input::has('bigpet') ? \Input::get('bigpet') : '';
        $smallpet       = \Input::has('smallpet') ? \Input::get('smallpet') : '';
        $info           = \Input::has('info') ? \Input::get('info') : '';
        $checkindate    = \Input::has('checkindate') ? \Input::get('checkindate') : '';
        $checkoutdate   = \Input::has('checkoutdate') ? \Input::get('checkoutdate') : '';
        $room_img       = \Input::hasFile('room_img') ? \Input::file('room_img') : '';

        $roomtype = RoomType::where('typeroom_id', $typeroom_id)->first();
        if(empty($roomtype)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];
        
        $image_name = "";
        $date = date('Y-m-d');

        $path = base_path();
        if(!file_exists($path."/public/RoomTypeImg")){
            $oldmask = umask(0);
            mkdir($path."/public/RoomTypeImg", 0777);
            umask($oldmask);
        }

        if(!empty($room_img)){
            $image_name = "/public/RoomTypeImg/".$date."-".$room_img->getClientOriginalName();
            copy($room_img, $path.$image_name);
        }
        
        $roomtype->type_nameroom    = $type_nameroom;
        $roomtype->type_rate        = $type_rate;
        // $roomtype->cost             = $cost;
        $roomtype->size             = $size;
        $roomtype->people           = $people;
        $roomtype->extra            = $extra;
        $roomtype->bigpet           = $bigpet;
        $roomtype->smallpet         = $smallpet;
        $roomtype->info             = $info;
        $roomtype->checkindate      = $checkindate;
        $roomtype->checkoutdate     = $checkoutdate;
        if(!empty($image_name)){
            $roomtype->room_img     = $image_name;
        }
        $roomtype->save();
        
        return ['status' => 'success'];
    }

    public function postRemove()
    {
        $typeroom_id    = \Input::has('typeroom_id') ? \Input::get('typeroom_id') : '';

        $roomtype = RoomType::where('typeroom_id', $typeroom_id)->first();
        if(empty($roomtype)) return ['status' => 'error', 'msg' => 'ไม่พบข้อมูลในระบบ'];

        // $path = base_path();
        // if(!empty($roomtype->room_img)){
        //     if(file_exists($path.$roomtype->room_img)){
        //         unlink($path.$roomtype->room_img);
        //     }
        // }

        $roomtype->isDelete = true;
        $roomtype->save();

        // $roomtype->delete();

        return ['status' => 'success'];
    }

    public function ajaxCenter()
    {
        $method = \Input::has('method') ? \Input::get('method') : '';
        
        switch ($method) {
            case 'getRoomTypeData':

                $typeroom_id = \Input::has('typeroom_id') ? \Input::get('typeroom_id') : '';

                $roomtype = RoomType::where('typeroom_id', $typeroom_id)->first();

                if(!empty($roomtype)){
                    return ['status' => 'success', 'data' => $roomtype];
                }else{
                    return ['status' => 'error', 'data' => "ไม่พบข้อมูลในระบบ"];
                }
                
                break;

            default:
                return ['status' => 'error', 'msg' => 'Not found method'];
                break;
        }
    }
}
