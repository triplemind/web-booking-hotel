<?php namespace App\Services;

use App\Services\Users\UserObject;

class BaseRepository {

	public $userObject;

	public function __construct()
	{
		$this->userObject = new UserObject;
	}
}