<?php namespace App\Services\RegisterStay;

use Illuminate\Database\Eloquent\Model;

class RegisterStay extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'registe_tstay';
 	protected $primaryKey 	= 'registe_stay_id';

	// public function catagory()
	// {
	// 	return $this->belongsTo("App\Services\Catagories\Catagories", 'user_id');
	// }
}