<?php namespace App\Services\RoomType;

use Illuminate\Database\Eloquent\Model;

class RoomType extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'room_type';
 	protected $primaryKey 	= 'typeroom_id';

	// public function catagory()
	// {
	// 	return $this->belongsTo("App\Services\Catagories\Catagories", 'user_id');
	// }
}