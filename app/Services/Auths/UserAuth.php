<?php namespace App\Services\Auths;

use App\Services\Users\UserObject;
use App\Services\Users\User;

class UserAuth {


    public static function attempt($username, $password)
    {
        $userObject = new UserObject;

        // Query.
        $user   = User::where('username', $username)->where('password', $password);
        $user   = $user->orWhere(function($q) use ($username, $password) {
                        $q->where('email', $username)->where('password', $password);
                    });

        $userModel = $user->first(['user_id', 'user_type', 'username', 'firstname', 'lastname', 'tel', 'line_id', 'email']);

        if (empty($userModel)) return false;

        // if($userModel->user_type == 'admin')  \session::put('id', 0);
        // Save User to Session.
        $userObject->setUp($userModel);
        // Set Lang.
        // setLang();
        //Return.
        return $userModel;
	}

    public static function check_user_type()
    {
        $result = false;

        if (\Session::has('current_user')) {
            if (\Session::get('current_user')->user_type == 'ผู้ดูแลระบบ') {

                $result = true;
            }
        }

        return $result;
    }

	public static function check()
	{
		$result = false;

        if (\Session::has('current_user')) {

        	$result = true;
        }

        return $result;
	}

    public static function clear()
    {
        \Session::forget('current_user');
        
    }
}