<?php namespace App\Services\Payment;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'payment';
 	protected $primaryKey 	= 'pay_id';

	// public function catagory()
	// {
	// 	return $this->belongsTo("App\Services\Catagories\Catagories", 'user_id');
	// }
}