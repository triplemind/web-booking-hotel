<?php namespace App\Services\Reserve;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'tbl_reserve';
 	protected $primaryKey 	= 'reserve_id';

	public function roomtype()
	{
		return $this->belongsTo("App\Services\RoomType\RoomType", 'typeroom_id');
	}

	public function payment()
	{
		return $this->belongsTo("App\Services\Payment\Payment", 'pay_id');
	}

	public function room()
	{
		return $this->belongsTo("App\Services\Room\Room", 'room_id');
	}
}