<?php namespace App\Services\PetType;

use Illuminate\Database\Eloquent\Model;

class PetType extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'pet_type';
 	protected $primaryKey 	= 'type_id';

	// public function catagory()
	// {
	// 	return $this->belongsTo("App\Services\Catagories\Catagories", 'user_id');
	// }
}