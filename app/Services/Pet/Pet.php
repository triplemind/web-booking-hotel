<?php namespace App\Services\Pet;

use Illuminate\Database\Eloquent\Model;

class Pet extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'pet';
 	protected $primaryKey 	= 'pet_id';

	public function species()
	{
		return $this->belongsTo("App\Services\Species\Species", 'species_id');
	}

	public function type()
	{
		return $this->belongsTo("App\Services\PetType\PetType", 'type_id');
	}

	public function user()
	{
		return $this->belongsTo("App\Services\Users\User", 'user_id');
	}
}