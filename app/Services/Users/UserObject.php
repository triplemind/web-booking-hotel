<?php namespace App\Services\Users;

use App\Services\Users\User;

class UserObject {

	protected $user_id;
	protected $user_type;
	protected $username;
	protected $firstname;
	protected $lastname;
	protected $tel;
	protected $line_id;
	protected $email;

	public function __construct()
	{
		// Put Session to Object.
		if (\Session::has('current_user')) {

			$current_user 			= \Session::get('current_user');
			$this->user_id			= $current_user->user_id;
			$this->user_type 		= $current_user->user_type;
			$this->username 		= $current_user->username;
			$this->firstname 		= $current_user->firstname;
			$this->lastname 		= $current_user->lastname;
			$this->tel 				= $current_user->tel;
			$this->line_id 			= $current_user->line_id;
			$this->email 			= $current_user->email;
		}
	}

	public function setUp($userModel)
	{
		// Save to Session.
        \Session::put('current_user', $userModel);
    }

	public function getID()
	{
		return $this->user_id;
	}
	public function getFirstname()
	{
		return $this->firstname;
	}
	public function getLastname()
	{
		return $this->lastname;
	}
	public function getUsername()
	{
		return $this->username;
	}
	public function getUserType()
	{
		return $this->user_type;
	}
	public function getEmail()
	{
		return $this->email;
	}
	public function getTel()
	{
		return $this->tel;
	}
	public function getLineID()
	{
		return $this->line_id;
	}
	

}