<?php namespace App\Services\Users;

use Illuminate\Database\Eloquent\Model;

class User extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'tbl_user';
 	protected $primaryKey 	= 'user_id';

	// public function catagory()
	// {
	// 	return $this->belongsTo("App\Services\Catagories\Catagories", 'user_id');
	// }
}