<?php namespace App\Services\Room;

use Illuminate\Database\Eloquent\Model;

class Room extends Model {

	protected $connection 	= 'mysql';
	protected $table 		= 'room';
 	protected $primaryKey 	= 'rm_id';

	public function roomtype()
	{
		return $this->belongsTo("App\Services\RoomType\RoomType", 'typeroom_id');
	}
}