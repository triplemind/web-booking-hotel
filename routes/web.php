<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// ADMIN REDIRECT TO LOGIN
Route::get('/admin', function () {
    return \Redirect::route('auth.login.get');
});


// LOGIN ROUTE
Route::get('/auth/login', [
	'uses'	=> 'Auth\LoginController@getLogin',
	'as'	=> 'auth.login.get'
]);

Route::post('/auth/login', [
	'uses'	=> 'Auth\LoginController@postLogin',
	'as'	=> 'auth.login.post'
]);
// LOGIN ROUTE

// LOGOUT ROUTE
Route::get('/auth/logout', [
	'uses'	=> 'Auth\LoginController@logout',
	'as'	=> 'auth.logout.get'
]);
// LOGOUT ROUTE

// FORGOT PASSWORD ROUTE
Route::get('/auth/forgotpassword', [
	'uses'	=> 'Auth\ForgotPasswordController@getForgotPassword',
	'as'	=> 'auth.forgotpassword.get'
]);

Route::post('/auth/forgotpassword', [
	'uses'	=> 'Auth\ForgotPasswordController@postForgotPassword',
	'as'	=> 'auth.forgotpassword.post'
]);
// FORGOT PASSWORD ROUTE

// REGISTER
Route::get('/auth/register', [
  'uses'  => 'Auth\RegisterController@getRegister',
  'as'  => 'auth.register.get'
]);

Route::post('/auth/register', [
  'uses'  => 'Auth\RegisterController@postRegister',
  'as'  => 'auth.register.post'
]);
// REGISTER

// +++++++++++++++++++++++ ADMIN +++++++++++++++++++++++

// DASHBOARD
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Dashboard'], function(){

  Route::get('/dashboard',[
    'uses' => 'DashboardController@getIndex',
    'as' => 'dashboard.index.get'
  ]);

  Route::get('/dashboard/reservedetail/{id}', [
    'uses'  => 'DashboardController@getReserveDetail',
    'as'  => 'dashboard.orderdetail.get'
  ]);

  Route::get('/dashboard/histories', [
    'uses'  => 'DashboardController@getHitoriesOrder',
    'as'  => 'dashboard.histories.get'
  ]);

   Route::post('/dashboard/ajax_center', [
    'uses'  => 'DashboardController@ajaxCenter',
    'as'  => 'dashboard.ajax_center.post'
  ]);

});
// DASHBOARD  


// USER
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'User'], function(){

  Route::get('/user',[
    'uses' => 'UserController@getIndex',
    'as' => 'user.index.get'
  ]);

  Route::post('/user/ajax_center', [
    'uses'  => 'UserController@ajaxCenter',
    'as'  => 'user.ajax_center.post'
  ]);

  Route::post('/user/add', [
    'uses'  => 'UserController@postAdd',
    'as'  => 'user.add.post'
  ]);

  Route::post('/user/edit', [
    'uses'  => 'UserController@postEdit',
    'as'  => 'user.edit.post'
  ]);

  Route::post('/user/delete', [
    'uses'  => 'UserController@postRemove',
    'as'  => 'user.delete.post'
  ]);

});
// USER

// MANAGE ROOM
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'Room'], function(){

  Route::get('/room',[
    'uses' => 'RoomController@getIndex',
    'as' => 'room.index.get'
  ]);

  Route::post('/room/ajax_center', [
    'uses'  => 'RoomController@ajaxCenter',
    'as'  => 'room.ajax_center.post'
  ]);

  Route::post('/room/add', [
    'uses'  => 'RoomController@postAdd',
    'as'  => 'room.add.post'
  ]);

  Route::post('/room/edit', [
    'uses'  => 'RoomController@postEdit',
    'as'  => 'room.edit.post'
  ]);

  Route::post('/room/delete', [
    'uses'  => 'RoomController@postRemove',
    'as'  => 'room.delete.post'
  ]);

});
// MANAGE ROOM

// MANAGE ROOM TYPE
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'RoomType'], function(){

  Route::get('/roomtype',[
    'uses' => 'RoomTypeController@getIndex',
    'as' => 'roomtype.index.get'
  ]);

  Route::post('/roomtype/ajax_center', [
    'uses'  => 'RoomTypeController@ajaxCenter',
    'as'  => 'roomtype.ajax_center.post'
  ]);

  Route::post('/roomtype/add', [
    'uses'  => 'RoomTypeController@postAdd',
    'as'  => 'roomtype.add.post'
  ]);

  Route::post('/roomtype/edit', [
    'uses'  => 'RoomTypeController@postEdit',
    'as'  => 'roomtype.edit.post'
  ]);

  Route::post('/roomtype/delete', [
    'uses'  => 'RoomTypeController@postRemove',
    'as'  => 'roomtype.delete.post'
  ]);

});
// MANAGE ROOM TYPE

// MANAGE PET TYPE
Route::group(['middleware' => ['auth'], 'prefix' => 'admin', 'namespace' => 'PetType'], function(){

  Route::get('/pettype',[
    'uses' => 'PetTypeController@getIndex',
    'as' => 'pettype.index.get'
  ]);

  Route::post('/pettype/ajax_center', [
    'uses'  => 'PetTypeController@ajaxCenter',
    'as'  => 'pettype.ajax_center.post'
  ]);

  Route::post('/pettype/add', [
    'uses'  => 'PetTypeController@postAdd',
    'as'  => 'pettype.add.post'
  ]);

  Route::post('/pettype/edit', [
    'uses'  => 'PetTypeController@postEdit',
    'as'  => 'pettype.edit.post'
  ]);

  Route::post('/pettype/delete', [
    'uses'  => 'PetTypeController@postRemove',
    'as'  => 'pettype.delete.post'
  ]);

  Route::post('/pettype/addspecies', [
    'uses'  => 'PetTypeController@postAddSpecies',
    'as'  => 'pettype.addspecies.post'
  ]);

  Route::post('/pettype/editspecies', [
    'uses'  => 'PetTypeController@postEditSpecies',
    'as'  => 'pettype.editspecies.post'
  ]);

  Route::post('/pettype/deletespecies', [
    'uses'  => 'PetTypeController@postRemoveSpecies',
    'as'  => 'pettype.deletespecies.post'
  ]);

});
// // MANAGE PET TYPE

// +++++++++++++++++++++++ ADMIN +++++++++++++++++++++++



// +++++++++++++++++++++++ NORMAL USER +++++++++++++++++++++++
Route::group(['namespace' => 'Font'], function(){

  Route::get('/',[
    'uses' => 'FontController@getIndex',
    'as' => 'font.index.get'
  ]);

  Route::get('/activity',[
    'uses' => 'FontController@getActivityPage',
    'as' => 'font.activity.get'
  ]);

  Route::get('/gallery',[
    'uses' => 'FontController@getGalleryPage',
    'as' => 'font.gallery.get'
  ]);

   Route::get('/swim',[
    'uses' => 'FontController@getSwimPage',
    'as' => 'font.swim.get'
  ]);

    Route::get('/food',[
    'uses' => 'FontController@getFoodPage',
    'as' => 'font.food.get'
  ]);

     Route::get('/shower',[
    'uses' => 'FontController@getShowerPage',
    'as' => 'font.shower.get'
  ]);

  //PROFILE
  Route::get('/profile',[
    'uses' => 'FontController@getMyProfile',
    'as' => 'font.profile.get'
  ]);

  Route::post('/profile',[
    'uses' => 'FontController@postEditMyProfile',
    'as' => 'font.profile.post'
  ]);
  //PROFILE

  //ROOMS DETAIL
  Route::get('/roomtype',[
    'uses' => 'FontController@getRoomTypePage',
    'as' => 'font.roomtype.get'
  ]);

  Route::get('/roomdetail/{id}',[
    'uses' => 'FontController@getRoomDetail',
    'as' => 'font.roomdetail.get'
  ]);

  // Route::get('/search/{data}',[
  Route::get('/search',[
    'uses' => 'FontController@getSearchPage',
    'as' => 'font.search.get'
  ]);
  //ROOMS DETAIL

  Route::post('/ajax_center', [
    'uses'  => 'FontController@ajaxCenter',
    'as'  => 'font.ajax_center.post'
  ]);


  // // BOOKING
  Route::get('/booking/{id}',[
    'uses' => 'BookingController@getIndex',
    'as' => 'booking.booking.get'
  ]);

  Route::post('/booking',[
    'uses' => 'BookingController@postAddBooking',
    'as' => 'booking.booking.post'
  ]);

  Route::get('/bookingdetail/{id}',[
    'uses' => 'BookingController@getBookingDetail',
    'as' => 'booking.bookingdetail.get'
  ]);

  Route::get('/bookinghistory',[
    'uses' => 'BookingController@getBookingHistory',
    'as' => 'booking.bookinghistory.get'
  ]);

  Route::post('/booking/ajax_center', [
    'uses'  => 'BookingController@ajaxCenter',
    'as'  => 'booking.ajax_center.post'
  ]);
  // // BOOKING

});

// +++++++++++++++++++++++ NORMAL USER +++++++++++++++++++++++